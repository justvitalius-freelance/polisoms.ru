## Prepare a remote server
- Connect to remote server via ssh and install ```rvm``` and ```imagemagick```.
- Create a new Database with ```utf8_general_ci``` charset.
- Install ruby like ```rvm install 2.1.5```.
- Create a gemset like ```rvm use 2.1.5@polisoms --create```.


## Configure remote server for First Deploy
- Clone this project
- Run ```bundle install``` in the project folder
- Configure ```server``` and ```user``` in ```deploy.rb```.
- Run ```cap production deploy:check```. Capistrano will connect to remote server and create folders structure. But will fail because not find some config files.
- Connect to remote server via ssh. Insert valid ```database.yml``` and ```mail.yml``` into ```shared/config``` folder. And insert valid ```robots.txt``` into ```shared/public``` folder.
- Run again ```cap production deploy:check```. At now it will finish successfully.

## First Deploy on configured remote server
- Run ```cap production deploy``` on local. Capistrano will fully deploy project, install gems, migrate database and compile assets.
If it was fail when install mysql2, try run on remote server ```~/.rvm/bin/rvm2.1.5@polisoms do bundle config build.mysql2 --with-mysql-include=/usr/in clude/mysql/ --with-mysql-lib=/usr/ lib64/mysql```.
- After successfully deploy run ```cap production unicorn:start```.
- At last run ```cap production whenever:update_cron``` to start sync by cron and ```cap production delayed_job:start``` for email sender.
- To setup searching engine run ```cap production thinking_sphinx:index``` and ```cap production thinking_sphinx:start```. 

# To manual start sync
Run ```cap production sync:start```


# Whenever receipes
Apply ```cap staging whenever:update_crontab```
Disable ```cap staging whenever:clear_crontab```
[More info](https://github.com/javan/whenever/blob/master/lib/whenever/capistrano/v2/recipes.rb)

# Sync Doctor via API
To get doctors run ```rake sync:start``` or ```cap staging sync:start```.
To clear run ```rake sync:clear``` or ```cap staging sync:clear```.