set :stage, :production
set :branch, 'master'


set :rvm_type, :user
set :rvm_ruby_version, '2.1.5@polisoms'

set :bundle_jobs, 4
set :bundle_flags, ''


# delayed job

before 'deploy:starting', 'delayed_job:stop'
after 'deploy:publishing', 'delayed_job:start'
# after 'deploy:publishing', 'delayed_job:restart'
#

after 'deploy:publishing', 'unicorn:restart'