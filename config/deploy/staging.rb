set :stage, :staging
set :branch, 'feature/vacancies'

set :rvm_type, :user
set :rvm_ruby_version, '2.1.5@polisoms'

set :bundle_jobs, 4
set :bundle_flags, ''

set :log_level, :debug

# after 'deploy:finishing', 'content:clone_system'

set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }


# after 'deploy:stop', 'delayed_job:stop'
before 'deploy:starting', 'unicorn:stop'
after 'deploy:publishing', 'unicorn:restart'
