# first step
# cap production deploy:check
#
# then
# transfer to server database.yml, mail.yml, robots.txt
#
# and finally
# cap production deploy

# ~/.rvm/ bin/rvm 2.1 .5 @polisoms do bundle config build.mysql2 --with-mysql-include=/usr/in clude/mysql/ --with-mysql-lib=/usr/ lib64/mysql

# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'polisoms'
set :repo_url, 'git@bitbucket.org:justvitalius-freelance/polisoms.ru.git'

set :deploy_user, 'polis'
set :deploy_to, "/home/#{fetch(:deploy_user)}/www/#{fetch(:application)}-#{fetch(:stage)}"

set :deploy_via, :copy

# server '82.146.52.34', user: 'polis', roles: %w{web app db}
server '95.213.207.84', user: 'polis', roles: %w{web app db}


set :use_sudo, false
set :log_level, :debug

set :linked_files, %w{config/database.yml config/mail.yml public/robots.txt}
set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

set :keep_releases, 5


set :delayed_job_roles, [:app]
set :delayed_job_bin_path, 'script'
set :delayed_job_args, '-n 1 --queues=mailers'


# before 'deploy:starting', 'delayed_job:stop'

# Unicorn
# after 'deploy:publishing', 'unicorn:restart'
# after 'deploy:publishing', 'delayed_job:start'