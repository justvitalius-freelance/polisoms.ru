Polisoms::Application.routes.draw do

  match '/search', to: 'search#search'
  

  # match '/choose_district' => 'appointments#choose_district'
  # match '/choose_hospital' => 'appointments#choose_hospital'
  # match '/check_patient' => 'appointments#check_patient'
  # match '/get_speciality_list' => 'appointments#get_speciality_list'
  # match '/get_doctor_list' => 'appointments#get_doctor_list'
  # match '/get_available_dates' => 'appointments#get_available_dates'
  # match '/get_available_appointments' => 'appointments#get_available_appointments'
  # match '/set_appointment' => 'appointments#set_appointment'
  # match '/get_patient_history' => 'appointments#get_patient_history'
  # match '/create_claim_for_refusal' => 'appointments#create_claim_for_refusal'


  scope 'api' do
    # раздел запись к врачу
    get 'districts' => 'api_appointments#districts'
    get 'polyclinics' => 'api_appointments#polyclinics'
    get 'specialities' => 'api_appointments#specialities'
    get 'doctors' => 'api_appointments#doctors'
    # ======================
  end

  namespace :api do
    resources :reviews, only: [:create, :index]
  end



  # публичные страницы с записью к врачу
  get '/appointment/step1' => 'appointments#step1', as: :appointment_step1
  match '/appointment/step2' => 'appointments#step2', as: :appointment_step2
  match '/appointment/step3' => 'appointments#step3', as: :appointment_step3
  match '/appointment/step4' => 'appointments#step4', as: :appointment_step4
  match '/appointment/check' => 'appointments#check', as: :appointment_check
  match '/appointment/confirm' => 'appointments#confirm', as: :appointment_confirm
  match '/appointment/history' => 'appointments#history', as: :appointment_history

  # публичные страницы на отмену записи к врачу
  get '/cancel_appointment/step1' => 'cancel_appointments#step1', as: :cancel_appointment_step1
  match '/cancel_appointment/step2' => 'cancel_appointments#step2', as: :cancel_appointment_step2
  match '/cancel_appointment/history' => 'cancel_appointments#history', as: :cancel_appointment_history
  match '/cancel_appointment/auth' => 'cancel_appointments#passport_auth', as: :cancel_appointment_auth
  match '/cancel_appointment/claim_for_refusal' => 'cancel_appointments#claim_for_refusal', as: :cancel_appointment_claim_for_refusal

  # публичные страницы для вызова врача на дом
  get '/doctor_requests/person' => 'doctor_requests#person', as: :doctor_requests_person
  match '/doctor_requests/hospital' => 'doctor_requests#hospital', as: :doctor_requests_hospital
  match '/doctor_requests/form' => 'doctor_requests#form', as: :doctor_requests_form
  post '/doctor_requests/create' => 'doctor_requests#create', as: :doctor_requests
  get '/doctor_requests/success' => 'doctor_requests#success', as: :doctor_requests_success
  get '/doctor_requests/apology' => 'doctor_requests#apology', as: :doctor_requests_apology
  get '/doctor_requests/rostovskaya_all_live' => 'doctor_requests#all_live', as: :doctor_requests_all_live
  get '/doctor_requests/list' => 'doctor_requests#list', as: :doctor_requests_list
  put '/doctor_requests/:id' => 'doctor_requests#update', as: :doctor_request
  get '/doctor_requests/:check_free_coupons' => 'doctor_requests#check_free_coupons', as: :doctor_request_check_free_coupons


  resources :reviews, only: [:index, :create]
  resources :articles
  
  match '/load_articles' => 'articles#index'
  
  resources :services


  mount Ckeditor::Engine => '/ckeditor'

  resources :abouts

  resources :offices

  # admin area
  ActiveAdmin.routes(self)
  post '/admin/reviews/:id/answer' => 'admin/reviews#answer', as: :admin_review_review_answers
  # =================

  devise_for :admin_users, ActiveAdmin::Devise.config
  
  root :to => 'static_pages#home'
  get 'poll_of_the_year' => 'static_pages#poll_of_the_year', as: :poll_of_the_year
  get 'jci' => 'static_pages#jci', as: :jci
  # страница с марафоном и записью к врачу по 4ое июля
  # get 'runners' => 'static_pages#runners', as: :runners
  get 'polls/:id' => 'static_pages#polls'
  get 'p/:id' => 'static_pages#external_poll', as: :external_poll

  resources :news

  post 'set_region', to: 'application#set_region'
  post 'update_office', to: 'offices#update_pos'

  resources :doctors, only: [:index, :show], as: :staff do
    member do
      post 'like'
    end
  end

  resources :vacancies, only: [:index, :show]
  resource :vacancy_requests, only: [:create]
  resource :visit_requests, only: [:create]
end
