# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if Rails.env.development?
  r = Region.where(name: 'test region', active: true, phone: '812 300 00 00').first_or_create
  Office.where(title: 'test region', region_id: r.id).first_or_create

  Rake::Task["csv:import_service_categories"].invoke
  Rake::Task["services:assign_category"].invoke

  if AdminUser.where(email: 'admin@example.com').first.present?
    AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password', role: 'superadmin')
  end
end
