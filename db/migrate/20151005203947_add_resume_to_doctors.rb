class AddResumeToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :resume, :text
  end
end
