class CreateVisitRequests < ActiveRecord::Migration
  def up
    create_table :visit_requests do |t|
      t.string :name
      t.string :birthday
      t.string :phone
      t.string :email

      t.timestamps
    end
  end

  def down
    drop_table :visit_requests
  end
  
end
