class RenameIdPolisomsInDoctorsApiOffices < ActiveRecord::Migration
  def up
    rename_column :doctors_api_offices, :id_polisoms, :polisoms_id
  end

  def down
    rename_column :doctors_api_offices, :polisoms_id, :id_polisoms
  end
end
