class AddRegionInvite < ActiveRecord::Migration
  def up
    add_column :regions, :invite, :string
    add_column :regions, :appointment_link, :string
  end

  def down
  end
end
