class AddOnSiteToReviewAnswers < ActiveRecord::Migration
  def change
    add_column :review_answers, :on_site, :boolean, default: false
    remove_column :review_answers, :kind
  end
end
