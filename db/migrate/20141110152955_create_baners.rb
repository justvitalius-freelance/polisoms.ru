class CreateBaners < ActiveRecord::Migration
  def change
    create_table :baners do |t|
      t.belongs_to :region
      t.string :title
      t.string :body
      t.string :link
      t.attachment :photo
      t.timestamps
    end
  end
end
