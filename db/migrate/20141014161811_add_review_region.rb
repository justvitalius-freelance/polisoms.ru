class AddReviewRegion < ActiveRecord::Migration
  def up
    add_column :reviews, :region_id, :integer
  end

  def down
    remove_column :reviews, :region_id
  end
end
