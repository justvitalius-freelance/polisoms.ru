class AddOfficeRegion < ActiveRecord::Migration
  def up
    add_column :offices, :region_id, :integer
  end

  def down
    remove_column :offices, :region_id
  end
end
