class AddVoters < ActiveRecord::Migration
  def up
    create_table :voters do |t|
      t.string :ip
      t.timestamps
    end
  end

  def down
    drop_table :voters
  end
end
