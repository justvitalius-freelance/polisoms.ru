class AddAddressToDoctorsApiOffices < ActiveRecord::Migration
  def change
    add_column :doctors_api_offices, :address, :text
  end
end
