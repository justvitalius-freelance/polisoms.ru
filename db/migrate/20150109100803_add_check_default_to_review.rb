class AddCheckDefaultToReview < ActiveRecord::Migration
  def up
    change_column_default :reviews, :check, false
  end

  def down
    change_column_default :reviews, :check, nil
  end
end
