class CreateCallDoctorHomeDistrictTimes < ActiveRecord::Migration
  def up
    create_table :call_doctor_home_district_times do |t|
      t.string :district_id
      t.time :call_home_time_start, null: false, default: Time.new.beginning_of_day
      t.time :call_home_time_end, null: false, default: Time.new.end_of_day
      t.text :end_call_home_time_text
      t.string :district_title
      t.boolean :actived, default: true

      t.timestamps
    end
  end

  def down
    drop_table :call_doctor_home_district_times
  end
end
