class ChangeDoctorsApiOfficesEncoding < ActiveRecord::Migration
  def change
    execute "ALTER TABLE doctors_api_offices CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
  end

end
