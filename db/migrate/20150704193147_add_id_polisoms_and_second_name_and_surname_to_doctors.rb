class AddIdPolisomsAndSecondNameAndSurnameToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :id_polisoms, :string
    add_column :doctors, :second_name, :string
    add_column :doctors, :surname, :string
  end
end
