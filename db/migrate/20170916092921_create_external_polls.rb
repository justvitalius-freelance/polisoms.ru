class CreateExternalPolls < ActiveRecord::Migration
  def up
    create_table :external_polls do |t|
      t.text :url
      t.text :comment
      t.boolean :active, default: true

      t.timestamps
    end
  end

  def down
    drop_table :external_polls
  end
end
