class CreateCallDoctorHomeCoupons < ActiveRecord::Migration
  def up
    create_table :call_doctor_home_coupons do |t|
      t.string :lpu_id
      t.text :lpu_description
      t.integer :amount

      t.timestamps
    end
  end

  def down
    drop_table :call_doctor_home_coupons
  end
end
