class AddWorkTimeTextToRegions < ActiveRecord::Migration
  def change
    add_column :regions, :work_time, :text
  end
end
