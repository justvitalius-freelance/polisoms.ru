class ChangePolisomsIdToInteger < ActiveRecord::Migration
  def up
    change_column :doctors_api_offices, :polisoms_id, :integer
  end

  def down
    change_column :doctors_api_offices, :polisoms_id, :string
  end
end
