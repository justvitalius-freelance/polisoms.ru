class AddPhoneToCallDoctorHomeDistrictTimes < ActiveRecord::Migration
  def change
    add_column :call_doctor_home_district_times, :phone, :string
  end
end
