class AddDoctorRequests < ActiveRecord::Migration
  def change
    create_table :doctor_requests do |t|
      t.string :full_name
      t.date :birthday
      t.string :department_name
      t.integer :department_id
      t.string :region_name
      t.integer :region_id
      t.string :phone_number
      t.string :additional_phone_number
      t.text :address
      t.datetime :visit_at

      t.timestamps
    end
  end

end
