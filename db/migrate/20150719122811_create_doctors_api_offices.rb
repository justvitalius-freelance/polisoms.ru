class CreateDoctorsApiOffices < ActiveRecord::Migration
  def up
    create_table :doctors_api_offices do |t|
      t.string :title
      t.string :id_polisoms
      t.timestamps
    end
  end

  def down
    drop_table :doctors_api_offices
  end
end
