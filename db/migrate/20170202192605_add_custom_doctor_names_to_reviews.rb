class AddCustomDoctorNamesToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :custom_doctor_names, :string
  end
end
