class AddCalledAndCommentToDoctorRequests < ActiveRecord::Migration
  def change
    add_column :doctor_requests, :called, :boolean, default: false
    add_column :doctor_requests, :comment, :text
  end
end
