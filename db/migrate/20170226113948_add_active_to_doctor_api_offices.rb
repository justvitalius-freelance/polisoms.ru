class AddActiveToDoctorApiOffices < ActiveRecord::Migration
  def change
    add_column :doctors_api_offices, :active, :boolean, default: false
  end
end
