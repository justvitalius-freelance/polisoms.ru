class BanerAddPos < ActiveRecord::Migration
  def up
    add_column :baners, :pos, :integer
  end

  def down
    remove_column :baners, :pos
  end
end
