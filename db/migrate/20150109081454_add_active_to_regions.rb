class AddActiveToRegions < ActiveRecord::Migration
  def change
    add_column :regions, :active, :boolean, default: false

    Region.first.try(:update_attributes, active: true)
  end


end
