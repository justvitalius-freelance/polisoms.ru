class AddAboutRegion < ActiveRecord::Migration
  def up
    add_column :abouts, :region_id, :integer
  end

  def down
    remove_column :abouts, :region_id
  end
end
