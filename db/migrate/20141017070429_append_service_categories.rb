#encoding: utf-8
class AppendServiceCategories < ActiveRecord::Migration
  def migrate(direction)
    super
    # Create a default
    ServiceCategory.create!(:title => 'Диспансеризация') if direction == :up
    ServiceCategory.create!(:title => 'Услуги') if direction == :up
  end
end
