class AddExtraTypeToServices < ActiveRecord::Migration
  def up
    add_column :services, :extra_type, :string
  end

  def down
    remove_column :services, :extra_type
  end
end
