class CreateReviewAnswers < ActiveRecord::Migration
  def change
    create_table :review_answers do |t|
      t.text :text
      t.string :kind
      t.integer :review_id

      t.timestamps
    end
  end
end
