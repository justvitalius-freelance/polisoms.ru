class AddExtraTypeToAbouts < ActiveRecord::Migration
    def up
    add_column :abouts, :extra_type, :string
  end

  def down
    remove_column :abouts, :extra_type
  end
end
