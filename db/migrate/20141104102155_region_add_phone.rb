class RegionAddPhone < ActiveRecord::Migration
  def up
    add_column :regions, :phone, :string, :limit => 30
  end

  def down
    remove_column :regions, :phone
  end
end
