class AddCategoryIdToVacancies < ActiveRecord::Migration
  def change
    add_column :vacancies, :category_id, :integer, default: 1
  end
end
