class OfficeAddLatLon < ActiveRecord::Migration
  def up
    add_column :offices, :lat, :float
    add_column :offices, :lon, :float
  end

  def down
  end
end
