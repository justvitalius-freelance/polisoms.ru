class AddSubjectToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :subject, :string
  end
end
