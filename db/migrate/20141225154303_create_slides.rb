class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.string :title
      t.text :text
      t.string :link_title
      t.string :link_url
      t.integer :position
      t.attachment :image

      t.timestamps
    end
  end
end
