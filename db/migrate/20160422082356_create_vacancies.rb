class CreateVacancies < ActiveRecord::Migration
  def up
    create_table :vacancies do |t|
      t.string :title
      t.text :description
      t.belongs_to :region
      t.attachment :image
      t.boolean :active, default: true

      t.timestamps
    end
  end

  def down
    drop_table :vacancies
  end
end
