class CreateDefaultUser < ActiveRecord::Migration
  def migrate(direction)
    super
    # Create a default user
    AdminUser.create!(:email => 'admin@example.com', :password => 'password', :password_confirmation => 'password', :role=>"superadmin") if direction == :up
  end
end
