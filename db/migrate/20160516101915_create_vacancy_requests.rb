class CreateVacancyRequests < ActiveRecord::Migration
  def up
    create_table :vacancy_requests do |t|
      t.string :name
      t.string :phone
      t.string :email

      t.integer :vacancy_id
      t.timestamps
    end
  end

  def down
    drop_table :vacancy_requests
  end
end
