class AddRegionIdToSlides < ActiveRecord::Migration
  def change
    add_column :slides, :region_id, :integer
  end
end
