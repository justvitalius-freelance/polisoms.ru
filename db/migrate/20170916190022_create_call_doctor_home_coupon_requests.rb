class CreateCallDoctorHomeCouponRequests < ActiveRecord::Migration
  def up
    create_table :call_doctor_home_coupon_requests do |t|
      t.string :lpu_id
      t.string :patient_id

      t.timestamps
    end
  end

  def down
    drop_table :call_doctor_home_coupon_requests
  end
end
