class AddRegionIdToServices < ActiveRecord::Migration
  def up
    add_column :services, :region_id, :integer
  end

  def down
    remove_column :services, :region_id
  end
end
