# coding: utf-8
class ReviewsMailer < ActionMailer::Base
  default from: 'robot@polisoms.ru'
  default to: %w(justvitalius@gmail.com) if Rails.env.development?
  default to: %w(makarenko@polisoms.ru info@polisoms.ru) if Rails.env.production?
  default to: %w(justvitalius@gmail.com makarenko@polisoms.ru info@polisoms.ru) if Rails.env.staging?


  #  оповещение админа о поступлении нового отзыва
  def admin_notify(review)
    subject = 'Новый отзыв'
    subject += ' на тестовом сайте beta.polisoms.ru' if Rails.env.staging?
    subject += ' на сайте www.polisoms.ru' if Rails.env.production?
    @review = review
    Delayed::Worker.logger.debug("Will send admin notify about review with id=#{@review.id}")
    mail subject: subject
  end


  #  кастомный ответ на отзыв
  def answer(review, answer, to, subject)
    @answer_text = answer
    Delayed::Worker.logger.debug("Will send answer on review with id=#{review.id}")
    mail subject: subject,
         to: to,
         template_name: 'answer'
  end

end
