class Api::ReviewsController < ApplicationController

  respond_to :json

  def index
    reviews = Review.order('created_at DESC').includes(:doctor).includes(:region)
    respond_to do |format|
      format.json { render json: { reviews: reviews.map { |r| mapped_review(r) } } }
    end
  end

  def create
    @review = ReviewMapper.new(
        params[:review].merge({ region_id: current_region.id })
    ).to_review
    respond_to do |format|
      if @review.save
        ReviewsMailer.delay.admin_notify(@review) unless Rails.env.development?
        format.json { render json: { status: 'success' } }
      else
        format.json { render json: { status: 'error' } }
      end
    end
  end

  private

  def mapped_review(review)
    doctors = Doctor.where(id: review.doctors.split(',')) if review.doctors.present?
    office = review.office

    hash = {
        id: review.id,
        author: review.author,
        author_contact: review.contact,
        review_text: review.description,
        review_status: review.status,
        created_at: review.created_at
    }

    if office
      hash[:office] = {
          id: office.id,
          id_polisoms: office.polisoms_id,
          title: office.title
          # region_id: 2,
          # region_title: office.region.try(:name)
      }
    end

    unless doctors.blank?
      hash[:doctors] = doctors.map do |doctor|
        {
            id: doctor.id,
            id_polisoms: doctor.id_polisoms.to_i,
            name: doctor.full_name,
            job: doctor.job
        }
      end
    end

    hash

  end

end
