class VacanciesController < ApplicationController

  def index
    @collection = Vacancy.where(region_id: current_region.id, active: true).order('created_at DESC')
  end

  def show
    @resource = Vacancy.find(params[:id])
    @vacancy_request = VacancyRequest.new
    @vacancy_request.vacancy = @resource
  end

end
