class VisitRequestsController < ApplicationController

  def create
    @request = VisitRequest.new(params[:visit_request])
    if @request.save
      redirect_to runners_path, notice: 'Заявка принята. Мы свяжемся с вами по указанным в форме контактам'
    else
      redirect_to runners_path, error: 'Не удалось отправить заявку, попробуйте еще раз'
    end
  end

end
