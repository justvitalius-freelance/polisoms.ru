class VacancyRequestsController < ApplicationController

  def create
    @request = VacancyRequest.new(params[:vacancy_request])
    if @request.save
      redirect_to vacancy_path(params[:vacancy_request][:vacancy_id]), notice: 'Заявка принята. Мы свяжемся с вами по указанным в форме контактам'
    else
      redirect_to vacancies_path, error: 'Не удалось отправить заявку, попробуйте еще раз'
    end
  end

end
