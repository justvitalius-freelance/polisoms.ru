# encoding: utf-8
class ApiAppointmentsController < ApplicationController
  before_filter :create_appointment, except: :index


  def districts
    data = @appointment.districts.map do |district|
      {
          title: district[:district_name],
          id: district[:id_district]
      }
    end
    render json: { districts: data }
  end


  def polyclinics
    data = []
    if params[:district_id]
      data = _polyclinics.map do |polyclinic|
        {
            title: polyclinic[:lpu_full_name],
            id: polyclinic[:id_lpu]
        }
      end
    end

    render json: { polyclinics: data }
  end


  def specialities
    data = _specialities.map do |speciality|
      {
          id: speciality[:id_spesiality],
          title: speciality[:name_spesiality]
      }
    end
    render json: { specialities: data }
  end


  def doctors
    data = _doctors.map do |doctor|
      {
          id: doctor[:id_doc],
          title: doctor[:name],
          availability: doctor[:count_free_ticket].try(:to_i) || 0
      }
    end

    respond_to do |format|
      format.json { render json: { doctors: data } }
      format.js {
        @doctors = data
        render 'appointments/doctors_list'
      }
    end


  end


  private

  def create_appointment
    @appointment = Appointment.new
  end

  def _polyclinics
    @_polyclinics ||= @appointment.get_lpu_list(params[:district_id])
  end

  def _doctors
    @_doctors ||= @appointment.get_doctor_list(params[:speciality_id], params[:lpu_id], params[:patient_id])
  end

  def _specialities
    @_specialities ||= @appointment.get_spesiality_list(params[:lpu_id], params[:parient_id])
  end


end
