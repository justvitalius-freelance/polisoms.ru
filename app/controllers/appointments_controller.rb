# encoding: utf-8
class AppointmentsController < ApplicationController
  before_filter :create_appointment, :check_region

  def step1
  end

  def step2
    params.delete(:action)
    @back_path = appointment_step1_path(params)
  end

  # валидируем пациента и в случае успеха показываем третий шаг
  def step3
    begin
      init_birthday
      params.delete(:action)
      @back_path = appointment_step1_path(params)
      patient = @appointment.check_patient(params[:lpu_id], params[:first_name], params[:last_name], params[:birthday])
      if patient.blank?
        flash[:error] = 'Уважаемый Пациент! К сожалению, мы не смогли найти Вас в нашей базе данных. Возможно, Вы никогда ранее не обращались в наши центры, либо же допустили ошибку при вводе информации. Пожалуйста, попробуйте ввести данные снова, либо же обратитесь в наш колл-центр по телефону в Санкт-Петербурге 670 00 03 с 8.00 до 20.00 ежедневно. В период с 11.00 до 16.00 наш колл-центр загружен менее всего.'
        redirect_to @back_path
      else
        params[:patient_id] = patient
      end
    rescue Savon::SOAPFault, ArgumentError, TypeError
      flash[:error] = 'Введенные ранее данные устарели, пожалуйста, начните с начала'
      redirect_to appointment_step1_path(params)
    end
  end


  # показываем список возможных дат для записи
  def step4
    begin
    params.delete(:action)
    @back_path = appointment_step3_path(params)
    # если дату передаем, то
    # 1. определяем календарные границы этой даты (с учетом не ранее сегодняшней даты)
    # 2. ищем по календарной дате все талоны
    # 3. сортируем талоны по дате
    # 4. убеждаемся,что переданная дата соответствует одной из тех, где доступны талоны
    # 5. вытаскиваем талоны по переданной дате
    # 6. если переданной даты нет в списке, то берем первую из списка
    # 7. если талонов на этот месяц нет, то в @date ставим начало месяца
    if params[:date]

      # нельзя выдавать дату старта поиска ранее, чем сегодня
      start_date = if Date.parse(params[:date]).beginning_of_month == Date.today.beginning_of_month
                     Date.today
                   else
                     Date.parse(params[:date]).beginning_of_month
                   end

      end_date = Date.parse(params[:date]).end_of_month
      search_date = Date.parse(params[:date])

      @all_tickets_on_month = @appointment.get_avaible_appointments(params[:doctor_id], params[:lpu_id], start_date, end_date).map { |ap| Appointments::RecordDay.new(ap) }

      if @all_tickets_on_month.any?
        # группируем записи по дате. Т.к.из БД приходит уже отсортированный массив,
        # то первый ключ в сгрупированном списке и будет ближайшей датой
        @grouped_tickets_by_date = @all_tickets_on_month.group_by { |x| x.visit_start.to_date }

        if @grouped_tickets_by_date.keys.include?(search_date.to_date)
          @date = search_date
        else
          @date = @grouped_tickets_by_date.keys.first
        end

        @earliest_tickets = @grouped_tickets_by_date[@date.to_date]
      else
        @date = start_date
      end

    # если дату не передаем
    # 1. определяем текущую дату
    # 2. определяем календарные границы от сегодняшней даты до конца месяца
    # 3. по календарным границам ищем все талоны
    # 4. если талоны есть, то
    # 5. группируем талоны по времени
    # 6. берем первую из списка дату и талоны по ней
    else
      start_date = Date.today
      end_date = start_date.end_of_month

      @all_tickets_on_month = @appointment.get_avaible_appointments(params[:doctor_id], params[:lpu_id], start_date, end_date).map { |ap| Appointments::RecordDay.new(ap) }

      if @all_tickets_on_month.any?
        # группируем записи по дате. Т.к.из БД приходит уже отсортированный массив,
        # то первый ключ в сгрупированном списке и будет ближайшей датой
        @grouped_tickets_by_date = @all_tickets_on_month.group_by { |x| x.visit_start.to_date }

        @date = @grouped_tickets_by_date.keys.first
        @earliest_tickets = @grouped_tickets_by_date[@date.to_date]
      else
        # если ничего не нашли, то выставляем @date в сегодняшнюю дату, чтобы отрисовать календарь
        @date = start_date
      end
    end

    # записываем значение в visit_date для отображения в интерфейсе
    params[:visit_date] = I18n.l @date, format: :full

    rescue Savon::SOAPFault, ArgumentError, TypeError
      flash[:error] = 'Введенные ранее данные устарели, пожалуйста, начните с начала'
      redirect_to appointment_step1_path(params)
    end
  end

  def check
    params.delete(:action)
    @back_path = appointment_step4_path(params)
  end


  def confirm
    lpu_id = params[:lpu_id]
    patient_id = params[:patient_id]
    appointment_id = params[:appointment_id]
    @confirmation = @appointment.set_appointment(appointment_id, lpu_id, patient_id).try(:first)
    if @confirmation.try(:[], :success) == false
      flash[:error] = 'Запись не удалась, попробуйте еще раз'
      redirect_to appointment_step3_path(params)
    end
  end

  private

  def create_appointment
    @appointment = Appointment.new
  end


  def init_birthday
    params[:birthday] = unless params['birthday(1i)'].blank? && params['birthday(2i)'].blank? && params['birthday(3i)'].blank?
                          "#{params['birthday(1i)']}-#{params['birthday(2i)']}-#{params['birthday(3i)']}"
                        else
                          nil
                        end
  end


  def init_doctor
    @doctor ||= @appointment.get_doctor_list(params[:speciality_id], params[:lpu_id], params[:patient_id]).select { |d| d[:id_doc] == params[:doctor_id].to_s }.first
  end

  def init_test_params
    params[:district_id] ||= '15'
    params[:lpu_id] ||= '430'
    params[:lpu_title] ||= '__lpu '
    params[:first_name] ||= 'Минеев'
    params[:last_name] ||= 'Виталий'
    params[:middle_name] ||= 'Васильевич'
    params['birthday(3i)'] ||= '07'
    params['birthday(2i)_month'] ||= '10'
    params['birthday(1i)'] ||= '1988'
    params[:birthday] ||= "#{params['birthday(1i)']}-#{params['birthday(2i)_month']}-#{params['birthday(3i)']}"
    params[:speciality_id] ||= '1'
    params[:patient_id] ||= '265335'
    params[:doctor_id] ||= '4'
    params[:doctor_name] ||= '__doctor-name'
    params[:visit_start] ||= Date.today.beginning_of_month.strftime('%Y-%m-%d')
    params[:visit_end] ||= Date.today.end_of_month.strftime('%Y-%m-%d')
    params[:appointment_id] ||= 1
    params[:visit_time] ||= '__17:00'
    params[:visit_date] ||= '__2014-01-01'
  end

  def check_region
    redirect_to root_path unless region_has_appointments
  end

end
