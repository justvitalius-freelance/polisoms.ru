# encoding: utf-8
class DoctorRequestsController < ApplicationController
  before_filter :create_appointment, except: [:list, :update]
  before_filter :check_requests_time, only: :form

  http_basic_authenticate_with name: "polisoms", password: "temporarysection", only: :list

  def list
    @filtered_date = process_filtered_date
    filtered_date_start = @filtered_date.beginning_of_day
    filtered_date_end = @filtered_date.end_of_day
    @doctor_requests = DoctorRequest
                           .order('created_at DESC')
                           .where('created_at >= ?', filtered_date_start)
                           .where('created_at < ?', filtered_date_end)

    render layout: false
  end

  def update
    @request = DoctorRequest.find(params[:id])
    @request.update_attributes(params[:doctor_request])
    render json: @request
  end

  def person
    @district_collection = CallDoctorHomeDistrictTime.order(district_title: :asc)
    @show_districts_info = @district_collection.present?
  end

  def hospital
    params.delete(:action)
    @back_path = doctor_requests_person_path(params)
  end

  # валидируем пациента и в случае успеха показываем третий шаг
  def form
    begin
      init_birthday
      params.delete(:action)
      @back_path = doctor_requests_hospital_path(params)
      patient = @appointment.check_patient(params[:lpu_id], params[:first_name], params[:last_name], params[:birthday])
      if patient.blank?
        flash[:error] = 'Пациент с введенными значениями не найден'
        redirect_to doctor_requests_person_path(params)
      else
        params[:patient_id] = patient
      end
    rescue Savon::SOAPFault, ArgumentError, TypeError
      flash[:error] = 'Введенные ранее данные устарели, пожалуйста, начните с начала'
      redirect_to doctor_requests_person_path(params)
    end
  end


  def create
    begin
      requestData = {
        visit_at: visit_at,
        birthday: Date.new(params['birthday(1i)'].to_i, params['birthday(2i)'].to_i, params['birthday(3i)'].to_i),
        full_name: "#{params[:last_name]} #{params[:first_name]} #{params[:middle_name]}",
        address: params[:address],
        phone_number: params[:phone_number],
        additional_phone_number: params[:additional_phone_number],
        department_name: params[:lpu_title],
        department_id: params[:lpu_id],
        region_id: params[:district_id]
      }

      DoctorRequest.create(requestData)
      CallDoctorHomeCouponRequest.new({lpu_id: params[:lpu_id], patient_id: params[:patient_id]}).save!
      redirect_to doctor_requests_success_path(params)
    rescue Exception => e
      puts 'ERROR'
      puts e
      flash[:error] = 'При создании заявки произошла ошибка, пожалуйста, начните с начала'
      redirect_to doctor_requests_person_path(params)
    end
  end

  def apology
    @resource = CallDoctorHomeDistrictTime.where(district_id: params[:district_id].to_s).first
  end

  def all_live
    render 'rostovskaya_all_live'
  end

  def check_free_coupons
    render json: CallDoctorHomeCouponsMediator.new(params[:lpu_id]).check_free_coupons
  end

  private

  def check_requests_time
    is_rostovskaya = params[:district_id].to_s == '16' && params[:lpu_id].to_s == '763'
    redirect_to doctor_requests_all_live_path(params) if is_rostovskaya

    @district_time = CallDoctorHomeDistrictTime.where(district_id: params[:district_id].to_s).first
    if @district_time && !@district_time.can_call_home?
      redirect_to doctor_requests_apology_path(params)
    end

  end

  def process_filtered_date
    date = DateTime.now
    if params[:filtered_date]
      begin
        date = Date.new *params[:filtered_date].each.map { |key, value| value.to_i }.reverse
      rescue
        date = DateTime.now
      end
    end
    date
  end

  def create_appointment
    @appointment = Appointment.new
  end


  def init_birthday
    params[:birthday] = unless params['birthday(1i)'].blank? && params['birthday(2i)'].blank? && params['birthday(3i)'].blank?
                          "#{params['birthday(1i)']}-#{params['birthday(2i)']}-#{params['birthday(3i)']}"
                        else
                          nil
                        end
  end


  def init_doctor
    @doctor ||= @appointment.get_doctor_list(params[:speciality_id], params[:lpu_id], params[:patient_id]).select { |d| d[:id_doc] == params[:doctor_id].to_s }.first
  end

  def init_test_params
    params[:district_id] ||= '15'
    params[:lpu_id] ||= '430'
    params[:lpu_title] ||= '__lpu '
    params[:first_name] ||= 'Минеев'
    params[:last_name] ||= 'Виталий'
    params[:middle_name] ||= 'Васильевич'
    params['birthday(3i)'] ||= '07'
    params['birthday(2i)_month'] ||= '10'
    params['birthday(1i)'] ||= '1988'
    params[:birthday] ||= "#{params['birthday(1i)']}-#{params['birthday(2i)_month']}-#{params['birthday(3i)']}"
    params[:speciality_id] ||= '1'
    params[:patient_id] ||= '265335'
    params[:doctor_id] ||= '4'
    params[:doctor_name] ||= '__doctor-name'
    params[:visit_start] ||= Date.today.beginning_of_month.strftime('%Y-%m-%d')
    params[:visit_end] ||= Date.today.end_of_month.strftime('%Y-%m-%d')
    params[:appointment_id] ||= 1
    params[:visit_time] ||= '__17:00'
    params[:visit_date] ||= '__2014-01-01'
  end

  def visit_at
    year = params['visitAt(1i)']
    month = params['visitAt(2i)']
    day = params['visitAt(3i)']
    if year.present? && month.present? && day.present?
      DateTime.new(year.to_i, month.to_i, day.to_i).at_beginning_of_day
    else
      DateTime.now.at_beginning_of_day
    end
  end

end
