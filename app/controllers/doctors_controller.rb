class DoctorsController < ApplicationController

  def index
    jsonable_offices
    # @is_minithumb = true
    # if params[:office_id]
    #   @is_minithumb = false
    #   @doctors = Doctor.actived.where(polisoms_office_id: params[:office_id].to_i)
    # else
    #   @doctors = Doctor.actived
    # end

    jsonable_doctors
  end

  def show
    @doctor = Doctor.where(id: params[:id]).first
    jsonable_offices
    jsonable_doctors
    redirect_to staff_index_path unless @doctor
  end

  def like
    @doctor = Doctor.where(id: params[:id]).first
    if @doctor
      voter = Voter.where(ip: current_ip).first_or_create!
      voter.likes @doctor
    end

    respond_to do |format|
      format.json do
        jsonable_doctors
        render json: @doctors
      end
      format.js
    end
  end

private

  def current_ip
    request.ip
  end

  def jsonable_doctors
    @doctors = @doctors = Doctor.actived.map do |doctor|
      {
          id: doctor.id,
          firstName: doctor.name,
          secondName: doctor.second_name,
          lastName: doctor.surname,
          jobTitle: doctor.job,
          officeId: doctor.polisoms_office_id,
          votes: doctor.votes_for.size,
          fullName: doctor.full_name,
          searchStr: doctor.full_name,
          avatarPath: doctor.photo.url(:avatar) || '/assets/default_doctor_avatar.png',
          profilePath: staff_path(doctor),
          likePath: like_staff_path(doctor.id),
          lastUpdatedAt: DateTime.now.to_i
      }
    end
  end

  def jsonable_offices
    @offices = DoctorsApiOffice.actived.order('title ASC').map do |o|
      o.title = o.label
      o
    end
  end

end
