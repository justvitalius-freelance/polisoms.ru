# encoding: utf-8
class CancelAppointmentsController < ApplicationController
  before_filter :create_appointment, :check_region

  def step1
  end

  def step2
    params.delete(:action)
    @back_path = cancel_appointment_step1_path(params)
  end

  # авторизация по паспорту
  # в случае успеха удаляем данные из общих параметров и редиректим на историю
  def passport_auth
    begin
      init_birthday
      @back_path = cancel_appointment_step1_path(params)
      patient_id = @appointment.check_patient(params[:lpu_id], params[:first_name], params[:last_name], params[:birthday])
      if patient_id.blank?
        flash[:error] = 'Уважаемый Пациент! К сожалению, мы не смогли найти Вас в нашей базе данных. Возможно, Вы никогда ранее не обращались в наши центры, либо же допустили ошибку при вводе информации. Пожалуйста, попробуйте ввести данные снова, либо же обратитесь в наш колл-центр по телефону в Санкт-Петербурге 670 00 03 с 8.00 до 20.00 ежедневно. В период с 11.00 до 16.00 наш колл-центр загружен менее всего.'
        redirect_to @back_path
      else
        params[:patient_id] = patient_id
        lpu_id = params[:lpu_id]
        patient_id = params[:patient_id]
        doc_series = params[:doc_series]
        doc_number = params[:doc_number]
        if doc_series && doc_number
          @confirmation = @appointment.check_patient_by_passport(lpu_id, patient_id, doc_series, doc_number)
          params.delete(:doc_series)
          params.delete(:doc_number)
          params.delete(:action)
          if @confirmation
            redirect_to cancel_appointment_history_path(params)
          else
            flash[:error] = 'Пациент с указанными паспортными данными не найден.'
            redirect_to cancel_appointment_auth_path(params)
          end
        end
      end
    rescue Savon::SOAPFault, ArgumentError, TypeError
      flash[:error] = 'Введенные ранее данные устарели, пожалуйста, начните с начала'
      redirect_to cancel_appointment_step1_path(params)
    end
  end


  # по дизайну нужно взять район и больницу.
  # но судя по документации, этого не взять этим методом
  def history
    params.delete(:action)
    @back_path = cancel_appointment_step2_path(params)
    @history = @appointment.get_patient_history(params[:lpu_id], params[:patient_id])
    @pastHistory = @history.map{ |item| item if item[:visit_start] < DateTime.now }
                       .compact
                       .sort{ |a, b| b[:visit_start] <=> a[:visit_start]}
    @futureHistory = @history.map{ |item| item if item[:visit_start] > DateTime.now }
                         .compact
                         .sort { |a, b| b[:visit_start] <=> a[:visit_start] }
  end

  def claim_for_refusal
    appointment_id = params.delete(:id_appointment)
    result = @appointment.create_claim_for_refusal(params[:lpu_id], params[:patient_id], appointment_id)
    if result.try(:[], 0)[:success]
      flash[:error] = 'Вы успешно отменили запись к врачу'
    else
      flash[:error] = 'При отмене записи произошла ошибка. Пожалуйста, свяжитесь с нами по телефону +7 (812) 670 00 03'
    end
    redirect_to cancel_appointment_history_path(params)
  end

  private

  def create_appointment
    @appointment = Appointment.new
  end


  def init_birthday
    params[:birthday] = unless params['birthday(1i)'].blank? && params['birthday(2i)'].blank? && params['birthday(3i)'].blank?
                          "#{params['birthday(1i)']}-#{params['birthday(2i)']}-#{params['birthday(3i)']}"
                        else
                          nil
                        end
  end


  def init_doctor
    @doctor ||= @appointment.get_doctor_list(params[:speciality_id], params[:lpu_id], params[:patient_id]).select { |d| d[:id_doc] == params[:doctor_id].to_s }.first
  end

  def check_region
    redirect_to root_path unless region_has_appointments
  end

end
