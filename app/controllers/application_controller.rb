class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_region,
                :region_session_present?,
                :region_session_geolocation?,
                :offices_for_reviews,
                :offices_for_review,
                :doctors_for_review,
                :region_has_appointments

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to admin_dashboard_path, :alert => exception.message
  end

  def current_ability
    @current_ability ||= Ability.new(current_admin_user)
  end

  def set_region
    if params[:id]
      @region = Region.find(params[:id])
      logger.debug('#################')
      logger.debug(@region.inspect)
      logger.debug('#################')
      session[:region_id] = @region.try(:id)
      session[:region_name] = @region.try(:name)
    elsif params[:name]
      @region = Region.find_by_name(params[:name])
      session[:region_name] = @region.try(:name)
      session[:region_id] = @region.try(:id)
    end
    render 'layouts/set_region'
    # redirect_to :back
  end

  protected

  def current_region
    if region_session_present?
      Region.find session[:region_id]
    else
      Region.actived.first
    end
  end

  def region_session_present?
    session[:region_id]!=nil
  end

  def region_session_geolocation?
    session[:region_name]!=nil
  end

  def offices_for_reviews
    @office_select ||= session[:region_id] == 1 ?
        DoctorsApiOffice.order('title ASC') :
        Office.where(region_id: session[:region_id])
  end

  def offices_for_review
    if session[:region_id] == 1
      @offices_for_review ||= DoctorsApiOffice.actived.order('title ASC').map do |office|
        {
            id: office.id,
            name: office.label
        }
      end
    else
      @office_select ||= Office.where(region_id: session[:region_id]).map do |office|
        {
            id: office.id,
            name: office.adress,
        }
      end
    end
  end

  def doctors_for_review
    if session[:region_id] == 1
      @doctors_for_review ||= Doctor.actived.map do |doctor|
        {
          id: doctor.id,
          officeId: doctor.polisoms_office_id,
          fullName: doctor.full_name,
        }
      end
    else
      @doctors_for_reviews = []
    end
  end


  def region_has_appointments
    current_region.id == 1
  end

end
