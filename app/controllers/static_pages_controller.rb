class StaticPagesController < ApplicationController
  def home
    @review = Review.new
    @articles = Article.joins(:office).where('offices.region_id=?', current_region.id).order('created_at DESC').limit(3)
    @slides = Slide.where(region_id: current_region.id).order('position ASC')
  end
  
  def appointment
    @other = Article.limit(3)
  end

  def poll_of_the_year
  end

  def jci
  end

  # страница с марафоном и записью к врачу по 4ое июля
  # def runners
  #   @visit_request = VisitRequest.new
  # end

  def polls
    @id = params[:id]
    @iframe_path = "http://polismobile0.1gb.ru?idRecord=#{@id}"
  end

  def external_poll
    @poll = ExternalPoll.where(id: params[:id]).first
    if @poll.try(:active)
      @iframe_path = @poll.url
    else
      redirect_to '/404'
    end
  end

end
