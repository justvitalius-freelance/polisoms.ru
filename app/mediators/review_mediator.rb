class ReviewMediator

  attr_accessor :review, :message

  def initialize(review=nil)
    @review = review
    @message = {}
  end

  def default_answer(type='1', view_context)
    if @review
      default_text = build_default_text type, view_context
      answer = build_review_answer default_text
      if answer.save
        Rails.logger.debug "Default answer with type=#{type} was created for Review id=#{@review.id}"
        if @review.has_email?
          send_email_to_author answer.text, build_email_subject('Вы оставили отзыв')
          @message[:notice] = "Отправлен автоматический ответ №#{type} на email #{@review.contact} к отзыву <a href='/admin/reviews/#{@review.id}'>№#{@review.id}</a>".html_safe
        else
          @message[:alert] = "Отзыв <a href='/admin/reviews/#{@review.id}'>№#{@review.id}</a> не имеет email, ответ не может быть отправлен".html_safe
        end
      else
        @message[:alert] = "При создании ответа возникла ошибка. Попробуйте еще раз"
      end
    end
    answer
  end


  def answer_with(text='', on_site=false)
    if text && @review
      answer = build_review_answer transformed_text(text), on_site
      if answer.save
        Rails.logger.debug "Custom answer was created for Review id=#{@review.id}"
        if @review.has_email?
          send_email_to_author answer.text, build_email_subject('Вы оставили отзыв')
          @message[:notice] = "Ответ отправлен автору на email #{@review.contact}"
        else
          @message[:alert] = "Ответ создан, но у автора нет email, чтобы получить его"
        end
      else
        @message[:alert] = "При создании ответа возникла ошибка. Попробуйте еще раз"
      end
    end
    answer
  end

private

  def build_review_answer(text='', on_site)
    answer = @review.answers.build(text: text)
    answer.on_site = on_site
    answer
  end

  def send_email_to_author(text, subject)
    if @review.has_email?
      Rails.logger.debug "Review id=#{@review.id} has email and we will send an answer to it author"
      ReviewsMailer.delay.answer(@review, text, @review.contact, subject)
      return true
    else
      Rails.logger.debug "Review id=#{@review.id} hasn't email and we will not send email to it author"
      return false
    end
  end


  def build_email_subject(subject='')
    subject += ' на девелоперской версии polisoms' if Rails.env.development?
    subject += ' на тестовом сайте beta.polisoms.ru' if Rails.env.staging?
    subject += ' на сайте www.polisoms.ru' if Rails.env.production?

    subject
  end

  def build_default_text(type, view_context)
    template_name = "#{@review.status.underscore}_#{type}"
    view_context.render_to_string("reviews_mailer/texts/_#{template_name}", layout: false,  locals: { review: @review })
  end


  def transformed_text(text)
    text.gsub("\n", "<br />").html_safe
  end

end