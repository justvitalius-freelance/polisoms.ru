class CallDoctorHomeCouponsMediator

  attr_accessor :lpu_id

  def initialize(lpu_id=nil)
    @lpu_id = lpu_id
    @requested_coupons = CallDoctorHomeCouponRequest.today(@lpu_id)
    @lpu_coupons = CallDoctorHomeCoupon.where(lpu_id: @lpu_id).first
  end

  def check_free_coupons
    # return { result: true }
    begin
      if lpu_has_fixed_coupons
        { free: free_coupons, result: can_call_doctor }
      else
        { result: true }
      end
    rescue Exception => e
      puts "Error"
      puts e
      { result: false }
    end
  end


  private
  def request_coupons_amount
    @requested_coupons.length
  end

  def coupons_amount
    @lpu_coupons.try(:amount)
  end

  def lpu_has_fixed_coupons
    @lpu_coupons != nil
  end

  def can_call_doctor
    coupons_amount !=0 && coupons_amount > request_coupons_amount
  end

  def free_coupons
    if can_call_doctor
      coupons_amount - request_coupons_amount
    else
      0
    end
  end

end
