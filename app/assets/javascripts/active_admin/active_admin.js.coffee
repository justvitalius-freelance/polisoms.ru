$ ->
  window.ActiveAdmin =
    params :
      district_id : null # кэш для выбранного района
      district_title : null # кэш для названия выбранного района
      polyclinic_id : null # кэш для выбранной больницы
      polyclinic_title : null # кэш для названия выбранной больницы

  A =
    container : '.js-call-doctor-home-coupons-admin'
    district_time_container : '.js-call-home-district-time-admin'
    districts_input : '#js-districts'
    polyclinics_input : '#js-polyclinics'
    lpu_description: '#js-lpu-description'
    district_title: '#js-district-title'


  fillLpuDescription = () ->
    $(A.lpu_description).val(ActiveAdmin.params.district_title + ' / ' + ActiveAdmin.params.polyclinic_title)

  # Выбор района при указании кол-ва купонов, здесь выбрать нужно и район и поликлинику
  districtSelect = (data) ->
    # записываем выбранный id в объект
    ActiveAdmin.params.district_id = data.id
    ActiveAdmin.params.district_title = data.title

    # активируем второе поле для выбора и очищаем ранее выбранное значение
    $(A.polyclinics_input).select2('enable', true).select2('data', '')

    # сбрасываем полное название отделения
    $(A.lpu_description).val('')

    return formatSelection(data)

  polyclinicSelection = (data) ->
    ActiveAdmin.params.polyclinic_id = data.id
    ActiveAdmin.params.polyclinic_title = data.title

    fillLpuDescription()

    return formatSelection(data)


  formatResult = (data) ->
    data.title

  formatSelection = (data) ->
    data.title


  if $(A.container).length

    $(A.districts_input).select2(
      width : 'element'
      minimumResultsForSearch : -1 # скрываем поле поиска
      query : (query) ->
        cachedData = @.cacheDataSource
        if cachedData
          query.callback results : cachedData.districts
          return
        else
          $.ajax
            url : "/api/districts/"
            dataType : "json"
            type : "GET"
            success : (data) =>
              @.cacheDataSource = data
              query.callback results : data.districts
              return

      formatResult : formatResult
      formatSelection : districtSelect
      dropdownCssClass : "bigdrop"
      escapeMarkup : (m) ->
        m
    )


    $(A.polyclinics_input).select2(
      width : 'element'
      minimumResultsForSearch : -1 # скрываем поле поиска
      query : (query) ->
        key = "district_#{ActiveAdmin.params.district_id}"
        cachedData = @.cacheDataSource
        if cachedData && cachedData.hasOwnProperty(key)
          query.callback results : cachedData[key].polyclinics
          return
        else
          $.ajax
            url : "/api/polyclinics/"
            data :
              district_id : ActiveAdmin.params.district_id
            dataType : "json"
            type : "GET"
            success : (data) =>
              @.cacheDataSource ||= {}
              @.cacheDataSource[key] = data
              query.callback results : data.polyclinics
              return


      initSelection : (element, callback) ->
        data =
          id : element.val()
          text : element.val()

        callback data
        return

      formatResult : formatResult
      formatSelection : polyclinicSelection
      dropdownCssClass : "bigdrop"
      escapeMarkup : (m) ->
        m
    ).select2('enable', false)

  if $(A.district_time_container).length
    $(A.districts_input).select2(
      width : 'element'
      minimumResultsForSearch : -1 # скрываем поле поиска
      query : (query) ->
        cachedData = @.cacheDataSource
        if cachedData
          query.callback results : cachedData.districts
          return
        else
          $.ajax
            url : "/api/districts/"
            dataType : "json"
            type : "GET"
            success : (data) =>
              @.cacheDataSource = data
              query.callback results : data.districts
              return

      formatResult : formatResult
      formatSelection : (data) ->
        $(A.district_title).val(data.title)
        return formatSelection(data)
      dropdownCssClass : "bigdrop"
      escapeMarkup : (m) ->
        m
    )
