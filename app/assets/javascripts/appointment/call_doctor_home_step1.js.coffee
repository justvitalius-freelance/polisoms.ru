
$ ->

  A = window.Appointment.call_doctor_home_step1

  districtSelect = (data) ->
    # записываем выбранный id в объект
    Appointment.params.district_id = data.id

    # активируем второе поле для выбора и очищаем ранее выбранное значение
    $(A.polyclinics_input).select2('enable', true).select2('data', '')


    # выключаем кнопку следующего шага в случае, если было что-то выбрано
    Appointment.disable_next_step() if data.id

    # скрываем инфо по купонам при выборе нового отделения
    success_free_coupons_info(false)
    failed_free_coupons_info(false)

    return formatSelection(data)


  polyclinicSelection = (data) ->
    Appointment.params.polyclinic_id = data.id

    # записываем название поликлиники в переменную
    $('input[name="lpu_title"]').val(data.title)

    # запрашиваем кол-во свободных талонов
    check_free_coupons() if data.id

    return formatSelection(data)


  formatResult = (data) ->
    data.title

  formatSelection = (data) ->
    data.title

  check_free_coupons = () ->
    $.ajax
      url : "/doctor_requests/check_free_coupons"
      dataType : "json"
      type : "GET"
      data :
        lpu_id: Appointment.params.polyclinic_id
      success : (data) =>
        toggle_coupons_info(data)
        if data.result
          Appointment.enable_next_step()
        else
          Appointment.disable_next_step()
      error: (data) =>
        Appointment.disable_next_step()
        toggle_coupons_info({ result: false })

  success_free_coupons_info = (show, amount) ->
    if (amount == null || amount == undefined)
      $(A.free_coupons_value).text(0)
    else
      $(A.free_coupons_value).text(amount)

    if show
      $(A.free_coupons_success_container).fadeIn()
    else
      $(A.free_coupons_success_container).hide()

  failed_free_coupons_info = (show) ->
    if show
      $(A.free_coupons_failed_container).fadeIn()
    else
      $(A.free_coupons_failed_container).hide()

  toggle_coupons_info = (data) ->
    if data.result && data.free
      success_free_coupons_info(true, data.free)
      failed_free_coupons_info(false)
    else if data.result
      success_free_coupons_info(false)
      failed_free_coupons_info(false)
    else
      success_free_coupons_info(false)
      failed_free_coupons_info(true)

  # если находимся на первом шаге оформления
  if $(A.container).length

    $(A.districts_input).select2(
      width: 'element'
      minimumResultsForSearch: -1 # скрываем поле поиска
      query: (query) ->
        cachedData = @.cacheDataSource
        if cachedData
          query.callback results: cachedData.districts
          return
        else
          $.ajax
            url: "/api/districts/"
            dataType: "json"
            type: "GET"
            success: (data) =>
              @.cacheDataSource = data
              query.callback results: data.districts
              return

      formatResult: formatResult
      formatSelection: districtSelect
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) ->
        m
    )




    $(A.polyclinics_input).select2(
      width: 'element'
      minimumResultsForSearch: -1 # скрываем поле поиска
      query: (query) ->
        key = "district_#{Appointment.params.district_id}"
        cachedData = @.cacheDataSource
        if cachedData && cachedData.hasOwnProperty(key)
          query.callback results: cachedData[key].polyclinics
          return
        else
          $.ajax
            url: "/api/polyclinics/"
            data:
              district_id: Appointment.params.district_id
            dataType: "json"
            type: "GET"
            success: (data) =>
              @.cacheDataSource ||= {}
              @.cacheDataSource[key] = data
              query.callback results: data.polyclinics
              return


      initSelection: (element, callback) ->
        data =
          id: element.val()
          text: element.val()

        callback data
        return

      formatResult: formatResult
      formatSelection: polyclinicSelection
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) ->
        m
    ).select2('enable', false)

