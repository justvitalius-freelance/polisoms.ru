
$ ->

  A = window.Appointment.step3
  checkable_css_name = '.js-checkable-doctor'

  fetchDoctorsList = () ->
    $.ajax(
      url : '/api/doctors'
      dataType : 'script'
      type : 'GET'
      data :
        speciality_id : Appointment.params.speciality_id
        patient_id : Appointment.patient_id()
        lpu_id : Appointment.lpu_id()
      success : ->
        $('.js-checkable-doctors-loader').removeClass('m-enabled')
    )

  specialitySelect = (data) ->
    # записываем выбранный id в объект
    Appointment.params.speciality_id = data.id

    # активируем второе поле для выбора и очищаем ранее выбранное значение
    # т.к. убрали селект, то больше не используем
    # $(A.doctors_input).select2('enable', true).select2('data', null)

    # записываем имя специализации, чтобы не делать лишний запросы в будущем
    $('input[name="speciality_name"]').val(data.title)

    # выключаем кнопку следующего шага в случае, если было что-то выбрано
    Appointment.disable_next_step() if data.id

    # запрашиваем отрендеринный список врачей в html
    $('.js-checkable-doctors-loader').addClass('m-enabled')
    $('#js-checkable-doctors-list').html('')
    fetchDoctorsList()

    return Appointment.select2_format_selection(data)


  doctorSelection = (data) ->
    Appointment.params.doctor_id = data.id

    # записываем имя доктора, чтобы не делать лишний запросы в будущем
    $('input[name="doctor_name"]').val(data.title)
    $('input[name="doctor_id"]').val(data.id)

    # включаем кнопку следующего шага
    Appointment.enable_next_step() if data.id


  # проверка,что находимся на правильной странице
  if $(A.container).length

    $(A.specialities_input).select2(
      width: 'element'
      minimumResultsForSearch: -1 # скрываем поле поиска

      query: (query) ->
        cachedData = @.cacheDataSource
        if cachedData
          query.callback results: cachedData.specialities
          return
        else
          $.ajax
            url: "/api/specialities/"
            dataType: "json"
            data:
              patient_id: Appointment.patient_id()
              lpu_id: Appointment.lpu_id()
            type: "GET"
            success: (data) =>
              @.cacheDataSource = data
              query.callback results: data.specialities
              return

      formatResult: Appointment.select2_format_result
      formatSelection: specialitySelect
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) ->
        m
    )

    # навешиваем клики на выбор врача в селекте
    $(A.checkable_doctors_list).on('change', '[name="doctors"]', (e) ->
      $(A.checkable_doctors_list).find(checkable_css_name).removeClass('m-selected')
      $el = $(this)
      $el.parents(checkable_css_name).addClass('m-selected')
      data = {
        id: $el.val(),
        title: $el.next().text()
      }
      console.log(data);
      doctorSelection(data)
    )


#  Выбор доктора в селекте
#  Выключен и заменен на простой список докторов
#
#    $(A.doctors_input).select2(
#      width: 'element'
#      minimumResultsForSearch: -1 # скрываем поле поиска
#      query: (query) ->
#        key = "doctors_#{Appointment.params.speciality_id}"
#        cachedData = @.cacheDataSource
#        if cachedData && cachedData.hasOwnProperty(key)
#          query.callback results: cachedData[key].doctors
#          return
#        else
#          $.ajax
#            url: "/api/doctors/"
#            data:
#              speciality_id: Appointment.params.speciality_id
#              patient_id: Appointment.patient_id()
#              lpu_id: Appointment.lpu_id()
#            dataType: "json"
#            type: "GET"
#            success: (data) =>
#              @.cacheDataSource ||= {}
#              @.cacheDataSource[key] = data
#              query.callback results: data.doctors
#              return
#
#      initSelection: (element, callback) ->
#        data =
#          id: element.val()
#          text: element.val()
#
#        callback data
#        return
#
#      formatResult: Appointment.select2_format_result
#      formatSelection: doctorSelection
#      dropdownCssClass: "bigdrop"
#      escapeMarkup: (m) ->
#        m
#    ).select2('enable', false)

