$ ->
  $('.js-select').select2(
    width: 'element'
    minimumResultsForSearch: -1 # скрываем поле поиска
  )

  # выставляем валидацию на числа
  $('.js-numeric').numeric()

  # апдейтим кнопку «назад» выбранными атрибутами
  #  Appointment.copy_hidden_params_to_links('body', '.js-back-btn')