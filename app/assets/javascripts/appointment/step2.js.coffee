$ ->
  A = window.Appointment.step2

  if $(A.container).length
    input_names = "#{A.container} input, #{A.container} select"

    if window.Appointment.validate_required(A.container)
      window.Appointment.enable_next_step()
    else
      window.Appointment.disable_next_step()

    $(document).on('change keyup', input_names, (e)->
      if window.Appointment.validate_required(A.container)
        window.Appointment.enable_next_step()
      else
        window.Appointment.disable_next_step()
    )