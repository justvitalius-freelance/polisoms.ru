$ ->
  A = window.Appointment.doctor_requests_form

  if $(A.container).length
    console.log 'into step'
    input_names = "#{A.container} input, #{A.container} select, #{A.container} textarea"

    if window.Appointment.validate_required(A.container)
      window.Appointment.enable_next_step()
    else
      window.Appointment.disable_next_step()

    $(document).on('change keyup', input_names, (e)->
      if window.Appointment.validate_required(A.container)
        window.Appointment.enable_next_step()
      else
        window.Appointment.disable_next_step()
    )