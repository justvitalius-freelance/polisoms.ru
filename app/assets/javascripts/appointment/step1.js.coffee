
$ ->

  A = window.Appointment.step1

  districtSelect = (data) ->
    # записываем выбранный id в объект
    Appointment.params.district_id = data.id

    # активируем второе поле для выбора и очищаем ранее выбранное значение
    $(A.polyclinics_input).select2('enable', true).select2('data', '')


    # выключаем кнопку следующего шага в случае, если было что-то выбрано
    Appointment.disable_next_step() if data.id

    return formatSelection(data)


  polyclinicSelection = (data) ->
    Appointment.params.polyclinic_id = data.id

    # записываем название поликлиники в переменную
    $('input[name="lpu_title"]').val(data.title)

    # включаем кнопку следующего шага
    Appointment.enable_next_step() if data.id

    return formatSelection(data)


  formatResult = (data) ->
    data.title

  formatSelection = (data) ->
    data.title


  # если находимся на первом шаге оформления
  if $(A.container).length

    $(A.districts_input).select2(
      width: 'element'
      minimumResultsForSearch: -1 # скрываем поле поиска
      query: (query) ->
        cachedData = @.cacheDataSource
        if cachedData
          query.callback results: cachedData.districts
          return
        else
          $.ajax
            url: "/api/districts/"
            dataType: "json"
            type: "GET"
            success: (data) =>
              @.cacheDataSource = data
              query.callback results: data.districts
              return

      formatResult: formatResult
      formatSelection: districtSelect
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) ->
        m
    )




    $(A.polyclinics_input).select2(
      width: 'element'
      minimumResultsForSearch: -1 # скрываем поле поиска
      query: (query) ->
        key = "district_#{Appointment.params.district_id}"
        cachedData = @.cacheDataSource
        if cachedData && cachedData.hasOwnProperty(key)
          query.callback results: cachedData[key].polyclinics
          return
        else
          $.ajax
            url: "/api/polyclinics/"
            data:
              district_id: Appointment.params.district_id
            dataType: "json"
            type: "GET"
            success: (data) =>
              @.cacheDataSource ||= {}
              @.cacheDataSource[key] = data
              query.callback results: data.polyclinics
              return


      initSelection: (element, callback) ->
        data =
          id: element.val()
          text: element.val()

        callback data
        return

      formatResult: formatResult
      formatSelection: polyclinicSelection
      dropdownCssClass: "bigdrop"
      escapeMarkup: (m) ->
        m
    ).select2('enable', false)

