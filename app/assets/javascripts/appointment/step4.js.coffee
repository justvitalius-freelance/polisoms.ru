
$ ->

  A = window.Appointment.step4

  if $(A.container).length
#    Appointment.copy_hidden_params_to_links(A.container, A.calendar)

    $(document).on('change', A.form, (e) ->
      $target = $(e.target)

      # сохраняем человеческое время приема из label в скрытый инпут
      $('input[name="visit_time"]').val($target.closest('label').data('visit-time'))

      $target.closest('form').submit()
    )

