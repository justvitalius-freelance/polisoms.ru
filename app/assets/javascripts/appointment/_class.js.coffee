window.Appointment ||= {}
window.Appointment =
  next_btn: '.js-next-step'
  step1:
    container: '.js-appointment-step-1'
    districts_input: '#js-districts'
    polyclinics_input: '#js-polyclinics'

  step2:
    container: '.js-appointment-step-2'

  step3:
    container: '.js-appointment-step-3'
    specialities_input: '#js-specialities'
    doctors_input: '#js-doctors'
    checkable_doctors_list: '#js-checkable-doctors-list'

  step4:
    container: '.js-appointment-step-4'
    calendar: '.js-calendar'
    form: '.js-form'

  doctor_requests_form:
    container : '.js-doctor-requests-form'
    form : '.js-form'

  login:
    container: '.js-appointment-login'

  call_doctor_home_step1:
    container : '.js-call-doctor-home-step-1'
    districts_input : '#js-districts'
    polyclinics_input : '#js-polyclinics'
    free_coupons_success_container: '#js-call-doctor-home-coupons-success-container'
    free_coupons_failed_container: '#js-call-doctor-home-coupons-failed-container'

    # Выклчен модуль показа количества свободных талонов
    #free_coupons_value: '#js-call-doctor-home-free-coupons'




  params:
    district_id: null # кэш для выбранного района
    polyclinic_id: null # кэш для выбранной больницы
    speciality_id: null
    doctor_id: null


  enable_next_step: ->
    $(@.next_btn).removeClass('m-inactive').prop('disabled', '')

  disable_next_step: ->
    $(@.next_btn).addClass('m-inactive').prop('disabled', 'disabled')


  # возвращает true, если все «required» инпуты внутри контейнера имеют значения
  validate_required: (container) ->
    all_filled = true
    $target = $(container).find('input[required="required"], select[required="required"]')
    $.each($target, (i) ->
      val = $(@).val()
      if all_filled
        all_filled = false if val.length==0
      val
    )
    all_filled


  select2_format_result: (data) ->
    data.title

  select2_format_selection: (data) ->
    data.title

  # сериализуем все параметры из скрытых полей в одном контейнере
  # и апдейтим ссылки внутри другого контейнера
  copy_hidden_params_to_links: (from, to) ->
    $container = $(from)
    $inputs = $container.find('.js-hidden-fields').find('input')
    search = window.location.search.replace('?', '')
    if search.length
      exists_params = JSON.parse "{\"" + decodeURI(search).replace(/"/g, "\\\"").replace(/&/g, "\",\"").replace(RegExp("=", "g"), "\":\"") + "\"}"
    new_params = $.param($inputs, exists_params)
    $.each($(to).find('a'), ->
      old = $(@).attr('href').replace('?','')
      path = if old.indexOf('?') >= 0 then "#{old}&#{new_params}" else "#{old}?#{new_params}"
      $(@).attr('href', path)
    )

  patient_id: ->
    $('.js-hidden-fields').find('input[name="patient_id"]').val()

  lpu_id: ->
    $('.js-hidden-fields').find('input[name="lpu_id"]').val()
