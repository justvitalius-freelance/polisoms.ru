$(document).ready(function() {
  // скрываем, если нет пагинации на странице
  if (!$('.pagination .next_page').attr('href')) {
    $('.js-load-more').remove();
  }

  $('.js-load-more').on('click', function() {
    var moreItemsLink = $('.pagination .next_page').attr('href');
    if (moreItemsLink) {
      $.getScript(moreItemsLink);
    }
  });
});
