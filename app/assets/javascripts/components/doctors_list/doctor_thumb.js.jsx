var DoctorThumb = function (props) {
  let doctor = props.doctor;
  return (
    <div className="doctor m-big">
      <div className="doctor__image-container">
        <img src={doctor.avatarPath} className="doctor__image"/>

        <div className="doctor__votes-container animated bounceIn" key={doctor.votes}>
          <span className="doctor__votes-count">
            {doctor.votes}
          </span>
          <span className="doctor__votes-icon fa fa-heart"/>
        </div>

        <button className="doctor__vote-btn" onClick={_.partial(props.handleVoteClick, doctor)}>
          Мне нравится
          <span className="fa fa-heart doctor__vote-btn-icon"/>
        </button>
      </div>

      <a href={doctor.profilePath} className="doctor__link">
        <div className="doctor__name">
          <div>{doctor.lastName}</div>
          <div>{doctor.firstName} {doctor.secondName}</div>
        </div>
        <div className="doctor__job">{doctor.jobTitle}</div>
      </a>
    </div>
  );
};