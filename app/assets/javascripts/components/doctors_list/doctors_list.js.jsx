
axios.interceptors.request.use(function (config) {
  var token = (document.getElementsByName('csrf-token')[0] || {}).content;
  config.headers['Content-Type'] = 'application/json;charset=utf-8';
  config.headers['X-CSRF-Token'] = token;
  config.headers['X-Requested-With'] = 'XMLHttpRequest';
  return config;
});

var DoctorsList = React.createClass({
  propTypes: {
    doctors: React.PropTypes.array.isRequired,
    districts: React.PropTypes.array
  },

  getDefaultProps: function() {
    return {
      doctors: [],
      districts: []
    }
  },

  getInitialState: function () {
    return {
      searchStr: '',
      selectedDistrict: 'all',
      doctors: this.props.doctors
    }
  },

  componentWillReceiveProps: function(newProps){
    this.setState({
      doctors: newProps.doctors
    })
  },

  render: function () {
    var filter = {
      officeId: this.state.selectedDistrict,
      searchStr: this.state.searchStr
    };

    var districts = this.props.districts.map( (d) => ({value: d.id, label: d.title}) );
    districts = [{value: 'all', label: 'Все отделения'}].concat(districts);

    var loader = <div className="doctors-loader"><img src="/assets/ajax-loader.gif"/></div>;
    var doctorsList = this.filteredDoctors(filter).map(function(doctor){
      if (this.state.selectedDistrict === 'all'){
        return <DoctorMiniThumb doctor={doctor} key={doctor.id} handleVoteClick={this.handleVoteClick}/>
      } else {
        return <DoctorThumb doctor={doctor} key={doctor.id} handleVoteClick={this.handleVoteClick}/>
      }
    }.bind(this));

    return (
      <div className="doctors-filter">
        <div className="doctors-filter-left">
          <SearchInput handleChange={this.handleSearchStrChange} />
        </div>
        <div className="doctors-filter-right">
          <Select
            className="doctors-filter-select"
            options={districts}
            clearable={false}
            value={this.state.selectedDistrict}
            onChange={this.handleDistrictSelect} />
        </div>
        <div className="doctors-list">
          {!this.props.doctors.length && loader}
          {this.props.doctors.length && doctorsList}
        </div>
      </div>

    );
  },

  handleSearchStrChange: function(e){
    let str = e.target.value;

    this.setState({
      searchStr: str
    });
  },

  handleDistrictSelect: function(id, option){
    this.setState({
      selectedDistrict: id
    });
  },

  filteredDoctors: function(filter){
    let filtered = _.chain(this.state.doctors);
    if (filter.hasOwnProperty('officeId') && filter.officeId && filter.officeId !== 'all'){
      filtered = filtered.where({officeId: filter.officeId})
    }
    if (filter.hasOwnProperty('searchStr') && filter.searchStr){
      filtered = filtered.filter( (doc) => ~doc.searchStr.toLowerCase().indexOf(filter.searchStr.toLowerCase()));
    }

    return filtered.value();
  },

  handleVoteClick: function(doctor, e){
    e.preventDefault();
    if (doctor){
      axios
        .post(doctor.likePath)
        .then((res) => this.setState({doctors: res.data}));
    }
  }

});

