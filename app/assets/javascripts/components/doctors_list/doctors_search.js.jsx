var DoctorsSearch = React.createClass({
  propTypes: {
    doctors: React.PropTypes.array.isRequired
  },

  getDefaultProps: function () {
    return {
      doctors: []
    }
  },

  getInitialState: function () {
    return {
      searchStr: '',
      doctors: this.props.doctors
    }
  },

  render: function () {
    var filter = {
      searchStr: this.state.searchStr
    };

    var doctorsList = 'Ничего не найдено...';

    var filtered = this.filteredDoctors(filter);
    if (filtered.length) {
      doctorsList = filtered.map(function (doctor) {
        return <DoctorMiniThumb doctor={doctor} key={doctor.id}/>
      }.bind(this));
    }

    var doctorsContainer = (
      <div className="doctors-list m-popup animated fadeIn">
        {doctorsList}
      </div>
    );

    return (
      <div>
        <SearchInput handleChange={this.handleSearchStrChange}/>
        { this.props.doctors.length && this.state.searchStr.length > 0 && doctorsContainer }
      </div>

    );
  },

  handleSearchStrChange: function (e) {
    let str = e.target.value;

    this.setState({
      searchStr: str
    });
  },

  handleDistrictSelect: function (id, option) {
    this.setState({
      selectedDistrict: id
    });
  },

  filteredDoctors: function (filter) {
    let filtered = _.chain(this.state.doctors);
    if (filter.hasOwnProperty('officeId') && filter.officeId && filter.officeId !== 'all') {
      filtered = filtered.where({officeId: filter.officeId})
    }
    if (filter.hasOwnProperty('searchStr') && filter.searchStr) {
      filtered = filtered.filter((doc) => ~doc.searchStr.toLowerCase().indexOf(filter.searchStr.toLowerCase()));
    }

    return filtered.value();
  },


});

