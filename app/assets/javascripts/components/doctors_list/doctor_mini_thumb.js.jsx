var DoctorMiniThumb = function (props) {
  let doctor = props.doctor;

  return (
    <div className="doctor m-small">
      <div className="doctor__image-container">

        <img src={doctor.avatarPath} className="doctor__image"/>

        <div className="doctor__votes-container animated bounceIn">
          <div className="doctor__votes-icon fa fa-heart"/>
          <div className="doctor__votes-count">{doctor.votes}</div>
        </div>

      </div>

      <a href={doctor.profilePath} className="doctor__scalable">
        <img src={doctor.avatarPath} className="doctor__image"/>

        <div className="doctor__overlay"/>
        <div className="doctor__name">{doctor.fullName}</div>
        <div className="doctor__job">{doctor.jobTitle}</div>
        {
          props.handleVoteClick &&
          <button
            className="doctor__vote-btn animated bounceIn"
            onClick={_.partial(props.handleVoteClick, doctor)}
            key={`${doctor.votes}-${doctor.lastUpdatedAt}`}>
            Мне нравится
            <span className="fa fa-heart doctor__vote-btn-icon"/>
          </button>
        }
      </a>

    </div>
  );
};