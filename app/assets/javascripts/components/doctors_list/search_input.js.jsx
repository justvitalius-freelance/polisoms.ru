var SearchInput = function (props) {
  return (
    <input
      className="doctors__search"
      size="40"
      placeholder="Поиск по врачу"
      autoFocus={true}
      onChange={props.handleChange}/>
  );
};