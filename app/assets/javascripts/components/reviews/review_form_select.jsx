var ReviewFormSelect = function (props) {
  var data = props.data || [];
  var value = props.value || '0';
  var label = props.label;
  var handleChange = props.onChange;
  return (
    <label className="review-form__select">
      { label &&
        <div className="review-form__inline">
          { label }
        </div>
      }
      <div className="review-form__inline">
        { !!data.length &&
          <select onChange={ (e) => handleChange(e.target.value) } value={ value }>
            { <option value="0" disabled>выберите</option>}
            { data && data.map(function (item) {
              return <option value={ item.id }>{ item.name }</option>
            })}
          </select>
        }
      </div>
    </label>
  );
};