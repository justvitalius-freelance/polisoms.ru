function validateReviewFormText(state) {
  return !!getStringFieldFromState(state, 'text').length;
}

function validateReviewFormName(state) {
  return !!getStringFieldFromState(state, 'name').length;
}

function validateReviewFormOffice(state) {
  return !!getStringFieldFromState(state, 'officeId').length;
}

function validateReviewFormCategory(state) {
  return !!getStringFieldFromState(state, 'categoryId').length;
}

function validateReviewFormContacts(state) {
  var emailRegex = /^\w+(\w+\-?\.?\+?)+@\w+\.\w{2,5}$/g;
  var phoneRegex = /^\+?([\d*\s?\-])+\d$/g;

  return !!getStringFieldFromState(state, 'contacts').match(phoneRegex)
          || !!getStringFieldFromState(state, 'contacts').match(emailRegex)
}

function validateReviewFormDoctors(state) {
  var categoryId = getStringFieldFromState(state, 'categoryId');
  var doctorSequence = getStringFieldFromState(state, 'doctorSequence');
  var doctors = doctorSequence.map(function(item){
    if (item.id === CUSTOM_DOCTOR_OPTION_ID) {
      return item.value;
    }
    return item.id;
  }).join();
  return !canChooseDoctors(categoryId)
    || (canChooseDoctors(categoryId) && !!doctors.length);
}

function validateReviewForm(state) {
  var validations = {
    text: validateReviewFormText(state),
    contacts: validateReviewFormContacts(state),
    name: validateReviewFormName(state),
    doctors: validateReviewFormDoctors(state),
    office: validateReviewFormOffice(state),
    category: validateReviewFormCategory(state),
  };

  var keys = [];
  for(var key in validations) {
    if (validations.hasOwnProperty(key)) keys.push(key);
  }

  return keys.reduce(function(sum, key) {
    return sum && validations[key];
  }, true);
}


////////////////////////
function getStringFieldFromState(state, fieldName) {
  return ((state || {})[fieldName] || '');
}


