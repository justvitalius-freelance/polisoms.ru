var REVIEW_FORM_TIMEOUTS = 5000;
var CUSTOM_DOCTOR_OPTION_ID = "Xnull";
var EMPTY_DOCTOR_ID = null;
var EMPTY_DOCTORS_SEQUENCE = [{
  id: EMPTY_DOCTOR_ID,
  value: "",
}];

function filterDoctorsByOffice(collection, officeId) {
  return filterById(collection, officeId, 'officeId');
}

function addCustomDoctorOption(collection) {
  return (collection || []).concat({
    id: CUSTOM_DOCTOR_OPTION_ID,
    name: 'Другой доктор'
  })
}

function wrapDoctorsForReviewSelect(collection) {
  return collection.map(function(item){
    return {
      id: item.id,
      name: item.fullName,
      officeId: item.officeId,
    }
  })
}

function canChooseDoctors(categoryId) {
  var category = getById(reviewCategories, categoryId) || {};
  return (category.actions || []).indexOf('showDoctors') > -1;
}

function sendReviewToServer(data) {
  var payload = Object.assign(
    {},
    data,
    {
      category: getById(reviewCategories, data.categoryId).name,
      subcategory: getById(reviewSubcategories, data.subcategoryId).name,
      officeId: parseInt(data.officeId),
      categoryId: parseInt(data.categoryId),
      subcategoryId: parseInt(data.subcategoryId),
      // doctorIds: data.doctorIds.filter(function(id){ return !!id; }).map(function(id){ return parseInt(id) }),
      doctorIds: doctorIdsToIntegers(sequenceToDoctorIds(data.doctorSequence)),
      customDoctors: sequenceToCustomDoctors(data.doctorSequence),
      type: data.type,
    }
  );
  return axios
    .post('/api/reviews', {
      review: payload
    })
    .then(function(res){
      return res.data;
    })
}

function getById(collection, id, fieldName) {
  return ((filterById(collection, id, fieldName) || [])[0] || {});
}

function filterById(collection, id, fieldName) {
  var _fieldName = fieldName || 'id';
  var _id = (id || '').toString();
  return collection.filter(function (item) {
    var itemId = (item[_fieldName] || '').toString();
    return itemId === _id;
  })
}

function humanizedSavedStatus(status) {
  if (status === null) {
    return '';
  }
  return status ? 'Отзыв успешно отправлен' : 'При отправке сообщения произошла ошибка. Повторите чуть позже.'
}

function sequenceToDoctorIds(sequence) {
  return sequence.reduce( function(ids, item) {
    if (item.id !== CUSTOM_DOCTOR_OPTION_ID) {
      return ids.concat(item.id)
    }
    return ids;
  }, [])
}

function sequenceToCustomDoctors(sequence) {
  return sequence.reduce(function (ids, item) {
    if (item.id === CUSTOM_DOCTOR_OPTION_ID) {
      return ids.concat(item.value)
    }
    return ids;
  }, [])
}

function doctorIdsToIntegers(collection) {
  var _collection = collection || [];
  return _collection.filter(function (id) {
    return !!id;
  }).map(function (id) {
    return parseInt(id)
  });
}