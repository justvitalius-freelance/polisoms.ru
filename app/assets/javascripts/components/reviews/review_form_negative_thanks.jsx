var ReviewFormNegativeThanks = function () {
  return (
    <div className="review-thanks m-negative">
      <p> Спасибо за Ваш отзыв!</p>
      <p>
        Мы свяжемся с Вами по телефону или электронной почте в течение трех рабочих дней, чтобы
        сообщить о результатах нашего внутреннего расследования!
      </p>
      <p>
        Если Вы не получили ответ в течение указанного времени,
        пожалуйста, продублируйте свое обращение на адрес
        <a href="mailto:info@polisoms.ru">info@polisoms.ru</a>
      </p>
    </div>
  );
};