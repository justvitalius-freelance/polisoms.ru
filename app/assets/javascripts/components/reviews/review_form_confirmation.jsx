var ReviewFormConfirmation = function (props) {
  var onSubmit = props.onSubmit;
  var onCancel = props.onCancel;
  return (
    <div className="review-form review-thanks">
      <div className="review-form__inline">
        <p>
          Спасибо за Ваше обращение!
        </p>
        <p>
          Пожалуйста, убедитесь, что Вы указали фамилию, имя, отчество пациента, который обращался
          в наши центры. <br/>
          Без этой информации мы не сможем разобраться в ситуации и сообщить Вам о результатах!
        </p>
      </div>
      <div className="review-form__block">
        <div className="review-form__inline">
          <button onClick={ onCancel } className="review-form__cancel-btn">Посмотреть обращение еще раз</button>
        </div>
        <div className="review-form__inline">
          <button onClick={ onSubmit } className="review-form__submit-btn">Отправить</button>
        </div>
      </div>
    </div>
  );
};