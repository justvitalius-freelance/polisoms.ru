var ReviewDoctorsSelect = React.createClass({
  propTypes: {
    doctors: React.PropTypes.array,
    officeId: React.PropTypes.string,
    selectedIds: React.PropTypes.array,
    doctorSequence: React.PropTypes.array,
    canChooseDoctors: React.PropTypes.bool,
    onChange: React.PropTypes.func,
  },

  getDefaultProps: function () {
    return {
      doctors: [],
      selectedIds: [],
      doctorSequence: [],
      canChooseDoctors: false,
    }
  },

  render: function () {
    var visibleSequence = this.getVisibleSequence();
    var filteredDoctors = addCustomDoctorOption(
      filterDoctorsByOffice(this.props.doctors, this.props.officeId)
    );
    var canChooseDoctors = this.props.canChooseDoctors;
    return (
      <div className="review-doctors">
        { canChooseDoctors &&
          <div className="review-doctors__label">Доктора</div>
        }
        { canChooseDoctors &&
          <div className="review-doctors__list">
            {
              visibleSequence.map(function (selectedDoctor, i) {
                var selectedDoctorId = selectedDoctor.id;
                var customDoctorValue = selectedDoctor.value;
                var isCustomDoctor = selectedDoctorId === CUSTOM_DOCTOR_OPTION_ID;
                return (
                  <div className="review-doctors__item">
                    { !isCustomDoctor &&
                      <ReviewFormSelect
                        onChange={ this.handleDoctorSelect.bind(this, i) }
                        data={ filteredDoctors }
                        value={ selectedDoctorId }
                      />
                    }
                    { isCustomDoctor &&
                      <ReviewFormInput
                        onChange={ this.handleCustomDoctorChange.bind(this, i) }
                        value={ customDoctorValue }
                        placeholder="ФИО"
                        size="29"
                        />
                    }
                    <button
                      onClick={ this.handleDoctorRemove.bind(this, i)}
                      className="review-doctors__remove-btn"
                      >×</button>
                  </div>
                );
              }.bind(this))
            }
            <button
              onClick={ this.handleAddDoctor }
              className="review-doctors__add-btn"
              >Еще один доктор</button>
          </div>
        }
      </div>
    );
  },

  handleDoctorSelect: function (numberOfInputInList, selectedId) {
    var sequence = this.getVisibleSequence();
    sequence[numberOfInputInList] = {
      id: selectedId,
      value: null,
    };
    return this.props.onChange(sequence);
  },
  
  handleCustomDoctorChange: function (numberOfInputInList, value) {
    var sequence = this.getVisibleSequence();
    sequence[numberOfInputInList] = {
      id: CUSTOM_DOCTOR_OPTION_ID,
      value: value,
    };
    return this.props.onChange(sequence);
  },

  handleAddDoctor: function(){
    this.props.onChange(
      this.getVisibleSequence().concat({
        id: EMPTY_DOCTOR_ID,
        value: null,
      })
    );
  },

  handleDoctorRemove: function(numberOfSelect) {
    this.props.onChange(
      this.getVisibleSequence()
        .slice(0, numberOfSelect)
        .concat(
          this.getVisibleSequence().slice(numberOfSelect + 1)
        )
    );
  },

  getVisibleDoctorIds: function() {
    var selectedDoctorsIds = this.props.selectedIds;
    return selectedDoctorsIds.length ? selectedDoctorsIds : [EMPTY_DOCTOR_ID];
  },

  getVisibleSequence: function () {
    var sequence = this.props.doctorSequence;
    return sequence.length ? sequence : EMPTY_DOCTORS_SEQUENCE;
  }

});


