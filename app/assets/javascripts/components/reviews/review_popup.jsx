var ReviewPopup = React.createClass({
  propTypes: {
    open: React.PropTypes.bool,
    onToggle: React.PropTypes.func,
  },

  getInitialState: function () {
    return {
      open: this.props.open,
    }
  },

  componentWillReceiveProps: function(nextProps) {
    var _nextProps = nextProps || {};
    if (_nextProps.open !== this.state.open) {
      this.setState({ open: _nextProps.open })
    }
  },

  render: function () {
    var isOpen = this.state.open;
    var classname = isOpen ? 'm-opened' : 'm-hidden';
    return (
      <div>
        { isOpen &&
          <ReviewPopupShadow onClick={ this.handleClosePopup }/>
        }
        <div className={ classname.concat(" review-popup") }>
          <ReviewForm onSubmit={ this.handleSubmit.bind(this) } />
        </div>
      </div>
    );
  },

  handleSubmit: function(savedStatus) {
    if (savedStatus) {
      return this.handleClosePopup();
    }
  },

  handleClosePopup: function () {
    return this.setState({ open: false }, () => this.props.onToggle());
  },

});