var ReviewFormInput = function (props) {
  var value = props.value;
  var label = props.label;
  var placeholder = props.placeholder;
  var size = props.size;
  var handleChange = props.onChange;
  return (
    <label className="review-form__input">
      { label &&
        <div className="review-form__inline">
          { label }
        </div>
      }
      <div className="review-form__inline">
        <input
          onChange={ (e) => handleChange(e.target.value) }
          value={ value }
          placeholder={ placeholder }
          size={ size }
          />
      </div>
    </label>
  );
};