var ReviewButton = React.createClass({
  propTypes: {
    type: React.PropTypes.string,
    filled: React.PropTypes.bool,
  },

  getDefaultProps: function () {
    return {
      type: 'positive',
    }
  },

  getInitialState: function () {
    return {
      open: false,
    }
  },

  getChildContext() {
    return {
      doctors: wrapDoctorsForReviewSelect(this.props.doctors),
      offices: this.props.offices,
      type: this.props.type,
    }
  },

  render: function () {
    var type = this.props.type;
    var open = this.state.open;
    var filled = this.props.filled;
    var title = type === 'positive' ? 'Поблагодарить' : 'Пожаловаться';
    title = filled ? '' : title;
    var classname = type === 'positive' ? 'm-positive' : 'm-negative';
    classname = classname.concat(' review-button');
    classname = filled ? classname.concat(' m-filled') : classname;
    return (
      <div>
        <button className={ classname } onClick={ this.handleClick } type="button">{ title }</button>
        <ReviewPopup open={ open } onToggle={ this.handleClick } />
      </div>
    );
  },

  handleClick: function () {
    this.setState({
      open: !this.state.open,
    })
  }

});

ReviewButton.childContextTypes = {
  doctors: React.PropTypes.array.isRequired,
  offices: React.PropTypes.array.isRequired,
  type: React.PropTypes.string.isRequired,
};