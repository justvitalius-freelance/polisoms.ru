var reviewCategories = [
  {
    id: 1,
    name: 'На регистратуру',
    actions: [],
    positive: true,
    negative: true,
  },
  {
    id: 2,
    name: 'На врача',
    actions: ['showDoctors'],
    positive: true,
    negative: true,
  },
  {
    id: 3,
    name: 'Процедурный кабинет',
    actions: [],
    positive: true,
    negative: true,
  },
  {
    id: 9,
    name: 'Call-центр',
    actions: [],
    positive: true,
    negative: true,
  },
  {
    id: 4,
    name: 'Невозможно записаться на прием',
    actions: [],
    positive: false,
    negative: true,
  },
  {
    id: 5,
    name: 'Лаборатория',
    actions: [],
    positive: true,
    negative: true,
  },
  {
    id: 6,
    name: 'Сервис',
    actions: [],
    positive: true,
    negative: true,
  },
  {
    id: 7,
    name: 'Врач не пришел на вызов',
    actions: ['showDoctors'],
    positive: false,
    negative: true,
  },
  {
    id: 8,
    name: 'Другое',
    actions: [],
    positive: true,
    negative: true,
  },
];

var reviewSubcategories = [
  {
    id: 1,
    categoryId: 1,
    name: 'хамское отношение',
  },
  {
    id: 2,
    categoryId: 1,
    name: 'некомпетентный персонал',
  },
  {
    id: 3,
    categoryId: 2,
    name: 'хамское отношение',
  },
  {
    id: 4,
    categoryId: 2,
    name: 'некомпетентный персонал',
  },
  {
    id: 5,
    categoryId: 3,
    name: 'хамское отношение',
  },
  {
    id: 6,
    categoryId: 3,
    name: 'некомпетентный персонал',
  },

  {
    id: 7,
    categoryId: 4,
    name: 'call-центр',
  },
  {
    id: 8,
    categoryId: 4,
    name: 'сайт',
  },
  {
    id: 9,
    categoryId: 4,
    name: 'нет расписания',
  },

  {
    id: 10,
    categoryId: 5,
    name: 'потеряли результаты анализов',
  },
  {
    id: 11,
    categoryId: 5,
    name: 'некорректные результаты',
  },
  {
    id: 12,
    categoryId: 5,
    name: 'долгое время ожидания результатов',
  },

  {
    id: 13,
    categoryId: 6,
    name: 'очередь, долгое время ожидания приема',
  },
  {
    id: 14,
    categoryId: 6,
    name: 'технические трудности',
  },
  {
    id: 15,
    categoryId: 6,
    name: 'другое',
  },
  {
    id: 16,
    categoryId: 9,
    name: 'хамское отношение',
  },
  {
    id: 17,
    categoryId: 9,
    name: 'некомпетентный персонал',
  },
  {
    id: 18,
    categoryId: 9,
    name: 'долгое время ожидания',
  },
];

function filterReviewCategories(type) {
  var _type = type || 'positive';

  return reviewCategories.filter(function(category){
    if (_type === 'negative') {
      return !!category.negative;
    }
    return !!category.positive;
  })
};

function filterReviewSubategories(categoryId) {
  var _categoryId = (categoryId || '').toString();
  return reviewSubcategories.filter(function (sb) {
    return sb.categoryId.toString() === categoryId;
  })
};