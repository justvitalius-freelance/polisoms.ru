var ReviewFormSubmit = function (props) {
  var disabledClass = props.disabled ? ' m-disabled' : '';
  return (
    <button
      onClick={ props.onClick }
      disabled={ props.disabled }
      className={ "review-form__submit-btn".concat(disabledClass) }
      >
      Отправить
    </button>
  );
};