var ReviewFormTextArea = function (props) {
  var value = props.value;
  var label = props.label;
  var placeholder = props.placeholder;
  var handleChange = props.onChange;
  return (
    <label className="review-form__textarea">
      { label &&
        <div className="review-form__inline">
          { label }
        </div>
      }
      <div className="review-form__inline">
        <textarea
          onChange={ (e) => handleChange(e.target.value) }
          value={ value }
          placeholder={ placeholder }
          rows="2"
          cols="65"
          />
      </div>
    </label>
  );
};