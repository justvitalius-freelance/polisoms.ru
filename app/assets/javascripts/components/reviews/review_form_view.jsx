var ReviewFormView = React.createClass({
  propTypes: {
    type: React.PropTypes.string.isRequired,
    categoryId: React.PropTypes.string,
    subcategoryId: React.PropTypes.string,
    officeId: React.PropTypes.string,
    name: React.PropTypes.string,
    contacts: React.PropTypes.string,
    text: React.PropTypes.string,

    savedStatus: React.PropTypes.bool,
    disabledForSend: React.PropTypes.bool,

    doctors: React.PropTypes.array,
    offices: React.PropTypes.array,
    doctorIds: React.PropTypes.array,
    doctorSequence: React.PropTypes.array,

    onSubmit: React.PropTypes.func,
    onFieldChange: React.PropTypes.func,
  },

  getDefaultProps: function () {
    return {
      type: '',
      doctors: [],
      offices: [],
      categoryId: null,
      subcategoryId: null,
      doctorIds: [],
      doctorSequence: [],
      customDoctors: [],
      officeId: null,
      name: '',
      contacts: '',
      text: '',
      savedStatus: null,
      disabledForSend: true,
      onFieldChange: function () {
      },
      onSubmit: function () {
      },
    }
  },

  render: function () {
    var type = this.props.type;
    var isNegative = type === 'negative';
    var isPositive = type === 'positive';

    var offices = this.props.offices;
    var selectedOfficeId = this.props.officeId;

    var filteredCategories = filterReviewCategories(type);
    var selectedCategoryId = this.props.categoryId;
    var filteredSubcategories = filterReviewSubategories(selectedCategoryId);
    var selectedSubcategoryId = this.props.subcategoryId;

    var doctors = this.props.doctors;
    var selectedDoctorIds = this.props.doctorIds;
    var doctorSequence = this.props.doctorSequence;

    var name = this.props.name;
    var contacts = this.props.contacts;
    var text = this.props.text;

    var disabledForSend = this.props.disabledForSend;
    var savedStatus = humanizedSavedStatus(this.props.savedStatus);

    var onFieldChange = this.props.onFieldChange;
    var onSubmit = this.props.onSubmit;

    return (
      <div className="review-form">
        { isNegative && <h1 className="review-form__title">Оставить жалобу</h1> }
        { isPositive && <h1 className="review-form__title">Благодарность</h1> }
        <div className="review-form__block">
          <ReviewFormSelect
            onChange={ (v) => onFieldChange('officeId', v) }
            data={ offices }
            value={ selectedOfficeId }
            label="Отделение"
            />
        </div>
        <div className="review-form__block">
          <div className="review-form__inline">
            <ReviewFormSelect
              onChange={ (v) => onFieldChange('categoryId', v) }
              data={ filteredCategories }
              value={ selectedCategoryId }
              label="Предмет"
              />
          </div>
          { isNegative &&
            <div className="review-form__inline m-padding">
              <ReviewFormSelect
                onChange={ (v) => onFieldChange('subcategoryId', v) }
                data={ filteredSubcategories }
                value={ selectedSubcategoryId }
                />
            </div>
          }
        </div>
        <div className="review-form__block">
          <ReviewDoctorsSelect
            doctors={ doctors }
            categoryId={ selectedCategoryId }
            officeId={ selectedOfficeId }
            selectedIds={ selectedDoctorIds }
            doctorSequence={ doctorSequence }
            canChooseDoctors={ canChooseDoctors(this.props.categoryId) }
            onChange={ (v) => onFieldChange('doctorSequence', v) }
            />
        </div>
        <div className="review-form__block">
          <div className="review-form__inline">
            Представьтесь
          </div>
          <div className="review-form__inline">
            <ReviewFormInput
              value={ name }
              onChange={ (v) => onFieldChange('name', v) }
              placeholder="Имя Отчество"
              />
          </div>
          <div className="review-form__inline">
            <ReviewFormInput
              value={ contacts }
              onChange={ (v) => onFieldChange('contacts', v) }
              placeholder="Номер телефона или email"
              size="30"
              />
          </div>
        </div>
        <div className="review-form__block">
          <ReviewFormTextArea
            value={ text }
            onChange={ (v) => onFieldChange('text', v) }
            placeholder="Сообщение"
            />
        </div>
        <div className="review-form__block">
          <ReviewFormSubmit onClick={ onSubmit } disabled={ disabledForSend }/>
          { !!savedStatus &&
            <p className="review-form__notice">{ savedStatus }</p>
          }
        </div>
      </div>
    );
  },

});

