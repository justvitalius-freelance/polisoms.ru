var ReviewPopupShadow = function (props) {
  var handleClick = props.onClick;
  return (
    <div
      className="review-popup__shadow"
      onClick={ handleClick }
      ></div>
  );
};