var reviewFormStatusTimerId; // таймер для сброса статуса сообщения об ошибке
var initState = {
  categoryId: null,
  subcategoryId: null,

  doctorSequence: [], // последовательность вводимых докторов

  doctorIds: [],
  customDoctors: [],
  officeId: null,
  name: '',
  contacts: '',
  text: '',

  savedStatus: null, // null | false | true
  lastSavedTime: null,
  needToConfirm: false,
};

var ReviewForm = React.createClass({
  propTypes: {
    type: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func,
  },

  getDefaultProps: function () {
    return {
      doctors: [],
      offices: [],
    }
  },

  getInitialState: function () {
    return initState;
  },

  render: function () {
    var type = this.context.type;
    var isNegative = type === 'negative';
    var isPositive = type === 'positive';
    var savedSuccessfull = !!this.state.savedStatus;
    var notSaved = this.state.savedStatus === null;

    var showConfirmation = notSaved && this.state.needToConfirm && isNegative;
    var showThanks = savedSuccessfull && isPositive;
    var showNegativeThanks = savedSuccessfull && isNegative;
    var showForm = !showConfirmation && !showThanks && !showNegativeThanks;

    var reviewFormSubmit = showForm && isPositive ? this.handleSaveReview : this.handleConfirmationShow;
    return (
      <div>
        { showForm &&
          <ReviewFormView
            type={ this.context.type }
            categoryId={ this.state.categoryId }
            subcategoryId={ this.state.subcategoryId }
            officeId={ this.state.officeId }
            name={ this.state.name }
            contacts={ this.state.contacts }
            text={ this.state.text }
            savedStatus={ this.state.savedStatus }

            disabledForSend={ !validateReviewForm(this.state) }

            doctors={ this.context.doctors }
            offices={ this.context.offices }
            doctorIds={ this.state.doctorIds }
            doctorSequence={ this.state.doctorSequence }

            onSubmit={ reviewFormSubmit }
            onFieldChange={ this.handleFieldChange }
            />
        }
  
        { showConfirmation &&
          <ReviewFormConfirmation
            onSubmit={ this.handleSaveReview }
            onCancel={ this.handleConfirmationCancel }
            />
        }
        { showThanks && <ReviewFormThanks /> }
        { showNegativeThanks && <ReviewFormNegativeThanks /> }
      </div>
    );
  },

  handleFieldChange: function(name, value) {
    console.log('field change', name, value);
    var changed = {};
    var resetable = {};

    changed[name] = value;

    if (name === 'categoryId') resetable.subcategoryId = null;
    if (name === 'officeId') resetable.doctorIds = [];
    if (!canChooseDoctors(this.state.categoryId)) resetable.doctorIds = [];

    return this.setState(
      Object.assign(
        {},
        this.state, // текущие
        resetable, // сбрасываем при перевыборе зависимых
        changed, // изменение из инпута
      )
    );
  },

  handleConfirmationShow: function() {
    this.setState({
      needToConfirm: true,
    })
  },

  handleSaveReview: function() {
    return sendReviewToServer(Object.assign({}, this.state, this.props))
      .then(function(res){
        res.status && this.toggleFormSaveStatus(res.status);
        setTimeout( () => {
          this.props.onSubmit(res.status);
        }, REVIEW_FORM_TIMEOUTS);
      }.bind(this));
  },

  handleConfirmationCancel: function () {
    this.setState({
      needToConfirm: false,
    })
  },

  toggleFormSaveStatus: function (incomingStatus) {
    clearTimeout(reviewFormStatusTimerId);
    this.setState({
      savedStatus: incomingStatus === 'success',
    }, function() {
      reviewFormStatusTimerId = setTimeout(function(){
        this.clearForm()
      }.bind(this), REVIEW_FORM_TIMEOUTS)
    })
  },

  clearForm: function() {
    if (this.state.savedStatus) {
      this.setState(
        Object.assign({}, initState)
      );
    } else {
      this.setState({
        savedStatus: null,
      });
    }
  }

});

ReviewForm.contextTypes = {
  doctors: React.PropTypes.array.isRequired,
  offices: React.PropTypes.array.isRequired,
  type: React.PropTypes.string.isRequired,
};

