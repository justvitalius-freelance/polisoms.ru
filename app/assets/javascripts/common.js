function control_in(control, text) {
  if (text == control.value) control.value = '';
}
function control_out(control, text) {
  if ('' == control.value) control.value = text;
}

function doc_scroll_top() {

  if (window.pageYOffset) {
    return window.pageYOffset;
  }

  if (document.documentElement && document.documentElement.scrollTop) {
    return document.documentElement.scrollTop;
  }
  if (document.body) {
    return document.body.scrollTop;
  }

  return 0;
}




function start_info_next() {
  var elem = $('.start__menu-selected').next();

  if (elem.length == 0)
    start_info_hover($('.start__menu li')[0]);
  else
    start_info_hover(elem);

  setTimeout('start_info_next();', 12000);
}

function start_info_hover(elem) {
  if ($(elem).hasClass('start__menu-selected'))
    return false;

  $('.start__info-data-show').fadeOut(333, 0).removeClass('start__info-data-show');
  $('.start__menu-selected').removeClass('start__menu-selected');

  $(elem).addClass('start__menu-selected');
  $('.start__info-data-' + $(elem).attr('id')).fadeIn(333).addClass('start__info-data-show');

  $('.start__text-position').removeClass('start__text-position-sm-i2').removeClass('start__text-position-sm-i3');
  $('.start__text-position').addClass('start__text-position-' + $(elem).attr('id'));
}

function start_info_init() {
  $('.start__menu li').click(function () {
    start_info_hover($(this));

  });

  setTimeout('start_info_next();', 8000);
}


