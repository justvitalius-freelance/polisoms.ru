(function(){
  'use strict';

  $(document).ready(function(){

    var $select= $('.js-doctors-offices-filter'),
        $overlay = $('.js-doctors-list-loader');

    activate();
    ////////////////////////////////////
    $select.on('change', function(){
      activate();
    })
    ////////////////////////////////////

    function activate(){
      var initOfficeId = $select.val();
      $('.js-doctors-list-loader').removeClass('m-inactive')
      getStaffByOffice(initOfficeId);
    }

    //  запрашиваем врачей с сервера по выбранному офису
    function getStaffByOffice(id){
      var params = {};
      if (id){
        params.office_id = id;
      }
      $.get(doctorsPath(), params)
    }

    function doctorsPath(id){
      return '/doctors.js'
    }

    //////////////////////////////////////
    $select.select2({
      minimumResultsForSearch: Infinity,
      width: "style"
    })

  });
})()