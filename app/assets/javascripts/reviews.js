
$(document).ready(function() {
	$('.doctors-select').on('change', function(event){
		el = event.target;
		$('.doctors-select').each(function(k,v){
			if($(el).parent().attr('data-num') == $(v).parent().attr('data-num')) {
				return false;
			}
			$(v).find('option[value='+ $(el).find('option:selected').val() +']').remove()
		});
	});
	$('.doctors-select:last').attr('name', 'review_doc[0][doctor_id]')
	
	$('#add_doctors').bind('click', function() {
		last_select = $('.doctors-select:last');
		if(last_select.find('option').length < 2) {
			$(this).hide();
			return false;
		}
		num = $('.doctors-select').length
		$('#doc').append('<div class="doc" data-num="'+ num +'">' + $('.doc:last').html() + '</div>');
		$('#doc').css('height', ($('.doctors-select').length - 1) * 30 + 50 + 'px')
		$('.doc').css('margin-top', '10px');
		$('.doctors-select:last').attr('name', 'review_doc['+ num +'][doctor_id]')
		$('.doctors-select:last').change();
		$('.doctors-select:last option[value='+ last_select.val() +']').remove();
		if($('.doctors-select:last option').length == 1) {
			$(this).hide();
		}
		return false;
	});

	$('#new_review, #edit_review').bind('submit', function() {
		docs = '';
		$('.doctors-select option:selected').each(function(k,v){docs += $(v).val() +  ','});
		docs = docs.slice(0, -1);
		$('#review_docs').val(docs)
	});

	$(".response__menu-up, .i2, #blago").click(function(){
		show_popupresponse(1);
	});
	
	$(".response__menu-down, .i1, #yabida").click(function(){
		show_popupresponse(2);
	});
	
	$(".response__menu-write").click(function(){
		show_popupresponse(3);
	});

  function show_popupresponse(popuptype) {
    switch (popuptype) {
      // positive
      case 1:
        $("#review_status option[value='Positive']").attr('selected', 'selected');
        break;
      // negative
      case 2:
        $("#review_status option[value='Negative']").attr('selected', 'selected');
        break;
    }


    $('#shadow').css('height', $('body').height() + 'px');
    $('#shadow').css('width', $('body').width() + 'px');
    $('#shadow').show();

    $(".popupresponse").show();

  };

  $("#shadow, .popupresponse__close").click(function () {
    $('.popupresponse').hide();

    //$(".popupthankyou").css('left', parseInt(($('body').width() - $(".popupthankyou").width()) / 2) + 'px');
    //$(".popupthankyou").css('top', doc_scroll_top() + 100 + 'px');

    $('#shadow').hide();
    $('.popupthankyou').hide();

  });

  function valid_item(item, default_text) {
    item = $('#' + item);
    if (item.val() == default_text || item.val() == '') {
      item.addClass('error-border');
      return false;
    }
    else {
      item.removeClass('error-border');
      return true
    }
  }

  function valid_feedback() {
    var errors = 0
    //var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    var emailRegex = /^\w+(\w+\-?\.?\+?)+@\w+\.\w{2,5}$/g;
    var intRegex = /^[0-9]+$/;
    var phoneRegex = /^\+?([\d*\s?\-])+\d$/g;
    contact_field = $('#review_contact').val();
    if (!valid_item('review_author', 'Введите Ваше имя...')) {
      errors++;
      $('#feedback_error').text('Представьтесь, пожалуйста:');
    }
    if (!valid_item('review_contact', 'e-mail или номер телефона')) {
      errors++;
      $('#feedback_error').text('Заполните, пожалуйста, контактную информацию:');
    }
    else if (!contact_field.match(emailRegex) && !contact_field.match(phoneRegex)) {
      errors++;
      $('#feedback_error').text('Необходимо указать e-mail или номер телефона');
      $('#review_contact').addClass('error-border');
    }
    if (errors == 0) {
      $('#feedback_error').text('');
    }
    if (!valid_item('review_description', 'Ваш отзыв')) {
      errors++;
      $('#feedback_error_description').text('Напишите Ваш отзыв:');
    }
    else {
      $('#feedback_error_description').text('')
    }
    if (errors >= 2) {
      $('#feedback_error').text('Заполните все поля, пожалуйста:');
    }
    return errors == 0;
  }

  // октатываемся на шаг назад
  $('.js-review-result-important .js-review-result-important-back').on('click', function (e) {
    $('.js-review-result-important').hide();
    $('.popupresponse').show();
  });

  // подтверждаем форму
  $('.js-review-result-important .js-review-result-important-submit').on('click', function (e) {
    e.preventDefault();
    $('.js-review-form').submit();
    $('.js-review-result-important').hide();
    $('.js-review-result-warning').fadeIn();
    setTimeout(function () {
      $('.popupthankyou__close').click();
    }, 4000);
  });

  var alignPopup = function () {
    $('.popupresponse').hide();
  };

  var showNegativePopup = function () {
    alignPopup();
    $('.js-review-result-important').show();
  };

  var showPositivePopup = function () {
    alignPopup();
    $('.js-review-result-thanks').fadeIn();
    setTimeout(function () {
      $('.popupthankyou__close').click();
    }, 3000);
  };

  $('.popupthankyou__close').click(function () {
    $('.popupthankyou').fadeOut();
    $('#shadow').hide();
  });

  $(".popupresponse__submit").click(function (e) {
    e.preventDefault();

    if (!valid_feedback()) return false;

    var status = $('#review_status').val();
    $('#check').val('2');
    if (status === 'Negative') {
      showNegativePopup();
    } else if (status === 'Positive') {
      $('.js-review-form').submit();
      showPositivePopup();
    }
  });

  // обработка checked
  function popup_check_doctor() {
    if ($(this).is(':checked'))
      $(this).parent().find('select').prop('disabled', false);
    else
      $(this).parent().find('select').prop('disabled', true);

  }

  $(".popupresponse__line input").click(popup_check_doctor);
});





