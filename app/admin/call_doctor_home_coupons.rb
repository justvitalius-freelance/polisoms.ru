# coding: utf-8

ActiveAdmin.register CallDoctorHomeCoupon do
  menu parent: 'Вызов врача на дом', label: 'Талоны вызова врачей'
  config.batch_actions = false
  config.clear_sidebar_sections!
  config.sort_order = 'created_at_asc'

  index do
    column :lpu_description
    column :amount
    column :actions do |item|
      link_to('Удалить', admin_call_doctor_home_coupon_path(item), class: "member_link", method: :delete)
    end
  end

  form :html => { :enctype => "multipart/form-data", class: 'appointment-form js-call-doctor-home-coupons-admin' } do |f|
    f.inputs do
      f.input :region_id, input_html: { id: 'js-districts', placeholder: 'Выберите из списка' }
      f.input :lpu_id, input_html: { id: 'js-polyclinics', placeholder: 'Сначала выберите район' }
      f.input :lpu_description, input_html: { id: 'js-lpu-description', rows: 1, readonly: true }
      f.input :amount
    end
    f.buttons
  end

  show do |slide|
    attributes_table do
      row :lpu_description
      row :amount
    end
  end

  controller do
    actions :all, :except => [:update, :edit]

    def create
      params[:call_doctor_home_coupon].delete(:region_id)
      super
    end
  end

end
