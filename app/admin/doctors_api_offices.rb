ActiveAdmin.register DoctorsApiOffice do
  menu parent: 'Персонал', if: proc { can?(:manage, Office) }
  controller.authorize_resource
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'address_asc'
  filter :title_or_address, as: :string, label: "Названию или Адресу"

  scope :all
  scope :actived, default: true
  scope :deactived

  index do
    column :title
    column :address
    column :doctors_count do |row|
      if row.doctors.count > 0
        status_tag "#{row.doctors.count}", class: 'ok'
      else
        status_tag 'пусто'
      end
    end

    default_actions
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs "Основная информация" do
      f.input :title
      f.input :address, as: :string
      f.input :active, as: :boolean
    end
    f.inputs "Информация для синхронизации" do
      f.input :polisoms_id, as: :string
    end
    f.buttons
  end

  show do
    attributes_table do
      row :title
      row :address
      row :active do |row|
        if row.active
          status_tag "активен", class: 'ok'
        else
          status_tag 'выключен'
        end
      end
      row :doctors_count do |row|
        row.doctors.count
      end
      row :doctors do |row|
        row.doctors.map do |doctor|
          link_to doctor.full_name, admin_doctor_path(doctor)
        end.join('<br/>').html_safe
      end
    end
  end
end

