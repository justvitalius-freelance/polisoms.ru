# coding: utf-8

ActiveAdmin.register ExternalPoll do
  menu :label => "Опросы"
  config.batch_actions = false
  config.clear_sidebar_sections!
  config.sort_order = 'created_at_asc'

  index do
    column :comment
    column :active do |p|
      status_tag(
          (p.active ? "да" : "нет"),
          (p.active ? :ok : '')
      )
    end
    column :url do |p|
      link_to 'ссылка', external_poll_url(p.id), target: '_blank'
    end

    default_actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :comment, input_html: { rows: 3 }
      f.input :url, as: :string
      f.input :active
    end
    f.buttons
  end

  show do |slide|
    attributes_table do
      row :comment
      row :active do |p|
        status_tag(
            (p.active ? "да" : "нет"),
            (p.active ? :ok : '')
        )
      end
      row :url
      row :wrapper_url do |p|

        link_to external_poll_url(p.id), external_poll_url(p.id), target: '_blank'
      end
    end
  end

end
