ActiveAdmin.register Service do
  menu :if => proc{ can?(:manage, Service) }     
  controller.authorize_resource
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'created_at_desc'
  filter :region_id, collection: proc { Region.all }, as: :select
  filter :title
  #menu :parent => I18n.t('services')
 
    index do 
     column :title
     column :description do |service|
       truncate(strip_tags(service.description), length: 800).try(:html_safe)
     end
     column :appointment
     # column :service_category_id do |column|
     #   ServiceCategory.find(column.service_category_id).title
     # end
     column :region_id do |column|
      unless column.region_id.blank?
        Region.where(:id => column.region_id).first.name
      else
        'Все регионы'
      end
    end

     default_actions
   end

   form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs do
     f.input :title
     f.input :service_category_id, :as => :select, :collection => ServiceCategory.all, :include_blank => false
     f.input :region_id, :as => :select, :collection => Region.order('created_at ASC'), include_blank: false
     f.input :extra_type, :as => :select, :collection => Service::EXTRA_TYPES.map{|t| [I18n.t(t, scope: 'activerecord.attributes.service.extra_types'), t]}, include_blank: true
     f.input :description, :as => :ckeditor, :label => false, :input_html => { :toolbar => 'Full' }
     f.input :appointment
   end
   f.buttons
  end

   show do
         attributes_table do
           row :title
           row :description do |row|
             row.description.try(:html_safe)
           end
           row :appointment
           row :region_id
         end
       end
end
