ActiveAdmin.register VacancyRequest do
  menu parent: 'Блок с Вакансиями', if: proc { can?(:manage, Vacancy) }
  controller.authorize_resource
  config.batch_actions = false
  config.sort_order = 'created_at_desc'
  filter :vacancy_id, collection: proc { Vacancy.all }, as: :select
  actions :index


  index do
    column :name
    column :phone
    column :email
    column :vacancy
    column :created_at

    #default_actions
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs "Details" do
      f.input :region_id, :as => :select, :collection => Region.all
      f.input :title
      f.input :active
      f.input :image, :as => :file, :hint => (f.object.new_record? || !f.object.image?) ? nil : image_tag(f.object.image.url(:medium))
      f.input :description, :as => :ckeditor, :label => false, :input_html => { :toolbar => 'Full' }
    end
    f.buttons
  end

  show do
    attributes_table do
      row :region
      row :title
      row :region do |v|
        if v.active?
          status_tag 'активно', class: 'ok'
        else
          status_tag 'скрыто'
        end
      end
      row :image do |v|
        if v.image?
          image_tag(v.image.url(:medium))
        else
          nil
        end
      end
      row :description do |v|
        raw v.description
      end
    end
  end
end

