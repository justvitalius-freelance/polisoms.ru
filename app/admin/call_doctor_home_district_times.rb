# coding: utf-8

ActiveAdmin.register CallDoctorHomeDistrictTime do
  menu parent: 'Вызов врача на дом', label: 'Время записи районов'
  config.batch_actions = false
  config.clear_sidebar_sections!
  config.sort_order = 'created_at_asc'

  index do
    column :district_title
    column :end_call_home_time_text do |row|
      raw(row.end_call_home_time_text)
    end
    column :call_time do |row|
      "#{row.human_time_start} - #{row.human_time_end}"
    end
    column :phone
    column :actived do |v|
      if v.actived?
        status_tag 'активно', class: 'ok'
      else
        status_tag 'выключено'
      end
    end
    default_actions
  end

  form :html => { :enctype => "multipart/form-data", class: 'appointment-form js-call-home-district-time-admin' } do |f|
    f.inputs do
      f.input :district_id, input_html: { id: 'js-districts', placeholder: 'Выберите из списка' }
      f.input :district_title, input_html: { readonly: true, id: 'js-district-title' }
      f.input :call_home_time_start, as: :time_picker
      f.input :call_home_time_end, as: :time_picker
      f.input :phone
      f.input :end_call_home_time_text, input_html: { rows: 3, toolbar: 'Full' }, as: :ckeditor, label: false
      f.input :actived
    end
    f.buttons
  end

  show do
    attributes_table do
      row :district_title
      row :human_time_start
      row :human_time_end
      row :phone
      row :end_call_home_time_text do |row|
        raw row.end_call_home_time_text
      end
      row :actived do |v|
        if v.actived?
          status_tag 'активно', class: 'ok'
        else
          status_tag 'выключено'
        end
      end
    end
  end

end
