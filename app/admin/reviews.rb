# coding: utf-8
ActiveAdmin.register Review do
  menu :if => proc { can?(:manage, Review) }
  controller.authorize_resource
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'created_at_desc'
  filter :region_id, collection: proc { Region.all }, as: :select
  filter :office_id, collection: proc { Office.all }, as: :select
  filter :check, as: :select
  filter :status, collection: proc { ['Positive', 'Negative'] }, as: :select

  index do
    column :status do |review|
      status_tag(
          (review.status.try(:underscore) == 'negative' ? "Отрицательный" : "Положительный"),
          (review.status.try(:underscore) == 'negative' ? :error : :ok)
      )
    end

    column('Отвечали?') do |review|
      status_tag(
          (review.answers.count > 0 ? "Обработан" : "Без ответа"),
          (review.answers.count > 0 ? :ok: :error)
      )
    end

    column('Опубликован?') do |review|
      status_tag(
          (review.check ? "Виден" : "Закрыт"),
          (review.check ? :ok : :error)
      )
    end
    
    column :author
    column :contact
    # column :description do |column|
    #   truncate(strip_tags(column.description), length: 200)
    # end

    column :region
    column :office

    column :created_at
    # default_actions
    # default_actions
    column 'Действия' do |resource|
      links = []
      links << link_to(I18n.t('active_admin.view'), resource_path(resource))
      # if !resource.is_active?
      links << link_to(
          content_tag(:i, nil, class: 'fa fa-lg fa-edit'),
          edit_resource_path(resource),
          title: I18n.t('active_admin.edit')
      )
      # end
      links << link_to(
          content_tag(:i, nil, class: 'fa fa-trash fa-lg text-red'),
          resource_path(resource),
          confirm: 'Удаленный регион нельзя восстановить. Прикрепленные статьи и регионы останутся. Удалить? ',
          method: :delete,
          title: I18n.t('active_admin.delete')
      )
      links.join('   ').html_safe
    end
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs "Details" do
      f.input :region_id, :as => :select, :collection => Region.all
      f.input :status, input_html: { disabled: true }
      f.input :office, input_html: { disabled: true }
      f.input :description
      f.input :check
    end
    f.buttons
  end


  show do
    review_answer = %Q{

С уважением,
Луковникова Александра
специалист службы контроля качества
Проект "Полис" Euromed group

service@polisoms.ru
(812) 670 06 03
}
    div(class: 'hidden') do
      review_answer = render("/reviews_mailer/texts/#{params[:type]}", review: resource) if params[:type]
    end
    attributes_table do
      row :region_id
      row :status do |review|
        status_tag(
            (review.status.try(:underscore) == 'negative' ? "Благодарность" : "Жалоба"),
            (review.status.try(:underscore) == 'negative' ? :error : :ok)
        )
      end
      row :office_id
      row :subject
      row :doctors do |row|
        if row.doctors.present?
          Doctor.where(id: row.doctors.split(',')).map{ |doctor| doctor.full_name }
              .concat(row.custom_doctor_names || [])
              .join('<br/>')
              .html_safe
        end
      end
      row('Опубликован?') do |review|
        status_tag(
            (review.check ? "Виден" : "Закрыт"),
            (review.check ? :ok : :error)
        )
      end
      row :author
      row :contact
      row :description do |row|
        truncate(strip_tags(row.description), length: 200)
      end
      row('Отвечали?') do |review|
        status_tag(
            (review.answers.count > 0 ? "Обработан" : "Без ответа"),
            (review.answers.count > 0 ? :ok : :error)
        )
      end
    end
    panel 'Ответы на отзывы' do
      table_for review.answers do
        column(ReviewAnswer.human_attribute_name(:text)) { |answer| simple_format answer.text.html_safe }
        column(ReviewAnswer.human_attribute_name(:visibility)) do |answer|
          status_tag(
              (answer.on_site? ? "Имейл+сайт " : "Только имейл"),
              (answer.on_site? ? :ok : :error)
          )
        end
        column(ReviewAnswer.human_attribute_name(:created_at)) { |answer| I18n.l answer.created_at, format: :day_hour }
      end

    end
    div do
      semantic_form_for [:admin, resource, resource.answers.build],
                        builder: ActiveAdmin::FormBuilder do |f|
        f.object.active_review = true
        f.object.text = strip_tags(review_answer.to_s)
        f.inputs "Новый ответ (будет отправлен на email автору)" do
          f.input :active_review, as: :boolean
          f.input :on_site, as: :boolean
          f.input :text
        end
        f.actions
      end
    end
  end

  # автоматические ответы на имейлы
  member_action :default_reply, :method => :get do
    if params.key?(:type)
      review = Review.find(params[:id])
      mediator = ReviewMediator.new(review)
      mediator.default_answer(params[:type], self)
    end
    redirect_to admin_reviews_path, mediator.message
  end


  # кастомные ответы на отзывы
  member_action :answer, :method => :post do
    if params.key?(:review_answer)
      review = Review.find(params[:id])

      # активируем или не активируем отзыв из ответа
      is_active_review = params[:review_answer].delete(:active_review) || false
      review.update_attribute(:check, is_active_review)

      mediator = ReviewMediator.new(review)
      answer_text = params[:review_answer][:text]
      answer_site_visible = params[:review_answer][:on_site]
      mediator.answer_with(answer_text, answer_site_visible)

    end
    redirect_to admin_review_url(review), mediator.message
  end

end


