ActiveAdmin.register Region do
  menu :if => proc{ can?(:manage, Region) }
  controller.authorize_resource
  config.batch_actions = false
  config.clear_sidebar_sections!
  config.sort_order = 'created_at_desc'

  # клонирование региона через amoeba
  member_action :clone, :method => :put do
    region = Region.find(params[:id])
    new_region = region.amoeba_dup
    new_region.active = false
    new_region.save!
    # redirect_to admin_regions_path, { :notice => "Регион «#{region.name}» скопирован" }
    redirect_to edit_resource_path(new_region), { :notice => "Регион «#{region.name}» скопирован. Измените параметры для него." }
  end

  # активация региона
  member_action :active, :method => :put do
    region = Region.find(params[:id])
    region.active = !region.active
    region.save!
    redirect_to admin_regions_path, { :notice => "Регион «#{region.name}» обновлен" }
  end

  index do
    # column :id
    column :name, sortable: true
    column :contents, sortable: false
    column :active, sortable: :active do |resource|
      if resource.active?
        link_to active_admin_region_path(resource), method: :put, title: 'деактивировать' do
          content_tag(:i, nil, class: 'fa fa-lg fa-circle text-green') +
          content_tag(:span, '  деактивировать')
        end.html_safe
      else
        link_to active_admin_region_path(resource), method: :put, title: 'активировать' do
          content_tag(:i, nil, class: 'fa fa-lg fa-exclamation-triangle text-red')+
          content_tag(:span, '  активировать')
        end
      end
    end
    # column :invite
    # column :appointment_link
    # column :phone

    # default_actions
    column :actions do |resource|
      links = []
      links << link_to(I18n.t('active_admin.view'), resource_path(resource))
      # if !resource.is_active?
      links << link_to(
          content_tag(:i, nil, class: 'fa fa-lg fa-edit'),
          edit_resource_path(resource),
          title: I18n.t('active_admin.edit')
      )
      # end
      links << content_tag(:span, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.html_safe)
      links << link_to('Клонировать', clone_admin_region_path(resource), :confirm => 'Клонирование создает дупликаты всех статей, отделений и другого контента для региона. По умолчанию регион будет деактивирвоан. Клонировать?', :method => :put)
      links << link_to(
          content_tag(:i, nil, class: 'fa fa-trash fa-lg text-red'),
          resource_path(resource),
          confirm: 'Удаленный регион нельзя восстановить. Прикрепленные статьи и регионы останутся. Удалить? ',
          method: :delete,
          title: I18n.t('active_admin.delete')
      )
      links.join('&nbsp;&nbsp;&nbsp;').html_safe
    end
    #- See more at: http://www.isotope11.com/blog/active-admin-default-action-override#sthash.p5hOBDFK.dpuf
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Форма редактирования" do
      f.input :name
      f.input :active
      f.input :invite
      f.input :appointment_link
      f.input :phone
      f.input :work_time, :as => :ckeditor, :label => false, :input_html => { :toolbar => 'Full' }
    end
    f.buttons
    f.inputs 'Подсказка' do
      image_tag 'region-form-instruction.png'
    end

  end

  show do
    attributes_table do
      row :id
      row :name
      row :active
      # row :invite
      row :appointment_link
      row :phone
      row :work_time do |region|
        strip_tags(region.work_time).try(:html_safe)
      end
    end
  end

end
