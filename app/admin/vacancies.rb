ActiveAdmin.register Vacancy do
  menu parent: 'Блок с Вакансиями', if: proc { can?(:manage, Vacancy) }
  controller.authorize_resource
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'created_at_desc'
  filter :region_id, collection: proc { Region.all }, as: :select


  index do
    column :category do |v|
      image_tag v.category_image_path
    end
    column :title
    column :active do |v|
      if v.active?
        status_tag 'активно', class: 'ok'
      else
        status_tag 'скрыто'
      end
    end
    column :region

    default_actions
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs 'Иконка категории' do
      content_tag :ol do
        content_tag :li do
          (1..Vacancy::CATEGORIES_COUNT).map do |num|
            label_tag do
              radio_button_tag('vacancy[category_id]', num, f.object.category_id == num) +
              image_tag(Vacancy.gen_category_image_path(num))
            end
          end.join('').html_safe
        end
      end
    end
    f.inputs "Детали вакансии" do
      f.input :region_id, :as => :select, :collection => Region.all
      f.input :title
      f.input :active
      f.input :image, :as => :file, :hint => (f.object.new_record? || !f.object.image?) ? nil : image_tag(f.object.image.url(:medium))
      f.input :description, :as => :ckeditor, :label => false, :input_html => { :toolbar => 'Full' }
    end
    f.buttons
  end

  show do
    attributes_table do
      row :region
      row :title
      row :category do |v|
        image_tag v.category_image_path
      end
      row :active do |v|
        if v.active?
          status_tag 'активно', class: 'ok'
        else
          status_tag 'скрыто'
        end
      end
      row :image do |v|
        if v.image?
          image_tag(v.image.url(:medium))
        else
          nil
        end
      end
      row :description do |v|
        raw v.description
      end
    end
  end
end

