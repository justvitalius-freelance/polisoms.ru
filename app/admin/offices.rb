ActiveAdmin.register Office do
  # Старая реализация офисов. Сейчас в работе  модель DoctorsApiOffice
  # menu :if => proc { can?(:manage, Office) }
  menu :if => proc { false }
  controller.authorize_resource
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'created_at_desc'
  filter :region_id, collection: proc { Region.all }, as: :select


  index do
    column :title
    column :adress
    column :region

    default_actions
  end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs "Details" do
      f.input :region_id, :as => :select, :collection => Region.all
      f.input :title
      f.input :adress
    end
    f.buttons
  end

  show do
    attributes_table do
      row :region_id
      row :title
      row :adress
    end
  end
end

