# coding: utf-8

ActiveAdmin.register Slide do
  menu :label => "Слайдер"
  config.batch_actions = false
  # config.clear_sidebar_sections!
  config.sort_order = 'position_asc'
  filter :region_id, collection: proc { Region.all }, as: :select

  index do
    column :position
    column :title
    column :region


    default_actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :title
      f.input :text, :input_html => { rows: 3 }
      f.input :link_title
      f.input :link_url
      f.input :position
      f.input :region_id, :as => :select, :collection => Region.all
      f.input :image
    end
    f.buttons
  end

  show do |slide|
    attributes_table do
      row :position
      row :title
      row :text do
        simple_format slide.text
      end
      row :link do
        link_to slide.link_title, slide.link_url
      end
      row :image do
        image_tag slide.image.url(:content)
      end

      row :region
    end
  end

end
