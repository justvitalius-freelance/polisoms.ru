ActiveAdmin.register Doctor do
  menu parent: 'Персонал', if: proc { can?(:manage, Doctor) }
  controller.authorize_resource
  config.batch_actions = false
  filter :polisoms_office_id, collection: proc { DoctorsApiOffice.actived }, as: :select
  filter :name_or_surname_or_second_name_or_job, as: :string, label: "ФИО и Должности"
  config.sort_order = 'surname_asc'

  scope :all
  scope :actived, default: true
  scope :deactived

  index do
    column :avatar do |row|
      image_tag row.photo.url(:avatar), width: '100px'
    end
    column :full_name
    column :job
    column :polisoms_office do |row|
      row.polisoms_office.try(:title)
    end

    default_actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Основная информация" do
      f.input :surname
      f.input :name
      f.input :second_name
      f.input :job
      f.input :active, as: :boolean
      f.input :photo, :as => :file, :hint => (f.object.new_record? || !f.object.photo?) ? nil : image_tag(f.object.photo.url(:avatar))
      unless f.object.new_record? || !f.object.photo?
        f.input :delete_photo, :as => :boolean, :label => I18n.t('destroy_image'), :wrapper_html => { :class => "important" }
      end
      f.input :resume
      f.input :polisoms_office_id, :as => :select, :collection => DoctorsApiOffice.actived
    end

    f.inputs "Информация для синхронизации" do
      f.input :id_polisoms, as: :string
    end
    f.buttons
  end

  show do
    attributes_table do
      row :avatar do |doctor|
        image_tag doctor.photo.url(:avatar), width: '100px'
      end
      row :full_name
      row :job
      row :active do |row|
        if row.active
          status_tag "активен", class: 'ok'
        else
          status_tag 'выключен'
        end
      end
      row :polisoms_office do |doctor|
        content_tag :p, doctor.polisoms_office.title
      end
      row :resume do |row|
        truncate(strip_tags(row.resume), length: 500).html_safe
      end
    end
  end
end
