# coding: utf-8
module AppointmentsHelper


  # рисуем последовательност из 3х шагов
  # и выделем цветом шаг по значению переменной num
  # в методе очень важна последовательность блоков
  # т.к.некоторые методы имеют свойство float
  # для позиционирования на странице
  def render_steps_for_step num=1

    step1_css = 'm-active' if num >= 1
    step2_css = 'm-active' if num >= 2
    step3_css = 'm-active' if num >= 3

    content_tag(:div, class: 'steps') do
      content_tag(:div, '01. Личные данные', class: "step left #{step1_css}") +
      content_tag(:div, '03. Запись к врачу', class: "step right #{step3_css}") +
      content_tag(:div, '02. Выбор отделения', class: "step center #{step2_css}")
    end +
    back_button

  end


  # кнопка «назад»
  def back_button
    content_tag :div, class: 'back-btn js-back-btn' do
      link_to('← Назад', @back_path, class: 'link') if @back_path
    end
  end

  def error_messages
    unless flash[:error].blank?
      content_tag :div, flash[:error], class: 'error'
    end
  end

  private


end
