module CallDoctorHomeDistrictTimeHelper

  def time_in_period?(current_time, start_time, end_time)
    started = (start_time.strftime("%H") + start_time.strftime("%M")).to_i
    ended = (end_time.strftime("%H") + end_time.strftime("%M")).to_i
    current = (current_time.strftime("%H") + current_time.strftime("%M")).to_i

    started <= current && current <= ended
  end

end