# encoding: utf-8
class CallDoctorHomeDistrictTime < ActiveRecord::Base
  include CallDoctorHomeDistrictTimeHelper

  attr_accessible :district_id,
                  :call_home_time_start,
                  :call_home_time_end,
                  :end_call_home_time_text,
                  :district_title,
                  :actived,
                  :phone


  def can_call_home?
    if actived?
      time_in_period? Time.now, call_home_time_start, call_home_time_end
    else
      true
    end
  end

  def human_time_start
    "#{call_home_time_start.strftime('%H:%M')}"
  end

  def human_time_end
    "#{call_home_time_end.strftime('%H:%M')}"
  end

  def title
    "#{district_title}, #{human_time_start} - #{human_time_end}"
  end

end
