# encoding: utf-8
class Vacancy< ActiveRecord::Base
  attr_accessible :title,
                  :description,
                  :region_id,
                  :active,
                  :image,
                  :delete_image,
                  :category_id

  belongs_to :region

  validates_presence_of :title, :description, :region_id

  has_attached_file :image, :styles => { :medium => "300x300#" }

  CATEGORIES_COUNT = 6

  def category_image_path
    self.class.gen_category_image_path category_id
  end

  def self.gen_category_image_path(id)
    "icons/vacancy_categories/#{id || 1}.png"
  end
  
end
