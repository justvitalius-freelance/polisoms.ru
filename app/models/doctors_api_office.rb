class DoctorsApiOffice < ActiveRecord::Base
  attr_accessible :title, :polisoms_id, :address, :active
  has_many :doctors, foreign_key: 'polisoms_office_id'

  scope :actived, -> { where(active: true) }
  scope :deactived, -> { where(active: false) }

  def label
    address || title
  end

  def self.disable_all
    self.update_all active: false
  end
end
