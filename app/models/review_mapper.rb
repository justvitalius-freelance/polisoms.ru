class ReviewMapper

  attr_accessor :to_review
  def initialize(review_json = {})
    puts review_json.inspect
    puts review_json['doctorIds'].inspect
    @to_review = Review.new(
        author: review_json['name'],
        contact: review_json['contacts'],
        description: review_json['text'],
        office_id: review_json['officeId'],
        status: review_json['type'],
        doctors: review_json['doctorIds'],
        custom_doctor_names: review_json['customDoctors'],
        region_id: review_json['region_id'],
        subject: subject(review_json)
    )
    puts @to_review.inspect
  end

  private

  def subject(review_json)
    str = review_json['category']
    str += " > #{review_json['subcategory']}" unless review_json['subcategory'].blank?
    str
  end

end