class DoctorRequest < ActiveRecord::Base
  attr_accessible :full_name,
                  :birthday,
                  :address,
                  :phone_number,
                  :additional_phone_number,
                  :visit_at,
                  :department_id,
                  :department_name,
                  :region_id,
                  :region_name,
                  :called,
                  :comment

end
