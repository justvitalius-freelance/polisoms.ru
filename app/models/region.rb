class Region < ActiveRecord::Base
  attr_accessible :name, :invite, :appointment_link, :phone, :active, :work_time

  has_many :offices
  has_many :abouts
  has_many :reviews
  has_many :services
  has_many :slides

  scope :actived, where(active: true)

  amoeba do
    enable
    prepend :name => "(Копия) "
  end


  def contents
    str = ["офисов: #{offices.count} "]
    str << "отзывов: #{reviews.count} "
    str << "статей: #{abouts.count} "
    str << "услуг: #{services.count} "
    str.join(', ')
  end

end
