# encoding: utf-8
class CallDoctorHomeCoupon < ActiveRecord::Base
  attr_accessible :lpu_id,
                  :lpu_description,
                  :amount

  attr_accessor :region_id

end
