# coding: utf-8

class Appointment
  extend Savon::Model
  attr_accessor :district, :hospital
  # client wsdl: "http://194.8.177.242:62300/Service/HubService.svc?WSDL",
  client wsdl: "http://37.139.45.5:62300/Service/HubService.svc?WSDL",
         env_namespace: 'soapenv',
         log: true,
         log_level: :debug,
         namespace_identifier: :tem,
         namespaces: ({"xmlns:hub" => "http://schemas.datacontract.org/2004/07/HubService2"})
         
  # operations :get_doctors
  operations :get_district_list, :get_lpu_list, :check_patient, :get_spesiality_list,
             :get_doctor_list, :get_available_dates, :get_avaible_appointments, :set_appointment,
             :get_patient_history, :create_claim_for_refusal,
             :get_office_list,
             :get_org_list,
             :check_patient_by_passport

             # код доступа к системе
  # def guid
  #   'a87d14cb-99e1-c5b2-ff59-d59a71fe55ba'
  # end
  
  def guid
    'f40ec472-8e6a-42b2-a180-baa6d6e924c3'
  end
  
  def doctors
    get_doctors.to_array(:get_doctors_response, :get_doctors_result, :doctor)
  end
  
  # получаем районы
  def districts
    get_district_list.to_array(:get_district_list_response, :get_district_list_result, :district)
  end
  
  # получаем список ЛПУ (Лечебно-профилактических учреждений) по району
  def get_lpu_list(district_id)
    response = super('message' => { 
      'tem:IdDistrict' => district_id, 
      'tem:guid' => guid 
    })
    response.to_array(:get_lpu_list_response, :get_lpu_list_result, :list_lpu, :clinic)
  end
  
  # проверяем есть ли пациент в БД
  def check_patient(lpu_id, first_name, last_name, date_of_birth)
    response = super('message' => {
      'tem:pat' => { 'hub:Birthday' => Date.parse(date_of_birth).to_s,
                      'hub:Document_N' => nil,
                      'hub:Document_S' => nil,
                      'hub:IdPat' => nil,
                      'hub:Name' => first_name,
                      'hub:SecondName' => nil,
                      'hub:Surname' => last_name },
      'tem:idLpu' => lpu_id,
      'tem:guid' => guid
    })
    response = response.to_array(:check_patient_response, :check_patient_result, :id_pat)[0]
  end

  def check_patient_by_passport(lpu_id, patient_id, doc_series, doc_number)
    response = super('message' => {
        'tem:idPat' => patient_id,
        'tem:document_S' => doc_series,
        'tem:document_N' => doc_number,
        'tem:idLpu' => lpu_id,
        'tem:guid' => guid
    })
    response = response.to_array(:check_patient_by_passport_response, :check_patient_by_passport_result, :success)[0]
  end
  
  # получаем список специальностей врачей для ЛПУ
  def get_spesiality_list(lpu_id, pat_id)
    response = super('message' => {         
      'tem:idLpu' => lpu_id.to_i,
      'tem:idPat' => pat_id.to_i,
      'tem:guid' => guid
    })
    response.to_array(:get_spesiality_list_response, :get_spesiality_list_result, :list_spesiality, :spesiality)
  end
  
  # получаем список врачей для ЛПУ
  def get_doctor_list(idSpesiality, id_lpu, pat_id)
    response = super('message' => {  
      'tem:idSpesiality' => idSpesiality,     
      'tem:idLpu' => id_lpu.to_i,
      'tem:idPat' => pat_id.to_i,
      'tem:guid' => guid
    })
    response.to_array(:get_doctor_list_response, :get_doctor_list_result, :docs, :doctor)
  end
  
  # получаем список дат для записи на прием
  def get_available_dates(idDoc, id_lpu, visit_start, visit_end)
    response = super('message' => {  
      'tem:idDoc' => idDoc,     
      'tem:idLpu' => id_lpu, 
      'tem:visitStart' => visit_start, 
      'tem:visitEnd' => visit_end, 
      'tem:guid' => guid
    })
    response.to_array(:get_available_dates_response, :get_available_dates_result, :available_date_list, :date_time)
  end
  
  # получаем список возможных записией на прием 
  def get_avaible_appointments(idDoc, id_lpu, visit_start, visit_end)
    response = super('message' => {  
      'tem:idDoc' => idDoc,     
      'tem:idLpu' => id_lpu, 
      'tem:visitStart' => visit_start, 
      'tem:visitEnd' => visit_end, 
      'tem:guid' => guid
    })
    response.to_array(:get_avaible_appointments_response, :get_avaible_appointments_result, :list_appointments, :appointment)
  end
  
  # записываемся на прием
  def set_appointment(id_appointment, id_lpu, pat_id)
    response = super('message' => {  
      'tem:idAppointment' => id_appointment,     
      'tem:idLpu' => id_lpu, 
      'tem:idPat' => pat_id, 
      'tem:guid' => guid
    })
    response.to_array(:set_appointment_response, :set_appointment_result)
  end
  
  # получаем список записей на прием для пациента
  def get_patient_history(id_lpu, pat_id)
    response = super('message' => {      
      'tem:idLpu' => id_lpu.to_i,
      'tem:idPat' => pat_id.to_i,
      'tem:guid' => guid
    })
    response.to_array(:get_patient_history_response, :get_patient_history_result, :list_history_visit, :history_visit)
  end
  
  # отказываемся от записи не прием
  def create_claim_for_refusal(id_lpu, pat_id, appointment_id)
    response = super('message' => {      
      'tem:idLpu' => id_lpu, 
      'tem:idPat' => pat_id,
      'tem:idAppointment' => appointment_id,  
      'tem:guid' => guid
    })
    response.to_array(:create_claim_for_refusal_response, :create_claim_for_refusal_result)
  end
  
  
  
end