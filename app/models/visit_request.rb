# encoding: utf-8
class VisitRequest < ActiveRecord::Base

  attr_accessible :name,
                  :phone,
                  :birthday,
                  :email

  validates :name, :phone, :email, :birthday, length: { maximum: 250 }

end
