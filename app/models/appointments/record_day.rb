class Appointments::RecordDay
  attr_reader :id, :visit_start, :visit_end

  def initialize(obj)
    @obj= obj
    @id = @obj[:id_appointment]
    @visit_start = @obj[:visit_start]
    @visit_end = @obj[:visit_end]
  end
end