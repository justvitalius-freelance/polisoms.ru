class Slide < ActiveRecord::Base
  attr_accessible :link_url, :link_title, :position, :text, :title, :image, :region_id

  belongs_to :region

  has_attached_file :image, :styles => { :content => "948x390#" }
end
