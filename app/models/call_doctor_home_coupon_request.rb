# encoding: utf-8
class CallDoctorHomeCouponRequest < ActiveRecord::Base
  attr_accessible :lpu_id,
                  :patient_id


  scope :today, -> (lpu_id) { where(lpu_id: lpu_id).where(:created_at => Time.now.beginning_of_day..Time.now.end_of_day) }

end
