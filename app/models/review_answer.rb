class ReviewAnswer < ActiveRecord::Base
  attr_accessible :text,
                  :review_id,
                  :on_site

  attr_accessor :active_review

  belongs_to :review

  validates :text, presence: true

  scope :on_site, where(on_site: true)

end
