# encoding: utf-8
class Doctor < ActiveRecord::Base
  attr_accessible :name,
                  :surname,
                  :second_name,
                  :job,
                  :office_id,
                  :status,
                  :otpusk,
                  :priem1,
                  :priem2,
                  :text,
                  :photo,
                  :active,
                  :resume,
                  :polisoms_office_id,
                  :delete_photo,
                  :id_polisoms

  acts_as_votable

  belongs_to :office
  belongs_to :polisoms_office, class_name: 'DoctorsApiOffice'
  has_many :reviews

  scope :actived, -> { where(active: true) }
  scope :deactived, -> { where(active: false) }

  has_attached_file :photo,
                    styles: { avatar: '', content: '134x134#', profile: '' },
                    convert_options: {
                        avatar: '-gravity north -thumbnail 222x222^ -extent 222x222',
                        profile: '-gravity north -thumbnail 240x360^ -extent 240x360'
                    },
                    default_url: '/assets/default_doctor_avatar.png'
  
  def delete_photo=(value)
     @delete_photo = !value.to_i.zero?
   end

   def delete_photo
     !!@delete_photo
   end
   alias_method :delete_photo?, :delete_photo
   
    def clear_photo
     self.photo = nil if delete_photo? && !photo.dirty?
    end

    def full_name
      "#{surname} #{name} #{second_name}"
    end
  
end
