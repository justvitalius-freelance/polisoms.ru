class Service < ActiveRecord::Base
	EXTRA_TYPES=['about_polis']

  attr_accessible :description, :title, :appointment, :service_category_id, :region_id, :extra_type
  belongs_to :service_category
  belongs_to :region

  validates :extra_type, inclusion: EXTRA_TYPES, allow_blank: true

  


  
  define_index do
    indexes title
    indexes description
  end
  
  
end
