class About < ActiveRecord::Base
  attr_accessible :description, :title, :category, :region_id, :extra_type
  CATEGORIES = [ "About", "Usefull", "Vacancy" ]
  EXTRA_TYPES=['how_to_join', 'what_to_take']

  validates :title, :description, presence: true
  validates :category, inclusion: CATEGORIES
  validates :extra_type, inclusion: EXTRA_TYPES, allow_blank: true

  belongs_to :region

  define_index do
    indexes title
    indexes description
  end
  
end
