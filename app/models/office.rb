class Office < ActiveRecord::Base
  attr_accessible :title, :adress, :map, :region_id
  has_many :doctors
  has_many :articles
  has_many :reviews
  belongs_to :region

  
end
