# encoding: utf-8
# DTO модель для офиса, который получаем через soap
module Sync
  class Office

    attr_accessor :district_id, :office_id, :name, :address

    def initialize(office)
      @district_id = office[:id_district]
      @office_id = office[:id_office]
      @name = office[:name]
    end

    def to_model
      {
          polisoms_id: office_id,
          title: name,
          address: address,
          active: true
      }
    end

  end
end