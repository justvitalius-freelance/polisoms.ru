# encoding: utf-8
module Sync
  class Transport
    extend Savon::Model

    # client wsdl: "http://194.8.177.242:62300/PolisManagement/PolisOmsIntegrationService.svc?singleWsdl",
    client wsdl: "http://37.139.45.5:62300/PolisManagement/PolisOmsIntegrationService.svc?singleWsdl",
           env_namespace: 'soapenv',
           log: false,
           log_level: :debug,
           pretty_print_xml: true,
           namespace_identifier: :tem

    operations :get_office_list,
               :get_doctors,
               :get_specialty_list,
               :get_position_list

    def doctors
      get_doctors.to_array(:get_doctors_response, :get_doctors_result, :doctor)
    end

    def offices
      get_office_list
          .to_array(:get_office_list_response, :get_office_list_result, :office)
          .map do |doctor|
            Sync::Office.new(doctor)
          end
    end

    def specialities
      get_specialty_list.to_array(:get_specialty_list_response, :get_specialty_list_result, :key_value_item)
    end

    def positions
      get_position_list.to_array(:get_position_list_response, :get_position_list_result, :key_value_item)
    end

  end
end
