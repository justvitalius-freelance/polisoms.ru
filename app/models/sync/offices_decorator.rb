# набор хелперов над коллекцией офисов
module Sync
  class OfficesDecorator

    def self.apply_addresses(offices=[])

      offices
          .map{ |o| o.district_id }
          .uniq
          .map { |district_id| Appointment.new.get_lpu_list district_id }
          .flatten
          .map do |info|
            office = find(offices, info)
            office.address = info[:lpu_full_name] if office
          end

      offices

    end
    
    def self.find(offices=[], office)
      offices.find { |item| office[:id_lpu] == item.office_id }
    end

  end
end