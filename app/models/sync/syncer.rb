module Sync
  class Syncer

    require 'base64'

    def initialize
      @api = Sync::Transport.new
      @logger = Logger.new("#{Rails.root}/log/doctors_api_sync.log", 1, (2097152*10))
      log "========== Start Sync =========="
    end

    def start
      log "========== Fetch Doctors =========="
      @doctors = @api.doctors
      log "========== Fetch Specialities =========="
      @specialities = @api.specialities

      sync_offices

      log "========== Fetch Positions =========="
      @positions = @api.positions

      if @doctors && @doctors.length
        responsed_ids = []
        log "========== Import Doctors =========="
        # disable_all_doctors
        # disable_all_offices
        @doctors.each_with_index do |doctor_response, i|
          if id = uniq_id(doctor_response)
            doctor = Doctor.where(id_polisoms: id).first_or_create!
            begin
              responsed_ids.push(id)
              doctor.name = doctor_response[:name] unless doctor_response[:name].class == Hash
              doctor.second_name = doctor_response[:second_name] unless doctor_response[:second_name].class == Hash
              doctor.surname = doctor_response[:surname] unless doctor_response[:surname].class == Hash
              doctor.job = job doctor_response
              doctor.photo = avatar_file(doctor_response) if avatar_base64(doctor_response)
              doctor.polisoms_office = connect_office doctor_response
              doctor.active = true
              doctor.resume = doctor_response[:resume] unless doctor_response[:resume].class == Hash

              doctor.save!
              log "Doctor ##{i+1} with id=#{id} sucessfull updated"
            rescue Exception => e
              log "Doctor ##{i+1} has errors. #{e}\n"
            end
          end
        end
        log "\n Was imported #{@doctors.count} doctors"
      else
        log "No doctors to import"
      end
      disable_doctors(responsed_ids)
      log "========== Sync Finish =========="
    end


  private

    def sync_offices
      log "========== Fetch Offices =========="
      @offices = Sync::OfficesDecorator.apply_addresses(@api.offices)
      DoctorsApiOffice.disable_all if @offices && @offices.length
    end

    def uniq_id(doctor)
      doctor_position = position(doctor)
      if doctor_position
        doctor_position[:id_mis_position]
      end
    end

    def job(doctor)
      doctor_position = position(doctor)
      # puts "#{doctor[:name]} #{doctor[:surname]} #{doctor_position.inspect}"
      if doctor_position
        speciality = @specialities.select do |_speciality|
          _speciality[:key].to_s == doctor_position[:id_doctors_specialyty].to_s
        end
        if speciality && speciality.length && speciality.first.has_key?(:value)
          speciality = speciality.first[:value]
          speciality = 'Врач общей практики' if speciality == 'ВОП'
          speciality
        end
      end
    end

    def position(doctor)
      if doctor.has_key? :positions_list
        if doctor[:positions_list].has_key? :position
          doctor_position = doctor[:positions_list][:position]
          if doctor_position.class == Array
            doctor_position.first
          else
            doctor_position
          end
        end
      end
    end

    def avatar_base64(doctor)
      if doctor.has_key? :avatar
        if doctor[:avatar].has_key? :bytes
          doctor[:avatar][:bytes]
        end
      end
    end

    def avatar_decode64(doctor)
      Base64.decode64(avatar_base64 doctor)
    end

    def avatar_file(doctor)
      avatar_name = "doctor_avatar_#{uniq_id(doctor)}"
      file_path = "tmp/#{avatar_name}"
      File.open(file_path, 'wb') do |file|
        file.write avatar_decode64(doctor)
      end
      File.new(file_path, 'r')
    end

    def office_id(doctor)
      if position doctor
        position(doctor)[:id_office]
      end
    end

    def connect_office(doctor)
      finded_office = @offices.find { |_office| _office.office_id == office_id(doctor) }
      if finded_office
        office = DoctorsApiOffice.where(finded_office.to_model).first_or_create!
        office.active = true
        office.save!
      end
    end

    def disable_all_doctors
      Doctor.all.each { |d| d.active = false; d.save!}
      log "Was disabled #{Doctor.count} doctors before sync"
    end

    def disable_doctors(not_disable_ids)
      doctors = Doctor.where(['id_polisoms NOT IN (?)', not_disable_ids])
      doctors.all.each { |d| d.active = false; d.save! }
      log "Was disabled #{doctors.count} doctors in sync"
    end

    def disable_all_offices
      log "Will destroy #{DoctorsApiOffice.count} remote offices before sync"
      DoctorsApiOffice.destroy_all
    end

    def log(txt)
      @logger.info "#{DateTime.now.strftime('%d/%m/%Y, %H:%M:%L')} — #{txt}"
      puts txt
    end

  end
end
