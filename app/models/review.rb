class Review < ActiveRecord::Base
  attr_accessible :author,
                  :title,
                  :contact,
                  :description,
                  :doctor_id,
                  :office_id,
                  :status,
                  :check,
                  :doctors,
                  :region_id,
                  :subject,
                  :custom_doctor_names

  serialize :doctors
  serialize :custom_doctor_names

  belongs_to :doctor
  belongs_to :office, class_name: 'DoctorsApiOffice'
  belongs_to :region

  # has_many :review_answers, as: :answers
  has_many :answers, foreign_key: 'review_id', class_name: 'ReviewAnswer'

  PHONE_REGEXP= /^\+?([\d*\s?\-])+\d$/
  EMAIL_REGEXP = /^\w+(\w+\-?\.?\+?)+@\w+\.\w{2,5}$/

  def has_email?
    !contact.match(EMAIL_REGEXP).nil?
  end


  def has_phone?
    !contact.match(PHONE_REGEXP).nil?
  end

  def title
    status == 'Positive' ? 'Благодарность' : 'Жалоба'
  end

  def custom_doctors
    custom_doctor_names.join(', ')
  end
end
