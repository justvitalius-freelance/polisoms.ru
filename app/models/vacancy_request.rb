# encoding: utf-8
class VacancyRequest < ActiveRecord::Base

  attr_accessible :name,
                  :phone,
                  :email,
                  :vacancy_id

  belongs_to :vacancy

  validates :name, :phone, :email, length: { maximum: 250 }
  validates_presence_of :vacancy_id

end
