﻿!(function (undefined) {

    if (navigator.userAgent.search("MSIE") >= 0) {
        var position = navigator.userAgent.search("MSIE") + 5;
        var end = navigator.userAgent.search("; Windows");
        var version = navigator.userAgent.substring(position, end);
        if (parseInt(version, 10) < 8) {
            return;
        }
    }

    var ROOT = this,
        initedWidget,
        defRootName = "zdrav";

    if (!ROOT[defRootName]) {
        ROOT[defRootName] = {};

        var rootObj = ROOT[defRootName];

        var Esi = function (params) {
            //this.init(params);
        };
    }
    var CONFIG = {

        //Default search params
        searchField: "q",
        formAction: "/search/",
        onSiteCheckbox: false,

        //About data url
        ABOUT_INFO: "/static/sites",

        //Path to iframe
        FRAME_PATH: "iframe.html",

        //params
        allowedExtend: ["searchField", "formAction", "style", "onSiteCheckbox"],

        //salt
        salt: new Date().getTime(),

        //cookie name
        cookie_name: "zdrav_state",

        //jsonp timeout fallback for ie
        jsonp_timeout: 1000,

        //iframe Start height
        iframe_height: "50px",

        //debug
        debug: false
    };
    var salty = function (name) {
        return name + CONFIG.salt;
    };


// url normalization
    var normalizeUrl = function (url) {
        var re = /^https?:\/\//i;

        return re.test(url) ? url : "http://" + url;
    };

    var cleanHost = function (url) {
        if (url.indexOf("www.") === 0) {
            url = url.substring(4, url.length);
        }

        return url;
    };


    var core_trim = String.prototype.trim;

//Each iterator method
    var each = function (obj, iterator, context) {
        if (obj === null || obj === undefined) {
            return;
        }
        var nativeForEach = Array.prototype.forEach,
            hasOwn = Object.prototype.hasOwnProperty;

        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context);
        } else if (obj.length === +obj.length) {
            for (var i = 0, l = obj.length; i < l; i++) {
                iterator.call(context, obj[i], i, obj);
            }
        } else {
            for (var key in obj) {
                if (hasOwn.call(obj, key)) {
                    iterator.call(context, obj[key], key, obj);
                }
            }
        }
    };


    var trim = core_trim && !core_trim.call("\uFEFF\xA0") ?
        function (text) {
            return text == null ?
                "" :
                core_trim.call(text);
        } :

        // Otherwise use our own trimming functionality
        function (text) {
            var whitespace = "[\\x20\\t\\r\\n\\f]",
                rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g");

            return text == null ?
                "" :
                ( text + "" ).replace(rtrim, "");
        };


//Array indexOf
    var indexOf = function (array, item) {
        if (array === null || array === undefined) return -1;
        var i, l,
            nativeIndexOf = Array.prototype.indexOf;

        if (nativeIndexOf && array.indexOf === nativeIndexOf) {
            return array.indexOf(item);
        }
        for (i = 0, l = array.length; i < l; i++) {
            if (i in array && array[i] === item) return i;
        }
        return -1;
    };


//Each extend object method
    var extend = function (obj) {
        each(Array.prototype.slice.call(arguments, 1), function (source) {
            for (var prop in source) {
                obj[prop] = source[prop];
            }
        });
        return obj;
    };


//class types collection
    var class2type = {};
    each("Boolean Number String Function Array Date RegExp Object".split(" "), function (name, i) {
        class2type[ "[object " + name + "]" ] = name.toLowerCase();
    });
    var getType = function (obj) {
        var core_toString = Object.prototype.toString;
        return obj == null ?
            String(obj) :
            class2type[ core_toString.call(obj) ] || "object";
    };

//isFunction method
    var isFunction;
    if (typeof (/./) !== 'function') {
        isFunction = function (obj) {
            return typeof obj === 'function';
        };
    } else {
        isFunction = function (obj) {
            var toString = Object.prototype.toString;
            toString.call(obj) == '[object Function]';
        };
    }


    var isWindow = function (obj) {
        return obj != null && obj == obj.window;
    };

//isArray
    var isArray = Array.isArray || function (obj) {
        var toString = Object.prototype.toString;
        return toString.call(obj) == '[object Array]';
    };

//isObject
    var isObject = function (obj) {
        return obj === Object(obj);
    };

    var isPlainObject = function (obj) {
        var core_hasOwn = Object.prototype.hasOwnProperty;
        // Must be an Object.
        // Because of IE, we also have to check the presence of the constructor property.
        // Make sure that DOM nodes and window objects don't pass through, as well
        if (!obj || getType(obj) !== "object" || obj.nodeType || isWindow(obj)) {
            return false;
        }

        try {
            // Not own constructor property must be Object
            if (obj.constructor && !core_hasOwn.call(obj, "constructor") && !core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                return false;
            }
        } catch (e) {
            // IE8,9 Will throw exceptions on certain host objects #9897
            return false;
        }

        // Own properties are enumerated firstly, so to speed up,
        // if last one is own, then all properties are own.

        var key;
        for (key in obj) {
        }

        return key === undefined || core_hasOwn.call(obj, key);
    };


    function buildParams(prefix, obj, traditional, add) {
        var name,
            rbracket = /\[\]$/;

        if (isArray(obj)) {
            // Serialize array item.
            each(obj, function (v, i) {
                if (traditional || rbracket.test(prefix)) {
                    // Treat each array item as a scalar.
                    add(prefix, v);

                } else {
                    // If array item is non-scalar (array or object), encode its
                    // numeric index to resolve deserialization ambiguity issues.
                    // Note that rack (as of 1.0.0) can't currently deserialize
                    // nested arrays properly, and attempting to do so may cause
                    // a server error. Possible fixes are to modify rack's
                    // deserialization algorithm or to provide an option or flag
                    // to force array serialization to be shallow.
                    buildParams(prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add);
                }
            });

        } else if (!traditional && getType(obj) === "object") {
            // Serialize object item.
            for (name in obj) {
                buildParams(prefix + "[" + name + "]", obj[ name ], traditional, add);
            }

        } else {
            // Serialize scalar item.
            add(prefix, obj);
        }
    }

    var param = function (a, traditional) {
        var prefix,
            s = [],
            r20 = /%20/g,
            add = function (key, value) {
                // If value is a function, invoke it and return its value
                value = isFunction(value) ? value() : ( value == null ? "" : value );
                s[ s.length ] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
            };

        // If an array was passed in, assume that it is an array of form elements.
        if (isArray(a) || ( !isPlainObject(a) )) {
            // Serialize the form elements
            each(a, function (value, name) {
                add(name, value);
            });

        } else {
            // If traditional, encode the "old" way (the way 1.3.2 or older
            // did it), otherwise encode params recursively.
            for (prefix in a) {
                buildParams(prefix, a[ prefix ], traditional, add);
            }
        }

        // Return the resulting serialization
        return s.join("&").replace(r20, "+");
    };

    var localJSON = {};

    (function () {
        'use strict';

        var hasJSON = ROOT.JSON ? true : false;

        function f(n) {
            // Format integers to have at least two digits.
            return n < 10 ? '0' + n : n;
        }

        if (typeof Date.prototype.toJSON !== 'function') {

            Date.prototype.toJSON = function (key) {

                return isFinite(this.valueOf())
                    ? this.getUTCFullYear() + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate()) + 'T' +
                    f(this.getUTCHours()) + ':' +
                    f(this.getUTCMinutes()) + ':' +
                    f(this.getUTCSeconds()) + 'Z'
                    : null;
            };

            String.prototype.toJSON =
                Number.prototype.toJSON =
                    Boolean.prototype.toJSON = function (key) {
                        return this.valueOf();
                    };
        }

        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap,
            indent,
            meta = {    // table of character substitutions
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"': '\\"',
                '\\': '\\\\'
            },
            rep;


        function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

            escapable.lastIndex = 0;
            return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string'
                    ? c
                    : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' : '"' + string + '"';
        }


        function str(key, holder) {

// Produce a string from holder[key].

            var i,          // The loop counter.
                k,          // The member key.
                v,          // The member value.
                length,
                mind = gap,
                partial,
                value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

            if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
                value = value.toJSON(key);
            }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

            if (typeof rep === 'function') {
                value = rep.call(holder, key, value);
            }

// What happens next depends on the value's type.

            switch (typeof value) {
                case 'string':
                    return quote(value);

                case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

                    return isFinite(value) ? String(value) : 'null';

                case 'boolean':
                case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

                    return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

                case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

                    if (!value) {
                        return 'null';
                    }

// Make an array to hold the partial results of stringifying this object value.

                    gap += indent;
                    partial = [];

// Is the value an array?

                    if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                        length = value.length;
                        for (i = 0; i < length; i += 1) {
                            partial[i] = str(i, value) || 'null';
                        }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                        v = partial.length === 0
                            ? '[]'
                            : gap
                            ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                            : '[' + partial.join(',') + ']';
                        gap = mind;
                        return v;
                    }

// If the replacer is an array, use it to select the members to be stringified.

                    if (rep && typeof rep === 'object') {
                        length = rep.length;
                        for (i = 0; i < length; i += 1) {
                            if (typeof rep[i] === 'string') {
                                k = rep[i];
                                v = str(k, value);
                                if (v) {
                                    partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                }
                            }
                        }
                    } else {

// Otherwise, iterate through all of the keys in the object.

                        for (k in value) {
                            if (Object.prototype.hasOwnProperty.call(value, k)) {
                                v = str(k, value);
                                if (v) {
                                    partial.push(quote(k) + (gap ? ': ' : ':') + v);
                                }
                            }
                        }
                    }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

                    v = partial.length === 0
                        ? '{}'
                        : gap
                        ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                        : '{' + partial.join(',') + '}';
                    gap = mind;
                    return v;
            }
        }

// If the JSON object does not yet have a stringify method, give it one.

        if (!hasJSON || typeof JSON.stringify !== 'function') {
            localJSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

                var i;
                gap = '';
                indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

                if (typeof space === 'number') {
                    for (i = 0; i < space; i += 1) {
                        indent += ' ';
                    }

// If the space parameter is a string, it will be used as the indent string.

                } else if (typeof space === 'string') {
                    indent = space;
                }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

                rep = replacer;
                if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                        typeof replacer.length !== 'number')) {
                    throw new Error('JSON.stringify');
                }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

                return str('', {'': value});
            };
        } else {
            localJSON.stringify = JSON.stringify;
        }


// If the JSON object does not yet have a parse method, give it one.

        if (!hasJSON || typeof JSON.parse !== 'function') {
            localJSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

                var j;

                function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                    var k, v, value = holder[key];
                    if (value && typeof value === 'object') {
                        for (k in value) {
                            if (Object.prototype.hasOwnProperty.call(value, k)) {
                                v = walk(value, k);
                                if (v !== undefined) {
                                    value[k] = v;
                                } else {
                                    delete value[k];
                                }
                            }
                        }
                    }
                    return reviver.call(holder, key, value);
                }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

                text = String(text);
                cx.lastIndex = 0;
                if (cx.test(text)) {
                    text = text.replace(cx, function (a) {
                        return '\\u' +
                            ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                    });
                }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

                if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                    j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                    return typeof reviver === 'function'
                        ? walk({'': j}, '')
                        : j;
                }

// If the text is not JSON parseable, then a SyntaxError is thrown.

                throw new SyntaxError('JSON.parse');
            };
        } else {
            localJSON.parse = JSON.parse;
        }
    }());
//Cookie object

    var Cookie = (function () {
        var doc = document;

        return {

            //set cookie method
            set: function (name, value, props) {
                props = props || {};
                var exp = props.expires,
                    d, updatedCookie, propName, propValue;

                if (typeof exp == "number" && exp) {
                    d = new Date();
                    d.setTime(d.getTime() + exp * 1000);
                    exp = props.expires = d;
                }

                if (exp && exp.toUTCString) {
                    props.expires = exp.toUTCString();
                }

                value = encodeURIComponent(value);
                updatedCookie = name + "=" + value;

                for (propName in props) {
                    updatedCookie += "; " + propName;
                    propValue = props[propName];
                    if (propValue !== true) {
                        updatedCookie += "=" + propValue;
                    }
                }

                doc.cookie = updatedCookie;
            },

            //get cookie method by name
            get: function (name) {
                var matches = doc.cookie.match(new RegExp(
                        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));

                return matches ? decodeURIComponent(matches[1]) : undefined;
            },

            //delete cookie method by name
            del: function (name) {
                Cookie.set(name, null, { expires: -1 });
            }


        };
    }());
    var Dom = (function () {
        var doc = document;

        return {
            create: function (str) {
                var div = doc.createElement("div"),
                    elements, list = [];

                div.innerHTML = str;

                elements = div.childNodes;

                each(elements, function (item, i) {
                    list[i] = item;
                });

                return list;
            },
            appendTo: function (els, to) {
                each(els, function (item, i) {
                    to.appendChild(item);
                });
            },
            remove: function (el) {
                el.parentNode.removeChild(el);
            },
            clear: function (el) {
                var els = [],
                    _this = this;

                each(el.childNodes, function (item, i) {
                    els.push(item);
                });
                each(els, function (el) {
                    _this.remove(el);
                });
            },


            offset: function (elem) {
                var top, left;

                if (elem.getBoundingClientRect) {
                    var box = elem.getBoundingClientRect(),
                        body = document.body,
                        docElem = document.documentElement,
                        scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop,
                        scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft,
                        clientTop = docElem.clientTop || body.clientTop || 0,
                        clientLeft = docElem.clientLeft || body.clientLeft || 0;


                    top = box.top + scrollTop - clientTop;
                    left = box.left + scrollLeft - clientLeft;
                } else {
                    top = 0, left = 0;
                    while (elem) {
                        top = top + parseInt(elem.offsetTop, 10);
                        left = left + parseInt(elem.offsetLeft, 10);
                        elem = elem.offsetParent;
                    }
                }
                return {top: top, left: left};
            },

            getById: function (id) {
                var main = doc;
                //id = salty(id);

                return main.getElementById(id);
            },

            getStyle: function (oElm, strCssRule) {
                var strValue = "";
                if (document.defaultView && document.defaultView.getComputedStyle) {
                    strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
                }
                else if (oElm.currentStyle) {
                    strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1) {
                        return p1.toUpperCase();
                    });
                    strValue = oElm.currentStyle[strCssRule];
                }
                return strValue;
            },
            getStyleAsync: function (oElm, strCssRule, callback) {
                var _this = this;

                domReady(function () {
                    if (typeof callback === "function") {
                        callback(_this.getStyle(oElm, strCssRule));
                    }
                });
            },
            setStyleAttr: function (el, styles, important) {
                var style_arr = [];
                each(styles, function (value, name) {
                    style_arr.push(name + ":" + value + (important ? "!important" : ""));
                });

                el.style.cssText = style_arr.join(";");
            },
            setStyle: function (el, styles) {
                each(styles, function (value, name) {
                    el.style[name] = value;
                });
            },

            addClass: function (el, cls) {
                //cls = salty(cls);

                var re = new RegExp("(^|\\s)" + cls + "(\\s|$)", "g");
                if (re.test(el.className)) return;
                el.className = (el.className + " " + cls).replace(/\s+/g, " ").replace(/(^ | $)/g, "");
            },
            removeClass: function (el, cls) {
                //cls = salty(cls);

                var re = new RegExp("(^|\\s)" + cls + "(\\s|$)", "g");
                el.className = el.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "");
            }
        };
    }());
//domReady event listner
    /*!
     * domready (c) Dustin Diaz 2012 - License MIT
     */
    var domReady;
    !function (definition) {
        domReady = definition();
    }(function (ready) {

        var fns = [], fn, f = false,
            doc = document,
            testEl = doc.documentElement,
            hack = testEl.doScroll,
            domContentLoaded = 'DOMContentLoaded',
            addEventListener = 'addEventListener',
            onreadystatechange = 'onreadystatechange',
            readyState = 'readyState',
            loaded = /^loade|c/.test(doc[readyState]);

        function flush(f) {
            loaded = 1;
            while (f = fns.shift()) f();
        }

        doc[addEventListener] && doc[addEventListener](domContentLoaded, fn = function () {
            doc.removeEventListener(domContentLoaded, fn, f);
            flush();
        }, f);

        hack && doc.attachEvent(onreadystatechange, fn = function () {
            if (/^c/.test(doc[readyState])) {
                doc.detachEvent(onreadystatechange, fn);
                flush();
            }
        });

        return (ready = hack ?
            function (fn) {
                self != top ?
                    loaded ? fn() : fns.push(fn) :
                    function () {
                        try {
                            testEl.doScroll('left');
                        } catch (e) {
                            return setTimeout(function () {
                                ready(fn);
                            }, 50);
                        }
                        fn();
                    }();
            } :
            function (fn) {
                loaded ? fn() : fns.push(fn);
            });
    })


//Event object
    var Event = (function () {

        var guid = 0;

        var fixEvent = function (event) {
            event = event || window.event;

            if (event.isFixed) {
                return event;
            }
            event.isFixed = true;

            event.preventDefault = event.preventDefault || function () {
                this.returnValue = false;
            };
            event.stopPropagation = event.stopPropagaton || function () {
                this.cancelBubble = true;
            };

            if (!event.target) {
                event.target = event.srcElement;
            }

            if (!event.relatedTarget && event.fromElement) {
                event.relatedTarget = event.fromElement == event.target ? event.toElement : event.fromElement;
            }

            if (event.pageX === null && event.pageX === undefined && event.clientX !== null && event.clientX !== undefined) {
                var html = document.documentElement, body = document.body;
                event.pageX = event.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
                event.pageY = event.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
            }

            event.isNatural = (function () {
                if (event.pageX === 0 && event.pageY === 0) { //if user clicked in left corner, well he is lucky
                    return false;
                }
                return true;
            }());

            if (!event.which && event.button) {
                event.which = (event.button & 1 ? 1 : ( event.button & 2 ? 3 : ( event.button & 4 ? 2 : 0 ) ));
            }

            return event;
        };

        function commonHandle(event) {
            var handlers, handler, ret, g;
            event = fixEvent(event);

            handlers = this.events[event.type];

            for (g in handlers) {
                handler = handlers[g];

                ret = handler.call(this.context || this, event);
                if (ret === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
        }

        return {
            add: function (elem, type, handler, context) {
                if (elem.setInterval && ( elem != window && !elem.frameElement )) {
                    elem = window;
                }

                if (!handler.guid) {
                    handler.guid = ++guid;
                }

                if (!elem.events) {
                    elem.events = {};
                    elem.context = context;
                    elem.handle = function (event) {
                        if (typeof Event !== "undefined") {
                            return commonHandle.call(elem, event);
                        }
                    };
                }

                if (!elem.events[type]) {
                    elem.events[type] = {};

                    if (elem.addEventListener) {
                        elem.addEventListener(type, elem.handle, false);
                    } else if (elem.attachEvent) {
                        elem.attachEvent("on" + type, elem.handle);
                    }
                }

                elem.events[type][handler.guid] = handler;
            },

            remove: function (elem, type, handler) {
                var any,
                    handlers = elem.events && elem.events[type];

                if (!handlers) return;

                delete handlers[handler.guid];

                for (any in handlers) return false;

                if (elem.removeEventListener) {
                    elem.removeEventListener(type, elem.handle, false);
                } else if (elem.detachEvent) {
                    elem.detachEvent("on" + type, elem.handle);
                }

                delete elem.events[type];

                for (any in elem.events) return false;
                try {
                    delete elem.handle;
                    delete elem.events;
                } catch (e) { // IE
                    elem.removeAttribute("handle");
                    elem.removeAttribute("events");
                }
            }
        };
    }());
    /*
     * FlyJSONP v0.2
     * http://alotaiba.github.com/FlyJSONP
     *
     * FlyJSONP is a small JavaScript library, that allows you to do
     * cross-domain GET and POST requests with remote services that support
     * JSONP, and get a JSON response.
     *
     * Copyright (c) 2011 Abdulrahman Al-Otaiba
     * Dual-licensed under MIT and GPLv3.
     */
    var uuid;

    var JSONP = (function (global, win) {
        "use strict";
        /*jslint bitwise: true*/
        var self,
            iterator,
            garbageCollectGet,
            parametersToString,
            generateRandomName,
            callError,
            callSuccessGet,
            callSuccessPost,
            callComplete;

        iterator = 0;


        garbageCollectGet = function (callbackName, script) {
            script.parentNode.removeChild(script);
            global[callbackName] = undefined;
            try {
                delete global[callbackName];
            } catch (e) {
            }
        };

        parametersToString = function (parameters, encodeURI) {
            var str = "",
                key,
                parameter;

            for (key in parameters) {
                if (parameters.hasOwnProperty(key)) {
                    key = encodeURI ? encodeURIComponent(key) : key;
                    parameter = encodeURI ? encodeURIComponent(parameters[key]) : parameters[key];
                    str += key + "=" + parameter + "&";
                }
            }
            return str.replace(/&$/, "");
        };

        //Thanks to Kevin Hakanson
        //http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/873856#873856
        generateRandomName = function () {
            iterator++;

            uuid = 'flyjsonp_' + iterator + CONFIG.salt;
            return uuid;
        };

        callError = function (callback, errorMsg) {
            if (typeof (callback) !== 'undefined') {
                callback(errorMsg);
            }
        };

        callSuccessGet = function (callback, data) {
            if (typeof (callback) !== 'undefined') {
                callback(data);
            }
        };

        callSuccessPost = function (callback, data) {
            if (typeof (callback) !== 'undefined') {
                callback(data);
            }
        };

        callComplete = function (callback) {
            if (typeof (callback) !== 'undefined') {
                callback();
            }
        };

        self = {};

        //settings
        self.options = {};

        self.init = function (options) {
            var key;

            for (key in options) {
                if (options.hasOwnProperty(key)) {
                    self.options[key] = options[key];
                }
            }

            return true;
        };

        self.get = function (options) {
            options = options || {};
            var url = options.url,
                callbackParameter = options.callbackParameter || 'callback',
                parameters = options.parameters || {},
                script = win.document.createElement('script'),
                callbackName,
                prefix = "?";

            if (options.params && options.params.callback) {
                callbackName = options.params.callback;
            } else {
                callbackName = generateRandomName();
            }

            if (!url) {
                throw new Error('URL must be specified!');
            }

            parameters[callbackParameter] = callbackName;
            if (url.indexOf("?") >= 0) {
                prefix = "&";
            }
            url += prefix + parametersToString(parameters, true);

            global[callbackName] = function (data) {
                if (typeof (data) === 'undefined') {
                    callError(options.error, 'Invalid JSON data returned');
                } else {
                    if (options.httpMethod === 'post') {
                        data = data.query.results;
                        if (!data || !data.postresult) {
                            callError(options.error, 'Invalid JSON data returned');
                        } else {
                            if (data.postresult.json) {
                                data = data.postresult.json;
                            } else {
                                data = data.postresult;
                            }
                            callSuccessPost(options.success, data);
                        }
                    } else {
                        callSuccessGet(options.success, data);
                    }
                }
                garbageCollectGet(callbackName, script);
                callComplete(options.complete);
            };

            script.setAttribute('src', url);
            win.document.getElementsByTagName('head')[0].appendChild(script);

            Event.add(script, 'error', function () {
                garbageCollectGet(callbackName, script);
                callComplete(options.complete);
                callError(options.error, 'Error while trying to access the URL');
            });
        };

        return self.get;
    }(window, this));
    /*
     * a backwards compatable implementation of postMessage
     * by Josh Fraser (joshfraser.com)
     * released under the Apache 2.0 license.
     *
     * this code was adapted from Ben Alman's jQuery postMessage code found at:
     * http://benalman.com/projects/jquery-postmessage-plugin/
     *
     * other inspiration was taken from Luke Shepard's code for Facebook Connect:
     * http://github.com/facebook/connect-js/blob/master/src/core/xd.js
     *
     * the goal of this project was to make a backwards compatable version of postMessage
     * without having any dependency on jQuery or the FB Connect libraries
     *
     * my goal was to keep this as terse as possible since my own purpose was to use this 
     * as part of a distributed widget where filesize could be sensative.
     *
     */

    var Bridge = (function () {

        // A few vars used in non-awesome browsers.
        var postMessage,
            receiveMessage,
            interval_id,
            last_hash,
            cache_bust = 1,

        // A var used in awesome browsers.
            rm_callback,

        // A few convenient shortcuts.
            window = ROOT,
            FALSE = !1,

        // Reused internal strings.
            addEventListener = 'addEventListener',

        // I couldn't get window.postMessage to actually work in Opera 9.64!
            has_postMessage = window["postMessage"] && !( /opera/.test(navigator.userAgent) );

        var parseData = function (data) {
            try {
                data = data === "true" ? true :
                        data === "false" ? false :
                        data === "null" ? null :
                    // Only convert to a number if it doesn't change the string
                        +data + "" === data ? +data :
                    rbrace.test(data) ? localJSON.parse(data) :
                        data;
            } catch (e) {
            }
        };

        postMessage = function (message, target_url, target) {
            if (!target_url) {
                return;
            }

            // Serialize the message if not a string. Note that this is the only real
            // jQuery dependency for this script. If removed, this script could be
            // written as very basic JavaScript.
            message = typeof message === 'string' ? message : param(message);

            // Default to parent if unspecified.
            target = target || parent;

            if (has_postMessage) {
                // The browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                target.postMessage(message, target_url.replace(/([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // The browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the
                // callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date) + (cache_bust++) + '&' + message;
            }
        };


        receiveMessage = function (callback, source_origin, delay) {
            if (has_postMessage) {
                // Since the browser supports window.postMessage, the callback will be
                // bound to the actual event associated with window.postMessage.

                if (callback) {
                    // Unbind an existing callback if it exists.
                    rm_callback && receiveMessage();

                    // Bind the callback. A reference to the callback is stored for ease of
                    // unbinding.
                    rm_callback = function (e) {
                        if (( typeof source_origin === 'string' && e.origin !== source_origin ) ||
                            ( isFunction(source_origin) && source_origin(e.origin) === false )) {
                            return false;
                        }
                        callback(e);
                    };
                }

                if (window[addEventListener]) {
                    window[ callback ? addEventListener : 'removeEventListener' ]('message', rm_callback, false);
                } else {
                    window[ callback ? 'attachEvent' : 'detachEvent' ]('onmessage', rm_callback);
                }

            } else {
                // Since the browser sucks, a polling loop will be started, and the
                // callback will be called whenever the location.hash changes.

                interval_id && clearInterval(interval_id);
                interval_id = null;

                if (callback) {
                    delay = typeof source_origin === 'number'
                        ? source_origin
                        : typeof delay === 'number'
                        ? delay
                        : 100;

                    interval_id = setInterval(function () {
                        var hash = document.location.hash,
                            re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({ data: hash.replace(re, '') });
                        }
                    }, delay);
                }
            }
        };

        var parseParam = function (data) {
            var rbrace = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/;

            if (typeof data === "string") {
                try {
                    data = data === "true" ? true :
                            data === "false" ? false :
                            data === "null" ? null :
                        // Only convert to a number if it doesn't change the string
                            +data + "" === data ? +data :
                        rbrace.test(data) ? localJSON.parse(data) :
                            data;
                } catch (e) {
                }

            } else {
                data = undefined;
            }

            return data;
        };

        var parseMessage = function (message) {
            var params_arr = message.split("&"),
                params = {};

            each(params_arr, function (item) {
                var data = item.split("=");

                params[data[0]] = parseParam(decodeURIComponent(data.slice(1).join("=")));
            });

            return params;
        };

        var prepareMessage = function (obj) {
            var str = "";
            each(obj, function (item, name) {
                if (isObject(item) && !item.style) {
                    str += name + "=" + encodeURIComponent(localJSON.stringify(item));
                } else {
                    str += name + "=" + encodeURIComponent(item);
                }
                str = str + "&";
            });

            return str.substr(0, str.length - 1);
        };

        return {
            postMessage: postMessage,
            receiveMessage: receiveMessage,
            parseMessage: parseMessage,
            prepareMessage: prepareMessage
        };
    })();
    extend(Esi.prototype, {
        init: function (params) {
            console.log("init", params)
            var _this = this,
                data = Bridge.parseMessage(document.location.hash);

            //Setting up config
            _this.config = extend({}, CONFIG, {
                CURRENT_HOST: data.CURRENT_HOST,
                INIT_URL: data.INIT_URL
            });

            _this.setBridge();

            //Tell widget that it`s loaded
            _this.sendMessage({
                loaded: true
            });


            /*domReady(function(){
             _this.getContainers();
             _this.setEvents();
             });*/

            _this.collapsed = false;

        },
        receiveData: function (data) {
            var msg = Bridge.parseMessage(data),
                _this = this;

            if (msg["site_information"]) {
                _this.site_information = msg["site_information"];
                domReady(function () {
                    _this.renderInformation();
                });
            }

            if (msg["collapse"]) {
                if (_this.collapsed) {
                    _this.toggleInformation();
                }
            }

            if (msg["widget_params"]) {
                this.mergeConfiguration(msg["widget_params"]);
                domReady(function () {
                    _this.renderFields();
                });
            }
        },

        mergeConfiguration: function (data) {
            var _this = this;

            each(data, function (value, name) {
                if (indexOf(_this.config.allowedExtend, name) >= 0) {
                    _this.config[name] = value;
                }
            });

            _this.config.changedFields = data.changedFields;

            if (_this.config.debug) {
                if (data.HOST) {
                    _this.config.HOST = data.HOST;
                }
            }
        },

        sendMessage: function (data) {
            Bridge.postMessage(Bridge.prepareMessage(data), this.config.INIT_URL, parent);
        },

        setBridge: function (url) {
            var _this = this;

            Bridge.receiveMessage(function (e) {
                _this.receiveData(e.data);
            }, _this.config.CURRENT_HOST);
        },

        redrawHeight: function (extra) {
            if (this._body) {
                this.sendMessage(extend({
                    "height": this._body.offsetHeight
                }, extra || {}));
            }
        },

        toggleInformation: function () {

            if (this.collapsed) {
                Dom.removeClass(this.about_button, "active");
            } else {
                Dom.addClass(this.about_button, "active");
            }

            Dom.setStyle(this.about_container, {
                "display": this.collapsed ? "none" : "block"
            });
            this.collapsed = !this.collapsed;

            this.redrawHeight({
                collapsed: this.collapsed
            });
        },

        getThisSiteCheckbox: function () {
            if (!this.on_this_site) {
                this.on_this_site = Dom.getById("on_this_site");
            }
        },

        //get body and html Containers
        getContainers: function () {
            if (!this._html) {
                this._html = document.documentElement;
            }
            if (!this._body) {
                this._body = ROOT.document.body;
            }

            this.about_container = Dom.getById('eSiAboutT');
            this.main_container = Dom.getById("eSi");
            this.about_button = Dom.getById("eSiAboutButton");
            this.getThisSiteCheckbox();
        },

        //setting events function
        setEvents: function () {
            var _this = this;

            Event.add(Dom.getById("eSiAboutButton"), "click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                _this.toggleInformation();
            });

            Event.add(Dom.getById("eSi"), "click", function () {
                if (_this.collapsed) {
                    _this.toggleInformation();
                }
            });

            Event.add(Dom.getById("headLink"), "click", function (e) {
                e.preventDefault();
                var link = this.getAttribute("href");

                _this.sendMessage({
                    "redirect": link
                });
            });

            Event.add(Dom.getById("eSiAboutPage"), "click", function (e) {
                e.stopPropagation();
            });

            Event.add(Dom.getById("findForm"), "submit", function (e) {
                e.preventDefault();
                _this.searchHandler();
            });
        },


        searchHandler: function () {
            this.getThisSiteCheckbox();

            var _this = this,
                config = _this.config,
                formActionChanged = (config.changedFields.formAction && true),
                searchString = Dom.getById("inputText").value,
                searchForCurrentSite = this.on_this_site.checked,
                link, params = {};


            // if checkbox checked on this site
            if (searchForCurrentSite) {

                // trying to get custom formAction
                if (formActionChanged) {
                    link = config.formAction;
                    params[config.searchField] = searchString;

                    // if no custom action passed going to esir
                } else {
                    link = normalizeUrl(config.HOST + CONFIG.formAction);
                    params["site"] = cleanHost(config.CURRENT_HOST.replace(/^https?:\/\//i, ''));
                    params[CONFIG.searchField] = searchString;
                }

                //default behavior
            } else {
                link = normalizeUrl(config.HOST + CONFIG.formAction);
                params[CONFIG.searchField] = searchString;
            }

            _this.sendMessage({
                "redirect": link + ((link.indexOf("?") >= 0) ? "&" : "?") + param(params)
            });
        },

        renderFields: function () {
            this.getThisSiteCheckbox();

            if (this.config.onSiteCheckbox === true) {
                this.on_this_site.setAttribute("checked", "checked");
            }
        },

        renderInformation: function () {
            var template = Template.render("templates/about.dot", this.site_information),
                els = Dom.create(template),
                container = Dom.getById('eSiAboutPage');

            Dom.clear(container);
            Dom.appendTo(els, container);

            this.redrawHeight();
        }
    });

    new Esi(rootObj);
    var encodeHTMLSource = function (code) {
        var encodeHTMLRules = {
                "&": "&#38;",
                "<": "&#60;",
                ">": "&#62;",
                '"': '&#34;',
                "'": '&#39;',
                "/": '&#47;'
            },
            matchHTML = /&(?!#?\w+;)|<|>|"|'|\//g;

        return code ? code.toString().replace(matchHTML, function (m) {
            return encodeHTMLRules[m] || m;
        }) : code;

    };


    var Template = (function () {
        var tpls = {},
            salt = CONFIG.salt,
            host = CONFIG.HOST,
            path = ROOT.location.href;

        return {
            add: function (name, fn) {
                if (typeof fn === "function") {
                    tpls[name] = fn;
                }
            },
            render: function (name, context) {
                var params = {};

                if (!name) {
                    return false;
                }

                if (!context) {
                    context = {};
                }

                extend(params, context, {
                    _s: salt,
                    _h: host,
                    _p: path
                });


                if (tpls[name]) {

                    try {
                        return tpls[name](params);
                    } catch (e) {
                        throw e;
                    }
                }
            }
        };
    }());
    /*
     Typecast 1.4 (release)
     by Ara Pehlivanian (http://arapehlivanian.com)

     This work is licensed under a Creative Commons Licence
     http://creativecommons.org/licenses/by-nd/2.5/
     */

    var Typecast = {
        InitMask: false,
        InitSuggest: false,

        Init: function () {
            this.Parse(document.body.getElementsByTagName("input"));
            this.Behaviours.Mask.Init();
            this.Behaviours.Suggest.Init();
        },

        Parse: function (nodes) {

            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];

                if (node.type == "text" && node.className && node.className.indexOf("TC") != -1) {
                    if (!node.id) Typecast.Utils.GenerateID(node);

                    var behaviourName = (node.className.indexOf("[") != -1) ? node.className.substring(node.className.indexOf("TC") + 2, node.className.indexOf("[")) : node.className.substring(node.className.indexOf("TC") + 2, node.className.length);

                    Typecast["Init" + behaviourName] = true;
                    Typecast.Behaviours[behaviourName].InitField(node);

                    node.onfocus = Typecast.Behaviours[behaviourName].Run;
                    node.onkeyup = Typecast.Behaviours[behaviourName].KeyHandler;
                    node.onkeydown = Typecast.Behaviours[behaviourName].KeyHandler;
                    node.onblur = Typecast.Behaviours[behaviourName].Stop;
                    node.onmouseup = Typecast.Behaviours[behaviourName].MouseUp;
                }
            }
        },

        Behaviours: {
            Mask: {
                Init: function () {
                },

                InitField: function (field) {
                    var fieldData = [];
                    if (!eval("Typecast.Config.Data.Mask.Masks." + field.id)) {
                        fieldData = field.className.substring(field.className.indexOf("[") + 1, field.className.indexOf("]"))
                    } else {
                        fieldData = eval("Typecast.Config.Data.Mask.Masks." + field.id);
                    }
                    Typecast.Behaviours.Mask.ParseFieldData(field, fieldData);
                    field.value = field.DefaultText.join("");
                },

                Run: function (e) {
                    e = (!e) ? window.event : e;
                },

                Stop: function () {
                },

                KeyHandler: function (e) {
                    e = (!e) ? window.event : e;
                    var mask = Typecast.Behaviours.Mask;

                    mask.CursorManager.TabbedInSetPosition(this);

                    //Backspace
                    if (e.keyCode == 8 && e.type == "keydown" && this.AllowInsert) {
                        var preBackspaceCursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(this)[0];
                        mask.CursorManager.Move(this, -1);
                        var postBackspaceCursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(this)[0];

                        if (preBackspaceCursorPosition != postBackspaceCursorPosition) mask.DataManager.RemoveCharacterByShiftLeft(this);
                        mask.Render(this);
                    }

                    //Tab
                    if (e.keyCode == 9 && e.type == "keydown") {
                        return
                    }

                    //Enter
                    else if (e.keyCode == 13 && e.type == "keyup") {
                        return
                    }

                    //Esc
                    else if (e.keyCode == 27 && e.type == "keyup") {
                    }

                    //End
                    else if (e.keyCode == 35 && e.type == "keydown") {
                        var startIdx = Typecast.Behaviours.Mask.MaskManager.FindNearestMaskCharacter(this, this.DataIndex[this.DataIndex.length - 1], 1);
                        Typecast.Behaviours.Mask.CursorManager.SetPosition(this, startIdx);
                    }

                    //Home
                    else if (e.keyCode == 36 && e.type == "keydown") {
                        Typecast.Behaviours.Mask.CursorManager.SetPosition(this, this.MaskIndex[0]);
                    }

                    //Left or Up
                    else if (e.keyCode == 37 && e.type == "keydown" || e.keyCode == 38 && e.type == "keydown") {
                        mask.CursorManager.Move(this, -1);
                    }

                    //Right or Down
                    else if (e.keyCode == 39 && e.type == "keydown" || e.keyCode == 40 && e.type == "keydown") {
                        mask.CursorManager.Move(this, 1);
                    }

                    //Insert
                    else if (e.keyCode == 45 && e.type == "keydown" && this.AllowInsert) {
                        mask.CursorManager.ToggleInsert(this);
                    }

                    //Delete
                    else if (e.keyCode == 46 && e.type == "keydown") {
                        if (this.InsertActive) {
                            mask.DataManager.RemoveCharacterByShiftLeft(this);
                        } else {
                            mask.DataManager.RemoveCharacterByOverwrite(this);
                        }
                        mask.Render(this);
                    }

                    //Numeric Characters
                    else if ((mask.MaskManager.CurrentMaskCharacter(this) == Typecast.Config.Settings.Mask.MaskCharacters.Numeric) && (e.keyCode >= 48 && e.keyCode <= 57 && e.type == "keydown" || e.keyCode >= 96 && e.keyCode <= 105 && e.type == "keydown")) {
                        var keycode = parseInt(e.keyCode);
                        keycode = (keycode >= 96 && keycode <= 105) ? keycode - 48 : keycode;

                        mask.DataManager.AddData(this, String.fromCharCode(keycode));
                        mask.Render(this);
                        mask.CursorManager.Move(this, 1);
                    }

                    //Alpha Characters 65 - 90
                    else if ((mask.MaskManager.CurrentMaskCharacter(this) == Typecast.Config.Settings.Mask.MaskCharacters.Alpha) && (e.keyCode >= 65 && e.keyCode <= 90 && e.type == "keydown")) {
                        mask.DataManager.AddData(this, String.fromCharCode(e.keyCode));
                        mask.Render(this);
                        mask.CursorManager.Move(this, 1);
                    }

                    //Refresh
                    else if (e.keyCode == 116 && e.type == "keydown") {
                        return
                    }

                    else {
                    }
                    return false
                },

                MouseUp: function (e) {
                    e = (!e) ? window.event : e;
                    var cursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(this)[0];
                    var startIdx = Typecast.Behaviours.Mask.MaskManager.FindNearestMaskCharacter(this, cursorPosition, 0);
                    Typecast.Behaviours.Mask.CursorManager.SetPosition(this, startIdx);
                },

                ParseFieldData: function (field, fieldData) {
                    fieldData = fieldData.split(Typecast.Config.Settings.Mask.FieldDataSeparator);
                    field.Data = [];
                    field.DataIndex = [];
                    field.DefaultText = (fieldData[1]) ? fieldData[1].split("") : fieldData[0].split(""); //if default text isn't provided use mask
                    field.Mask = this.MaskManager.ParseMask(fieldData[0]);
                    field.MaskIndex = this.MaskManager.ParseMaskIndex(field.Mask);
                    field.CursorPersistance = [];
                    field.InsertActive = (this.MaskManager.IsComplexMask(field)) ? false : true;
                    field.HighlightChar = (this.MaskManager.IsComplexMask(field)) ? true : false;
                    field.AllowInsert = (this.MaskManager.IsComplexMask(field)) ? false : true;
                },

                MaskManager: {
                    ParseMask: function (mask) {
                        var arr = [];
                        var maskCharacters = Typecast.Config.Settings.Mask.MaskCharacters;
                        for (var i = 0; i < mask.length; i++) {
                            for (maskCharacter in maskCharacters) {
                                char = eval("maskCharacters." + maskCharacter);
                                if (mask.substring(i, i + 1) == char) {
                                    arr[i] = char;
                                }
                            }
                        }
                        return arr
                    },

                    ParseMaskIndex: function (mask) {
                        var arr = [];
                        for (var i = 0; i < mask.length; i++) {
                            if (mask[i] != null) arr[arr.length] = i;
                        }
                        return arr;
                    },

                    CurrentMaskCharacter: function (field) {
                        var cursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(field)[0];
                        return field.Mask[cursorPosition];
                    },

                    FindNearestMaskCharacter: function (field, cursorPosition, dir) {
                        var nearestMaskCharacter = (field.DataIndex.length > 0) ? cursorPosition : field.MaskIndex[0];

                        switch (dir) {
                            case -1:
                                for (var i = field.DataIndex.length - 1; i > -1; i--) {
                                    if (field.DataIndex[i] < cursorPosition) {
                                        nearestMaskCharacter = field.DataIndex[i];
                                        break;
                                    }
                                }
                                break;
                            case 0:
                                for (var i = 0; i < field.DataIndex.length; i++) {
                                    if (field.MaskIndex[i] >= cursorPosition) {
                                        nearestMaskCharacter = field.MaskIndex[i];
                                        break;
                                    } else {
                                        nearestMaskCharacter = field.MaskIndex[field.DataIndex.length];
                                    }
                                }
                                break;
                            case 1:
                                for (var i = 0; i < field.DataIndex.length; i++) {
                                    if (field.DataIndex[i] > cursorPosition) {
                                        nearestMaskCharacter = field.DataIndex[i];
                                        break;
                                    }
                                }
                                if (cursorPosition == field.MaskIndex[field.MaskIndex.length - 1]) nearestMaskCharacter = cursorPosition + 1;
                                else if (cursorPosition == field.DataIndex[field.DataIndex.length - 1]) nearestMaskCharacter = field.MaskIndex[field.DataIndex.length];
                                break;
                        }
                        return nearestMaskCharacter
                    },

                    IsComplexMask: function (field) {//reports if mask contains mixed mask characters... can't perform "insert" if true
                        var isComplex = false;
                        var previousMaskChar = "";
                        for (var i = 0; i < field.MaskIndex.length; i++) {
                            var currentMaskChar = field.Mask[field.MaskIndex[i]];
                            if (currentMaskChar != previousMaskChar && previousMaskChar != "") {
                                isComplex = true;
                            }
                            previousMaskChar = currentMaskChar;
                        }
                        return isComplex
                    }
                },

                CursorManager: {
                    Move: function (field, dir) {
                        var cursorPosition = this.GetPosition(field)[0];
                        var startIdx = Typecast.Behaviours.Mask.MaskManager.FindNearestMaskCharacter(field, cursorPosition, dir);
                        this.SetPosition(field, startIdx);
                    },
                    GetPosition: function (field) {
                        var arr = [0, 0];
                        if (field.selectionStart && field.selectionEnd) {
                            arr[0] = field.selectionStart;
                            arr[1] = field.selectionEnd;
                        }
                        else if (document.selection) {
                            var range = field.createTextRange();
                            range.setEndPoint("EndToStart", document.selection.createRange());
                            arr[0] = range.text.length;
                            arr[1] = document.selection.createRange().text.length;
                        }
                        return arr
                    },
                    SetPosition: function (field, startIdx) {
                        var endIdx = startIdx + ((field.HighlightChar) ? 1 : 0);
                        Typecast.Utils.PartialSelect(field, startIdx, endIdx);
                    },
                    TabbedInSetPosition: function (field) {
                        var mask = Typecast.Behaviours.Mask;

                        if (mask.MaskManager.CurrentMaskCharacter(field) == undefined) {
                            var startIdx = null;
                            if (field.DataIndex.length > 0 && field.DataIndex.length != field.MaskIndex.length) {
                                startIdx = field.MaskIndex[field.DataIndex.length];
                            }
                            else if (field.DataIndex.length == field.MaskIndex.length) {
                                startIdx = field.DataIndex[field.DataIndex.length - 1] + 1;
                            }
                            else {
                                startIdx = field.MaskIndex[0];
                            }
                            this.SetPosition(field, startIdx);
                        }
                    },
                    PersistPosition: function (field) {
                        field.CursorPersistance = this.GetPosition(field);
                    },
                    RestorePosition: function (field) {
                        this.SetPosition(field, field.CursorPersistance[0]);
                    },
                    ToggleInsert: function (field) {
                        if (field.InsertActive) {
                            field.InsertActive = false;
                            field.HighlightChar = true;
                        } else {
                            field.InsertActive = true;
                            field.HighlightChar = false;
                        }
                        var startIdx = this.GetPosition(field)[0];
                        this.SetPosition(field, startIdx);
                    }
                },

                DataManager: {
                    AddData: function (field, char) {
                        var cursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(field)[0];
                        if (field.InsertActive) {
                            this.InsertCharacter(field, char);
                        } else {
                            this.OverwriteCharacter(field, char, cursorPosition);
                        }
                        this.UpdateDataIndex(field);
                    },
                    InsertCharacter: function (field, char) {
                        var lastCharacterPosition = field.MaskIndex[field.MaskIndex.length - 1];
                        var currentCharacterPosition = this.CurrentDataIndexPosition(field);
                        for (var i = lastCharacterPosition; i >= currentCharacterPosition; i--) {
                            field.Data[field.MaskIndex[i + 1]] = field.Data[field.MaskIndex[i]];
                        }
                        field.Data[field.MaskIndex[currentCharacterPosition]] = char;
                    },
                    OverwriteCharacter: function (field, char, cursorPosition) {
                        field.Data[cursorPosition] = char;
                    },
                    RemoveCharacterByOverwrite: function (field) {
                        var currentCharacterPosition = this.CurrentDataIndexPosition(field);
                        if (currentCharacterPosition != null) {
                            field.Data[field.DataIndex[currentCharacterPosition]] = "";
                        }
                    },
                    RemoveCharacterByShiftLeft: function (field) {
                        var lastCharacterPosition = field.DataIndex[field.DataIndex.length - 1];
                        var currentCharacterPosition = this.CurrentDataIndexPosition(field);
                        var cursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(field)[0];

                        if (currentCharacterPosition != null && lastCharacterPosition >= cursorPosition) {
                            for (var i = currentCharacterPosition; i <= lastCharacterPosition; i++) {
                                field.Data[field.DataIndex[i]] = field.Data[field.DataIndex[i + 1]];
                            }
                            field.Data.length = field.Data.length - 1;
                            this.UpdateDataIndex(field);
                        }
                    },
                    UpdateDataIndex: function (field) {
                        field.DataIndex.length = 0;
                        for (var i = 0; i < field.Data.length; i++) {
                            if (field.Data[i] != undefined) field.DataIndex[field.DataIndex.length] = i;
                        }
                    },
                    CurrentDataIndexPosition: function (field) {
                        var cursorPosition = Typecast.Behaviours.Mask.CursorManager.GetPosition(field)[0];
                        var currentDataIndexPosition = null;
                        for (var i = 0; i < field.MaskIndex.length; i++) {
                            if (field.MaskIndex[i] == cursorPosition) {
                                currentDataIndexPosition = i;
                                break;
                            }
                        }
                        return currentDataIndexPosition
                    }
                },

                Render: function (field) {
                    this.CursorManager.PersistPosition(field);
                    var composite = [];
                    for (var i = 0; i < field.Mask.length; i++) {
                        composite[i] = field.Mask[i];
                        if (field.DefaultText[i]) composite[i] = field.DefaultText[i];
                        if (field.Data[i]) composite[i] = field.Data[i];
                    }
                    field.value = composite.join("")

                    this.CursorManager.RestorePosition(field);
                }
            },

            Suggest: {
                FieldID: null,
                OutputAreaVisible: false,
                InputValueBackup: null,
                ResultSet: [],
                ResultSetIndex: -1,

                Init: function () {
                    this.CreateOutputArea();
                },

                InitField: function (field) {
                },

                Run: function (e) {
                    e = (!e) ? window.event : e;
                    var suggest = Typecast.Behaviours.Suggest;
                    this.autocomplete = (Typecast.Config.Settings.Suggest.BrowserAutoComplete) ? "on" : "off"; //Reference: http://web.archive.org/web/20031203134351/http://devedge.netscape.com/viewsource/2003/form-autocompletion/
                    suggest.FieldID = this.id;
                },

                Stop: function (e) {
                    e = (!e) ? window.event : e;
                    var suggest = Typecast.Behaviours.Suggest;

                    suggest.HideOutputArea();
                    suggest.InputValueBackup = null;
                    suggest.ResultSet.length = 0;
                    suggest.ClearOutputAreaContents();
                },

                KeyHandler: function (e) {
                    e = (!e) ? window.event : e;
                    var suggest = Typecast.Behaviours.Suggest;

                    //Tab
                    if (e.keyCode == 9 && e.type == "keyup") {
                        return
                    }

                    //Enter
                    else if (e.keyCode == 13 && e.type == "keyup") {
                        suggest.InputValueBackup = this.value;
                        suggest.HideOutputArea();
                        suggest.PartialSelect(this, this.value.length);
                        return
                    }

                    //Shift
                    else if (e.keyCode == 16 && e.type == "keyup") {
                        return false
                    }

                    //Esc
                    else if (e.keyCode == 27 && e.type == "keyup") {
                        suggest.RevertInputValue(this);
                        suggest.HideOutputArea();
                        return
                    }

                    //Up
                    else if (e.keyCode == 38 && e.type == "keydown") {
                        if (suggest.ResultSet.length == 0 && this.value.length > 0) {
                            suggest.LookUp(this);
                            suggest.Render(this);
                            suggest.InputValueBackup = this.value;
                        }
                        suggest.MoveResultSetIndex(-1);
                        suggest.SuggestInputValue(this);
                        //return false
                    }

                    //Down
                    else if (e.keyCode == 40 && e.type == "keydown") {
                        if (suggest.ResultSet.length == 0 && this.value.length > 0) {
                            suggest.LookUp(this);
                            suggest.Render(this);
                            suggest.InputValueBackup = this.value;
                        }
                        suggest.MoveResultSetIndex(1);
                        suggest.SuggestInputValue(this);
                        //return false
                    }

                    //Everything else
                    else if (e.type == "keyup" && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) {
                        suggest.LookUp(this);
                        suggest.Render(this);
                        suggest.InputValueBackup = this.value;
                        if (e.keyCode != 8) {
                            suggest.AutoComplete(this);
                        }
                    }
                },

                MouseUp: function (e) {
                    e = (!e) ? window.event : e;
                    return
                },

                LookUp: function (field) {
                    field = (!field) ? document.getElementById(this.FieldID) : field;
                    var dictionaries = Typecast.Config.Data.Suggest.Dictionaries;
                    var dictionary = (eval("dictionaries." + field.id)) ? eval("dictionaries." + field.id) : dictionaries.Default;
                    dictionary.sort();
                    var query = Typecast.Utils.CaseSensitize(field.value);

                    this.ResultSet.length = 0;

                    for (var i = 0; i < dictionary.length; i++) {
                        if (Typecast.Utils.FindIn(dictionary[i], query) > -1) {
                            this.ResultSet[this.ResultSet.length] = dictionary[i];
                        }
                    }
                },

                Render: function (field) {
                    field = (!field) ? document.getElementById(this.FieldID) : field;
                    if (this.ResultSet.length == 0) {
                        this.HideOutputArea();
                        return
                    }

                    var outputArea = document.getElementById(Typecast.Config.Settings.Suggest.OutputAreaID);
                    this.ClearOutputAreaContents();

                    var ul = document.createElement("ul");
                    ul.id = "Suggestions";

                    for (var i = 0; i < this.ResultSet.length && i < Typecast.Config.Settings.Suggest.ResultLimit; i++) {
                        var li = document.createElement("li");
                        li.id = i;
                        li.onmouseover = this.MouseOver;
                        li.onmouseout = this.MouseOut;

                        var result = this.BuildHighlightedResult(this.ResultSet[i], field.value);

                        li.appendChild(result);
                        ul.appendChild(li);
                    }

                    outputArea.appendChild(ul);
                    this.ShowOutputArea(field);
                },

                SuggestInputValue: function (field) {
                    if (this.ResultSetIndex == -1) return
                    field = (!field) ? document.getElementById(this.FieldID) : field;

                    if (this.ResultSetIndex >= 0) { //arrow
                        field.value = this.ResultSet[this.ResultSetIndex];
                    } else { //typing
                        field.value += this.ResultSet[this.ResultSetIndex].substring(field.value.length, this.ResultSet[this.ResultSetIndex].length);
                    }
                },

                AutoComplete: function (field) {
                    if (!Typecast.Config.Settings.Suggest.MatchFromStart) return;
                    field = (!field) ? document.getElementById(this.FieldID) : field;
                    var startIdx = field.value.length;

                    if (this.ResultSet.length > 0) {
                        field.value += this.ResultSet[0].substring(field.value.length, this.ResultSet[0].length);
                        this.PartialSelect(field, startIdx);
                    }
                },

                PartialSelect: function (field, startIdx) {
                    if (!Typecast.Config.Settings.Suggest.MatchFromStart) return;
                    field = (!field) ? document.getElementById(this.FieldID) : field;

                    Typecast.Utils.PartialSelect(field, startIdx, field.value.length);
                },

                RevertInputValue: function (field) {
                    field = (!field) ? document.getElementById(this.FieldID) : field;
                    field.value = (field.value == this.InputValueBackup) ? "" : this.InputValueBackup;
                },

                MoveResultSetIndex: function (dir) {
                    if (!this.OutputAreaVisible) return;
                    if (this.ResultSet.length == 0) return;
                    if (this.ResultSetIndex == -1 && dir == -1) return //Lower Bound Test
                    if (this.ResultSetIndex == this.ResultSet.length - 1 && dir == 1) return //Upper Bound Test
                    if (this.ResultSetIndex == Typecast.Config.Settings.Suggest.ResultLimit - 1 && dir == 1) return //User Defined Upper Bound Test

                    var outputArea = document.getElementById(Typecast.Config.Settings.Suggest.OutputAreaID);

                    if (outputArea && this.ResultSetIndex != -1) outputArea.childNodes[0].childNodes[this.ResultSetIndex].className = "";

                    this.ResultSetIndex += dir;

                    if (outputArea && this.ResultSetIndex == -1) this.RevertInputValue();
                    if (outputArea && this.ResultSetIndex != -1) outputArea.childNodes[0].childNodes[this.ResultSetIndex].className = "selected";
                },

                MouseOver: function (e) {
                    e = (!e) ? window.event : e;
                    this.className = "selected";
                    Typecast.Behaviours.Suggest.ResultSetIndex = parseInt(this.id);
                    Typecast.Behaviours.Suggest.SuggestInputValue();
                },

                MouseOut: function (e) {
                    e = (!e) ? window.event : e;
                    this.className = "";
                },

                CreateOutputArea: function () {
                    var sid = Typecast.Config.Settings.Suggest.OutputAreaID;
                    var outputAreaExists = (document.getElementById(sid)) ? true : false;

                    if (!outputAreaExists) {
                        var outputArea = document.createElement("div");
                        outputArea.id = sid;
                    } else {
                        var outputArea = document.getElementById(sid);
                    }
                    outputArea.style.display = "none";
                    outputArea.style.position = "absolute";
                    this.OutputAreaVisible = false;
                    document.body.appendChild(outputArea);
                },

                ShowOutputArea: function (field) {
                    field = (!field) ? document.getElementById(this.FieldID) : field;
                    if (this.OutputAreaVisible) return
                    if (field.value == "") return

                    var outputArea = document.getElementById(Typecast.Config.Settings.Suggest.OutputAreaID);
                    var xy = Typecast.Utils.GetXY(field);

                    if (outputArea) {
                        if (Typecast.Config.Settings.Suggest.IEForceRelative && document.all) outputArea.parentNode.style.position = "relative";
                        outputArea.style.display = "block";
                        outputArea.style.left = xy[0] + "px";
                        outputArea.style.top = xy[1] + field.offsetHeight + "px";
                        outputArea.style.width = field.offsetWidth + "px";
                    } else {
                        Typecast.Behaviours.Suggest.LookUp(this);
                        Typecast.Behaviours.Suggest.Render(this);
                    }
                    this.OutputAreaVisible = true;
                },

                HideOutputArea: function () {
                    if (!this.OutputAreaVisible) return
                    this.ResultSetIndex = -1;
                    document.getElementById(Typecast.Config.Settings.Suggest.OutputAreaID).style.display = "none";
                    this.OutputAreaVisible = false;
                },

                ClearOutputAreaContents: function () {
                    var sid = Typecast.Config.Settings.Suggest.OutputAreaID;
                    var outputArea = document.getElementById(sid);

                    if (outputArea && outputArea.childNodes.length > 0) outputArea.removeChild(outputArea.childNodes[0]);
                },

                BuildHighlightedResult: function (str, fragment) {
                    var csStr = Typecast.Utils.CaseSensitize(str);
                    var csFragment = Typecast.Utils.CaseSensitize(fragment);
                    var span = document.createElement("span");
                    var lhs = document.createTextNode(str.substring(0, csStr.indexOf(csFragment)));
                    var strong = document.createElement("strong");
                    var highlight = document.createTextNode(str.substring(csStr.indexOf(csFragment), csStr.indexOf(csFragment) + csFragment.length));
                    var rhs = document.createTextNode(str.substring(csStr.indexOf(csFragment) + csFragment.length, csStr.length));

                    strong.appendChild(highlight);
                    span.appendChild(lhs);
                    span.appendChild(strong);
                    span.appendChild(rhs);

                    return span
                }
            }
        },

        Utils: {
            FindClass: function (findClass, parentClass) {
                parentClass = (!parentClass) ? 'Typecast' : parentClass;
                for (var i in eval(parentClass)) {
                    if (i == findClass) {
                        return i;
                    } else {
                        var found = Typecast.Utils.FindClass(findClass, parentClass + "." + i);
                        // if something was found send it back up the tree
                        // if the parentClass contains no dots it's the rootClass and should also be tacked on and returned
                        if (found) return (parentClass.indexOf(".") == -1) ? parentClass + "." + i + "." + found : i + "." + found;
                    }
                }
            },

            GenerateID: function (obj) {
                dt = new Date();
                obj.id = "GenID" + dt.getTime();
                return obj
            },

            FindIn: function (str, fragment) {
                if (!fragment || fragment.length <= 0) return -1
                var csStrIdx = this.CaseSensitize(str).indexOf(fragment);
                var matchFromStart = Typecast.Config.Settings.Suggest.MatchFromStart;

                if (matchFromStart && csStrIdx == 0) {
                    return csStrIdx
                }
                else if (!matchFromStart && csStrIdx != -1) {
                    return csStrIdx
                }
                else {
                    return -1
                }
            },

            CaseSensitize: function (str) {
                var isCaseSensitive = Typecast.Config.Settings.Suggest.isCaseSensitive;
                return (isCaseSensitive) ? str : str.toLowerCase();
            },

            GetXY: function (obj) {
                var x = 0;
                var y = 0;
                while (obj.offsetParent) {
                    x += obj.offsetLeft;
                    y += obj.offsetTop;
                    obj = obj.offsetParent;
                }
                return [x, y]
            },

            PartialSelect: function (field, startIdx, endIdx) {
                if (field.createTextRange) {
                    var fld = field.createTextRange();
                    fld.moveStart("character", startIdx);
                    fld.moveEnd("character", endIdx - field.value.length);
                    fld.select();
                } else if (field.setSelectionRange) {
                    field.setSelectionRange(startIdx, endIdx);
                }
            }
        }
    }


    var encodeHTMLSource = function (code) {
        var encodeHTMLRules = {
                "&": "&#38;",
                "<": "&#60;",
                ">": "&#62;",
                '"': '&#34;',
                "'": '&#39;',
                "/": '&#47;'
            },
            matchHTML = /&(?!#?\w+;)|<|>|"|'|\//g;

        return code ? code.toString().replace(matchHTML, function (m) {
            return encodeHTMLRules[m] || m;
        }) : code;

    };


    var Template = (function () {
        var tpls = {},
            salt = CONFIG.salt,
            host = CONFIG.HOST,
            path = ROOT.location.href;

        return {
            add: function (name, fn) {
                if (typeof fn === "function") {
                    tpls[name] = fn;
                }
            },
            render: function (name, context) {
                var params = {};

                if (!name) {
                    return false;
                }

                if (!context) {
                    context = {};
                }

                extend(params, context, {
                    _s: salt,
                    _h: host,
                    _p: path
                });


                if (tpls[name]) {

                    try {
                        return tpls[name](params);
                    } catch (e) {
                        throw e;
                    }
                }
            }
        };
    }());
//Template: templates/about.dot
    Template.add("templates/about.dot", function (it) {
        var out = '<h2>О сайте</h2><h3>Название сайта:</h3><p>' + (it.site_name) + '</p>';
        if (it.topics) {
            out += '<h3>Категории:</h3><p>';
            each(it.topics, function (data, index) {
                out += '<a href="http://' + (it._h) + (data.url) + '" target="_blank">' + (data.name) + '</a>';
                if (index < it.topics.length - 1) {
                    out += ', ';
                }
                out += ''
            });
            out += '</p>';
        }
        if (it.description) {
            out += '<h3>Описание:</h3><p>' + (it.description) + '</p>';
        }
        if (it.owner && it.owner.name) {
            out += '<h3>Ответственная организация:</h3><p><a href="';
            if (it.owner.url.indexOf('http://') < 0) {
                out += 'http://' + (it._h);
            }
            out += (it.owner.url) + '" id="ownerPassport" target="_blank">' + (it.owner.name) + '</a></p>';
        }
        if (it.parent_organization && it.parent_organization.name) {
            out += ' <h3>Курирующая организация:</h3> <p><a href="';
            if (it.parent_organization.url.indexOf('http://') < 0) {
                out += 'http://' + (it._h);
            }
            out += (it.parent_organization.url) + '" id="parentPassport" target="_blank">' + (it.parent_organization.name) + '</a></p>';
        }
        return out;
    });


    /*
     Typecast 1.4 (release)
     by Ara Pehlivanian (http://arapehlivanian.com)

     This work is licensed under a Creative Commons Licence
     http://creativecommons.org/licenses/by-nd/2.5/
     */

    Typecast.Config = {
        Settings: {
            Mask: {
                FieldDataSeparator: ",",
                MaskCharacters: {
                    Numeric: "#",
                    Alpha: "A"
                },
                AllowInsert: true,
                DisplayMaskCharacters: false //Display mask characters when default text is not present
            },

            Suggest: {
                OutputAreaID: "SuggestOutputArea",
                isCaseSensitive: false,
                MatchFromStart: false,
                ResultLimit: 15,
                BrowserAutoComplete: false,
                IEForceRelative: true
            }
        },

        Data: {
            Mask: {
                Masks: {
                    fax: "(###) ###-####,(   )    -    "
                }
            },
            Suggest: {
                Dictionaries: {
                    Default: [],
                    fname: [],
                    lname: [],
                    country: []
                }
            }
        }
    }
    var __hasProp = {}.hasOwnProperty,
        __extends = function (child, parent) {
            for (var key in parent) {
                if (__hasProp.call(parent, key))
                    child[key] = parent[key];
            }
            function ctor() {
                this.constructor = child;
            }

            ctor.prototype = parent.prototype;
            child.prototype = new ctor();
            child.__super__ = parent.prototype;
            return child;
        };

    var Base = (function (_super) {
        "use strict";

        function Base(options) {
            return this.initialize(options);
        }

        var eventSplitter = /\s+/;
        var eventsApi = function (obj, action, name, rest) {
            if (!name) return true;
            if (typeof name === 'object') {
                for (var key in name) {
                    obj[action].apply(obj, [key, name[key]].concat(rest));
                }
                return false;
            }
            if (eventSplitter.test(name)) {
                var names = name.split(eventSplitter);
                for (var i = 0, l = names.length; i < l; i++) {
                    obj[action].apply(obj, [names[i]].concat(rest));
                }
                return false;
            }

            return true;
        };
        var slice = [].slice;
        var triggerEvents = function (events, args) {
            var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
            switch (args.length) {
                case 0:
                    while (++i < l) (ev = events[i]).callback.call(ev.ctx);
                    return;
                case 1:
                    while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1);
                    return;
                case 2:
                    while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2);
                    return;
                case 3:
                    while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3);
                    return;
                default:
                    while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args);
            }
        };

        Base.prototype.trigger = function (name) {
            if (!this._events) return this;
            var args = slice.call(arguments, 1);
            if (!eventsApi(this, 'trigger', name, args)) return this;
            var events = this._events[name];
            var allEvents = this._events.all;
            if (events) triggerEvents(events, args);
            if (allEvents) triggerEvents(allEvents, arguments);
            return this;
        };

        Base.prototype.on = function (name, callback, context) {
            if (!eventsApi(this, 'on', name, [callback, context]) || !callback) return this;
            this._events || (this._events = {});
            var events = this._events[name] || (this._events[name] = []);
            events.push({callback: callback, context: context, ctx: context || this});
            return this;
        };

        Base.prototype.nodeContains = function (node, condition) {
            var a;
            if (node === document) {
                return false;
            }
            for (var _i = 0, _len = condition.length; _i < _len; _i++) {
                if (condition[_i] === node) {
                    a = condition;
                    break;
                }
            }
            if (a) {
                return node;
            } else {
                if (node) {
                    return this.nodeContains(node.parentNode, condition);
                }
            }
        }

        Base.prototype.bind = function (func, context) {
            var nativeBind = Function.prototype.bind,
                slice = Array.prototype.slice;
            if (func.bind === nativeBind && nativeBind) {
                return nativeBind.apply(func, slice.call(arguments, 1));
            }
            var args = slice.call(arguments, 2);
            return function () {
                return func.apply(context, args.concat(slice.call(arguments)));
            };
        };

        Base.prototype.normalizeUrl = function (url) {
            var re = /^https?:\/\//i;
            return re.test(url) ? url : "http://" + url;
        };

        return Base;
    })();


    var Steps = (function (_super) {
        "use strict";

        __extends(Steps, _super);

        function Steps(options) {
            return this.initialize(options);
        }


        Steps.prototype.initialize = function (options) {
            if (typeof options !== "undefined") {
                this.el = options.el;
            }

            this.currentStep = null;
            this.formElCount = 0;
            this.forms = [];

        };

        Steps.prototype.get = function (id) {
            if (typeof id !== "undefined") {
                return this.forms[id];
            } else {
                return this.forms;
            }
        };

        Steps.prototype.add = function (Step, options) {

            if (typeof Step === "function") {
                if (typeof options === "undefined") {
                    options = {};
                }

                options = extend(options, {
                    el: this.el.getElementsByTagName("DD")[this.formElCount],
                    steps: this
                });

                Step = new Step(options);
            }
            ++this.formElCount;

            this.forms.push(Step);

            Step.on("all", function (event, args) {
                var c, method;

                c = event.indexOf("/") + 1;

                if (c !== 0) {
                    method = event.substr(c);

                    if (typeof this[method] !== "undefined") {
                        this[method].apply(this, args);
                    }
                }

            }, this);

            Step.on("changeForm", function (step) {
                this.setCurrentStep(this.getStepNumber(step));
            }, this);

            Step.on("next", function (step) {
                this.setCurrentStep(this.getStepNumber(step) + 1);
            }, this);

            Step.on("prev", function (step) {
                this.setCurrentStep(this.getStepNumber(step) - 1);
            }, this);

            Step.on("updateDom", function (e, step) {
                this.trigger("updateDom", step);
            }, this);

            return Step;
        };

        Steps.prototype.remove = function (step) {
            delete this.forms[step];
        };

        Steps.prototype.prev = function () {
            this.setCurrentStep(this.currentStep - 1);
        };

        Steps.prototype.next = function () {
            this.setCurrentStep(this.currentStep + 1);
        };

        Steps.prototype.setCurrentStep = function (stepId) {
            if (stepId >= 0 && stepId < this.forms.length && this.currentStep != stepId) {
                this.currentStep = stepId;
                if (this.currentStep != null) {
                    for (var _i = this.currentStep + 1, _len = this.forms.length; _i < _len; _i++) {
                        this.forms[_i].disable();
                    }
                }
                this.forms[stepId].enable();
                this.trigger("setStep", this.currentStep, this.forms[stepId]);
            }
        };

        Steps.prototype.getStepNumber = function (form) {
            for (var _i = 0, _len = this.forms.length; _i < _len; _i++) {
                if (this.forms[_i] === form) {
                    return _i;
                }
            }
        };

        Steps.prototype.render = function () {
            this.setCurrentStep(0);
            for (var _i = 0, _len = this.forms.length; _i < _len; _i++) {
                this.forms[_i].render();
            }
        };

        return Steps;
    })(Base);


    var Step = (function (_super) {
        "use strict";

        __extends(Step, _super);

        function Step(options) {
            return this.initialize(options);
        }

        Step.prototype.initialize = function (options) {
            if (typeof options !== "undefined") {
                this.el = options.el;
                this.steps = options.steps;
            }
            this.data = options.data;
            this.lock = true;
            this.skipStep = false;


            this.on("loaded", this.loaded, this);
            this.on("error", this.error, this);

        };

        Step.prototype.serialize = function () {
            this.trigger("changeForm", this);
        };

        Step.prototype.render = function () {
            Event.add(this.el, "click", this.click, this);
            Event.add(this.getHeader(), "click", this.clickRetryHanlder, this);
        };

        Step.prototype.isEnable = function () {
            return this.lock;
        };

        Step.prototype.getStep = function (id) {
            return this.steps.get(id);
        };

        Step.prototype.enable = function () {
            this.el.innerHTML = "";

            //this.validate();
            if (this.lock) {
                Dom.removeClass(this.el, "w-step-disabled");
                this.lock = false;
                this.trigger("enableForm", this);
            }
        };

        Step.prototype.disable = function () {
            Dom.addClass(this.el, "w-step-disabled");
            if (!this.lock) {
                this.lock = true;
                this.trigger("disableForm", this);
            }
        };

        Step.prototype.next = function () {
            this.trigger("next", this);
        };

        Step.prototype.prev = function () {
            this.trigger("prev", this);
        };

        Step.prototype.skip = function () {
            this.skipStep = true;
            Dom.addClass(this.el, "w-step-skip");
            this.next();
        };

        Step.prototype.validate = function () {
            return true;
        };


        Step.prototype.click = function (e) {
            if (e.target.className === "next") {
                if (this.validate()) {
                    this.next();
                }
            }
            e.preventDefault();
        };

        Step.prototype.clickRetryHanlder = function (e) {
            if (e.target.className === "retry") {
                this.getData();
            }
            e.preventDefault();
        };

        Step.prototype.getHeader = function () {
            return this.el.previousSibling;
        };

        Step.prototype.getData = function (url, data) {
            if (url) {
                this._url = url;
            }
            if (data) {
                this._data = data;
            }

            if (this.loading) {
                return false;
            }
            if (!this._data) {
                this._data = {};
            }
            Dom.addClass(this.getHeader(), "loading");
            Dom.removeClass(this.getHeader(), "error");
            this.loading = true;
            /*

             if (this.about_data) {
             if (typeof fn === "function") {
             fn();
             }
             return true;
             }

             this.about_loading = true;*/

            JSONP({
                url: this.normalizeUrl(this._url),
                callbackParameter: "jsoncallback",
                ///*
                params: {
                    //callback: "aboutData"// + new Date().getTime()
                },
                parameters: extend({ token: OPTIONS.TOKEN }, this._data),
                //*/
                success: this.bind(function (data) {
                    this.loading = false;
                    //this.trigger("loaded", data);

                    if (data) {
                        if (data.success) {
                            //console.log(data, data[action]);
                            this.trigger("loaded", data.response);
                        } else {
                            //console.log(data, data.error);
                            this.trigger("error", data.error);
                        }
                    } else {
                        this.trigger("error");
                    }
                }, this),
                complete: this.bind(function (err, data) {
                    this.loading = false;
                    Dom.removeClass(this.getHeader(), "loading");
                }, this),
                error: this.bind(function () {
                    this.loading = false;
                    this.trigger("error");
                }, this)
            });
        };

        Step.prototype.setHeaderData = function (html) {
            var spans = this.getHeader().getElementsByTagName("span");
            for (var _i = 0, _len = spans.length; _i < _len; _i++) {
                if (spans[_i].className === "data") {
                    spans[_i].innerHTML = html;
                    break;
                }
            }
            this.trigger("updateDom", this);
        };

        Step.prototype.error = function (errors) {
            Dom.addClass(this.getHeader(), "error");
            Dom.addClass(this.el, "error");

            if (errors) {
                this.el.innerHTML = errors.ErrorDescription;
            }
            this.trigger("updateDom", this);
        };

        Step.prototype.loaded = function (data) {
            Dom.removeClass(this.getHeader(), "loading");
            Dom.removeClass(this.getHeader(), "error");
            Dom.removeClass(this.el, "error");
            this.trigger("updateDom", this);
        };

        return Step;
    })(Base);
    var stepsItems = [],
        OPTIONS = window.zdravWidget,
        API_HOSTNAME = "http://gorzdrav.spb.ru",
        FRAME_PATH = "polisframe.html",
        API_HOST = API_HOSTNAME + "/widget_api";


    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);

            if (!this.firstOpen) {
                if (this.data.dist_id && this.data.dist_title) {
                    this.setHeaderData(this.data.dist_title);
                    window.setTimeout(this.bind(function () { // BUG!
                        this.next();
                    }, this), 100);
                } else {
                    this.getData(API_HOST + "/dist_list");
                }

                this.firstOpen = true;
            } else {
                this.getData(API_HOST + "/dist_list");
            }
            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            if (data) {
                var inner = "<ul>";
//      for (var _i = 0, _len = data.length; _i < _len; _i++)
                {
//        inner += "<li><a href=\"#\" data-dist_id=\"" + data[_i].IdDistrict + "\">" + data[_i].DistrictName + "</a></li>";
                    inner += "<li><a href=\"#\" data-dist_id=\"108\">Красносельский</a></li>";
                    inner += "<li><a href=\"#\" data-dist_id=\"115\">Приморский</a></li>";
                }

                this.el.innerHTML = inner + "</ul>";
            } else {
                this.el.innerHTML = "Нет доступных районов";
            }
            this.trigger("updateDom", this);
        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;
                this.data.dist_title = target.innerHTML;
                this.data.dist_id = target.getAttribute("data-dist_id");
                this.setHeaderData("Район: " + target.innerHTML);
                this.next();
            }
            //var dt = this.nodeContains(e.target, this.el.getElementsByTagName("dt"))
        };

        return S;
    })(Step));


    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);

            if (!this.firstOpen) {
                if (this.data.lpu_id && this.data.lpu_title) {
                    this.setHeaderData(this.data.lpu_title);
                    window.setTimeout(this.bind(function () { // BUG!
                        this.next();
                    }, this), 100);
                } else {
                    this.getData(API_HOST + "/lpu_list", {
                        dist_id: this.data.dist_id
                    });
                }

                this.firstOpen = true;
            } else {
                this.getData(API_HOST + "/lpu_list", {
                    dist_id: this.data.dist_id
                });
            }

            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            if (data) {
                var inner = "<ul>";
                for (var _i = 0, _len = data.length; _i < _len; _i++) {
                    inner += "<li><a href=\"#\" data-lpu_id=\"" + data[_i].IdLPU + "\">";
                    if (data[_i].LpuName) {
                        inner += data[_i].LpuName;
                    } else {
                        inner += data[_i].LPUFullName;
                    }
                    inner += "</a></li>";
                }

                this.el.innerHTML = inner + "</ul>";
            } else {
                this.el.innerHTML = "Нет доступных учереждений";
            }
            this.trigger("updateDom", this);
        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;
                this.data.lpu_title = target.innerHTML;
                this.data.lpu_id = target.getAttribute("data-lpu_id");
                this.setHeaderData(target.innerHTML);
                this.next();
            }
        };

        return S;
    })(Step));


    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.render = function () {
            if (!this.data.patient_name) {
                this.data.patient_name = "";
            }
            if (!this.data.patient_surname) {
                this.data.patient_surname = "";
            }
            if (!this.data.patient_family_name) {
                this.data.patient_family_name = "";
            }
            if (!this.data.patient_date_of_birth) {
                this.data.patient_date_of_birth = "";
            }
            this.el.innerHTML = ""
                + "<label for=\"surname\">Фамилия:</label>"
                + "<input type=\"text\" name=\"surname\" id=\"surname\" value=\"" + this.data.patient_surname + "\"/>"
                + "<span class=\"error\">Вы не ввели фамилию</span>"
                + "<label for=\"name\">Имя:</label>"
                + "<input type=\"text\" name=\"name\" id=\"name\" value=\"" + this.data.patient_name + "\" />"
                + "<span class=\"error\">Вы не ввели имя</span>"
                + "<label for=\"family_name\">Отчество:</label>"
                + "<input type=\"text\" name=\"family_name\" id=\"family_name\" value=\"" + this.data.patient_family_name + "\"/>"
                + "<span class=\"error\">Вы не ввели отчество</span>"
                + "<label for=\"date_of_birth\">Дата рождения:</label>"
                + "<input type=\"text\" name=\"date_of_birth\" id=\"date_of_birth\" class=\"TCMask[##.##.####,__.__.____]\" value=\"" + this.data.patient_date_of_birth + "\" />"
                + "<span class=\"error\">Вы не ввели дату рождения</span>"
                + "<span class=\"serverError\"></span>"
                + "<input type=\"button\" value=\"Далее\" />";

            Typecast.Init();
            _super.prototype.render.apply(this, arguments);
            this.trigger("updateDom", this);
        };


        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);
            this.render();
            Event.add(this.el, "click", this.buttonHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            this.data.patient_id = data.patiend_id; // not patient!
            this.data.patient_surname = this.params.surname;
            this.data.patient_name = this.params.name;
            this.data.patient_family_name = this.params.family_name;
            this.data.patient_date_of_birth = this.params.date_of_birth;

            var a = this.data.patient_date_of_birth;
            var date = a.substr(0, a.indexOf("T00:00:00.000Z")).split("-")
            var birthDate = date[2] + "." + date[1] + "." + date[0];

            this.setHeaderData("Пациент: "
                + this.data.patient_surname
                + " "
                + this.data.patient_name
                + " "
                + this.data.patient_family_name
                + ", "
                + birthDate);
            var _this = this;

            this.trigger("updateDom", this);
            // To fix bug with two jsonp requests
            window.setTimeout(function () {
                _this.next();
            }, 100);
        };

        S.prototype.error = function (error) {
            //_super.prototype.render.apply(this, arguments);
            Dom.addClass(this.getHeader(), "error");
            Dom.addClass(this.el, "error");

            var elems = this.el.getElementsByTagName("span");

            for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                var elem = elems[_i];
                if (elem.getAttribute("class") === "serverError") {
                    if (error) {
                        if (error.IdError == 20) {
                            elem.innerHTML = "К сожалению, мы не смогли найти информацию о таком пациенте в базе данных Территориального фонда ОМС Санкт-Петербурга. Пожалуйста, позвоните нам по телефону 670-00-03, чтобы записаться на прием. Оператор примет Ваш звонок ежедневно с 8.00 до 20.00. Если у Вас нет полиса ОМС или Вы хотели бы получить полис ОМС нового образца, Вы можете сделать это в любом из наших центров без предварительной записи."
                        }
                        else {
                            elem.innerHTML = error.ErrorDescription;
                        }
                    }
                    else {
                        elem.innerHTML = "Произошла ошибка данные не загрузились";
                    }
                    elem.style.display = "block";
                }
            }
            this.trigger("updateDom", this);
        };


        S.prototype.buttonHanlder = function (e) {
            e.preventDefault();
            var target = e.target;
            if (target.getAttribute("type") === "button" || target.tagName === "A") {
                //this.data.clinic_id = target.getAttribute("data-clinic_id");
                //this.setHeaderData(target.innerHTML);
                //this.next();

                var elems = this.el.getElementsByTagName("input");

                for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                    var elem = elems[_i].nextSibling;
                    if (elem && elem.style) {
                        elem.style.display = "none";
                    }
                }

                this.params = this.validate();


                if (this.params) {
                    this.getData(API_HOST + "/check_patient", extend(this.params, {
                        lpu_id: this.data.lpu_id
                    }));
                }
            }
            e.stopPropagation();
            e.preventDefault();
        };

        S.prototype.validate = function () {
            var elems = this.el.getElementsByTagName("input"),
                error = false,
                errorSpan,
                ret = {};
            for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                errorSpan = elems[_i].nextSibling;
                if (errorSpan && errorSpan.tagName === "SPAN") {
                    errorSpan.style.display = "none";
                    if (elems[_i].value) {
                        ret[elems[_i].getAttribute("name")] = elems[_i].value;
                    } else {
                        error = true;
                        errorSpan.style.display = "block";
                    }
                }
            }

            if (!error) {
                var myDate = ret.date_of_birth;
                myDate = myDate.split(".");
                ret.date_of_birth = myDate[2] + "-" + myDate[1] + "-" + myDate[0] + "T00:00:00.000Z";

                return ret;
            }

            this.trigger("updateDom", this);
            return false;
        };


        return S;
    })(Step));


    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);
            this.getData(API_HOST + "/speciality_list", {
                clinic_id: this.data.lpu_id,
                patient_id: this.data.patient_id
            });

            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            if (data) {
                var inner = "<ul>";
                for (var _i = 0, _len = data.length; _i < _len; _i++) {
                    inner += "<li><a href=\"#\" data-speciality_id=\"" + data[_i].IdSpesiality + "\">" + data[_i].NameSpesiality + "</a></li>";
                }

                this.el.innerHTML = inner + "</ul>";
            } else {
                this.el.innerHTML = "Нет доступных специальностей";
            }
            this.trigger("updateDom", this);
        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;
                this.data.speciality_title = target.innerHTML;
                this.data.speciality_id = target.getAttribute("data-speciality_id");
                this.setHeaderData(target.innerHTML);
                this.next();
            }
        };

        return S;
    })(Step));

    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);
            this.getData(API_HOST + "/doctors_list", {
                clinic_id: this.data.lpu_id,
                patient_id: this.data.patient_id,
                speciality_id: this.data.speciality_id
            });

            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            if (data) {
                var inner = "<ul>";
                for (var _i = 0, _len = data.length; _i < _len; _i++) {
                    inner += "<li><a href=\"#\" data-doctor_id=\"" + data[_i].IdDoc + "\">" + data[_i].Name + "</a></li>";
                }

                this.el.innerHTML = inner + "</ul>";
            } else {
                this.el.innerHTML = "Нет доступных докторов";
            }
            this.trigger("updateDom", this);
        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;
                this.data.doctor_title = target.innerHTML;
                this.data.doctor_id = target.getAttribute("data-doctor_id");
                this.setHeaderData("Врач: " + target.innerHTML);
                this.next();
            }
        };

        return S;
    })(Step));

    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);
            this.getData(API_HOST + "/apps_list", {
                clinic_id: this.data.lpu_id,
                patient_id: this.data.patient_id,
                doctor_id: this.data.doctor_id
            });

            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            this.data.dates = data;
            if (data) {
                var inner = "<ul>";
                var arr = [];
                for (var i in data) {
                    arr.push([i, data[i]]);
                }
                var f = function (strDate) {
                    var dateParts = strDate.split("-");
                    date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
                    return date.getTime();
                };
                arr.sort(function (a, b) {
                    var a = f(a[0]);
                    var b = f(b[0]);
                    return a < b ? -1 : a > b ? 1 : 0;
                })
                for (var i = 0, l = arr.length; i < l; i++) {
                    var visit = arr[i][1][0].date_start;
                    inner += "<li><a href=\"#\" data-dm=\"" + arr[i][0] + "\" data-name=\"\">" + visit.day + " " + visit.month_verbose + "</a></li>";
                }
                this.el.innerHTML = inner + "</ul>";
            } else {
                this.el.innerHTML = "Нет доступных номерков";
            }
            this.trigger("updateDom", this);

        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;
                this.data.dm = target.getAttribute("data-dm");
                this.setHeaderData(target.innerHTML);
                this.next();
            }
        };

        return S;
    })(Step));

    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);
            //this.getData(API_HOST + "/apps_list", "");

            var numbers = this.data.dates[this.data.dm];
            var inner = "<ul>";
            if (numbers) {
                for (var _i = 0, _len = numbers.length; _i < _len; _i++) {
                    inner += "<li><a href=\"#\" data-number=\"" + numbers[_i].id + "\">" + numbers[_i].date_start.time + "</a></li>";
                }
            }

            this.el.innerHTML = inner + "</ul>";

            this.trigger("updateDom", this);
            Event.add(this.el, "click", this.selectHanlder, this);
        };

        S.prototype.loaded = function (data) {
            _super.prototype.loaded.apply(this, arguments);

            this.trigger("updateDom", this);
        };

        S.prototype.selectHanlder = function (e) {
            if (e.target.tagName === "A") {
                e.preventDefault();
                var target = e.target;

                var numbers = this.data.dates[this.data.dm],
                    number;
                for (var _i = 0, _len = numbers.length; _i < _len; _i++) {
                    if (numbers[_i].id === target.getAttribute("data-number")) {
                        this.data.number = numbers[_i];
                        break;
                    }
                }

                this.setHeaderData(target.innerHTML);
                this.next();
            }
        };

        return S;
    })(Step));

    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);

            this.el.innerHTML = ""
                + "<p>"
                + "Прием: "
                + this.data.number.date_start.day
                + " "
                + this.data.number.date_start.month_verbose
                + ", "
                + this.data.number.date_start.day_verbose
                + ", "
                + this.data.number.date_start.time
                + "</p>"
                + "<p>"
                + "Пациент: "
                + this.data.patient_surname
                + " "
                + this.data.patient_name
                + " "
                + this.data.patient_family_name
                + "</p>"
                + "<p>"
                + "Врач: "
                + this.data.doctor_title
                + ", "
                + this.data.speciality_title
                + "</p>"
                + "<p>"
                + this.data.lpu_title
                + ", "
                + this.data.dist_title
                + " район"
                + "</p>"
                + "<input type=\"button\" value=\"Записаться\" /><br/>";

            Event.add(this.el, "click", this.buttonHanlder, this);

            this.trigger("updateDom", this);
        };

        S.prototype.loaded = function (data) {
            var _this = this;
            window.setTimeout(function () {
                _this.next();
            }, 100);
        };

        S.prototype.error = function (error) {
            Dom.addClass(this.getHeader(), "error");
            Dom.addClass(this.el, "error");

            var elems = this.el.getElementsByTagName("span");

            if (error) {
                for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                    var elem = elems[_i];
                    if (elem.getAttribute("class") === "serverError") {
                        elem.innerHTML = error.ErrorDescription;
                        elem.style.display = "block";
                    }
                }
            }
            this.trigger("updateDom", this);
        };

        S.prototype.buttonHanlder = function (e) {
            e.preventDefault();
            var target = e.target;
            if (target.getAttribute("type") === "button" || target.tagName === "A") {
                this.getData(API_HOST + "/signup", {
                    clinic_id: this.data.lpu_id,
                    patient_id: this.data.patient_id,
                    doctor_id: this.data.doctor_id,
                    app_id: this.data.number.id
                });
                this.loaded();

            }
            e.stopPropagation();
        };

        return S;
    })(Step));

    stepsItems.push((function (_super) {
        __extends(S, _super);

        function S() {
            return S.__super__.constructor.apply(this, arguments);
        }

        S.prototype.enable = function () {
            _super.prototype.enable.apply(this, arguments);

            this.el.parentNode.getNodes
            var el = this.el.parentNode;

            this.parent = el.parentNode;
            this.parent.removeChild(el);

            this.parent.innerHTML = ""
                + "<h1>Электронная запись к врачу</h1>"
                + "<dl>"
                + "<dt class=\"new\" >"
                + "<span class=\"index\"><span></span></span>"
                + "<span class=\"data\">Новая запись<br/>"
                + "</dt>"
                + "<dt class=\"active done\" >"
                + "<span class=\"index\"><span>!</span></span>"
                + "<span class=\"title\">Запись успешно завершена!<br/>"
                + "</dt>"
                + "<dd class=\"active\" style=\"display: block\">"
                + "<p>"
                + "Прием: "
                + this.data.number.date_start.day
                + " "
                + this.data.number.date_start.month_verbose
                + ", "
                + this.data.number.date_start.day_verbose
                + ", "
                + this.data.number.date_start.time
                + "</p><p>"
                + "Пациент: "
                + this.data.patient_surname
                + " "
                + this.data.patient_name
                + " "
                + this.data.patient_family_name
                + "</p><p>"
                + "Врач: "
                + this.data.doctor_title
                + ", "
                + this.data.speciality_title
                + "</p><p>"
                + this.data.lpu_title
                + ", "
                + this.data.dist_title
                + " район"
                + "</p>";
            //      + "<input type=\"button\" value=\"Напечатать\" /><br/>";
            +"</dd>"
            + "</dl>";
            this.trigger("updateDom", this);
        };

        return S;
    })(Step));


    var Zdrav = (function (_super) {
        "use strict";

        __extends(Zdrav, _super);

        function Zdrav(name) {
            return this.initialize(arguments);
        }

        Zdrav.prototype.initialize = function (params) {

            this.TIME_HASH = new Date().getTime();

            if (OPTIONS) {
                this.el = OPTIONS.id;
                this.el.style.display = "none";
            }
            var className = ".zdrav_" + this.TIME_HASH;

            this.CONFIG = {
                CSS: ""
                    + className + " {"
                    + "background-color: #D1C5FF;"
                    + "color: #6d66ba;"
                    + "font-family: pt_sans_narrow_boldregular,Tahoma,Verdana,Arial,sans-serif;"
                    + "font-size: 14px; }"
                    + className + " h1 {"
                    + "background-image: url(data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAtCAYAAAA5reyyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAD1NJREFUeNrsmnt0VNW9xz/7nJkzr0ySSUISSAIogiAIiKi1vgrU+kCvz8q1iu9Ha6vVatXrVStaq9Y+1LbWd63Vtsv6wKu1y6pLLEVF1CvIQxESSHgkJJlJJpnM85xz/8hv0uNhMkSJQNe6e61ZM/ucfc7Z+7t/v+/v+/udUQuuWcqX2TJ9EcpHrWTGvCvwGH1kk6Wg7OG49VjgBfkGUPKx2HXtac+X/QRfSQdtn8zm00WXMf20G8imwsN16yOAqezedpH2ZT/BtnSCkWa2rjqe6MbpGCWdYKvhuHU6/8OrR9i3egENkQt2KXq6FkTbFQ/y+BKkExV8/OoPwVJonsxw3NbqX0SIKSPvQykv23pewquXo2tBAAy9Ek35KAscSCR4KJryo2shNOWX8xVEAocQ9u1H2DcZgDL/DHye6iFOQeHZFQDatoa/tI1ta4+k7ZM5jJr6In3RBtQwcGGpf3/KA4eQyXVQHvgKIWNvMmYnHb2v0RC5kK7kUurLz8G0koSM8egqgEWWlthjKOWhIXIRQWNvNkR/RVngAEp8k/B7RrEhej9dyWU7fL62q8xd03IozaL5vTMwMyH04bFCgt69ae/9Ox9vu5GRpaeC0vHqlTREzqcnvRJN+WmOPUxT570YegVKaVSXHMtelVeglMH6jrtJZhppjS+krmw+icx6TCtJqX/60Na1qwC0bQ1fuI32xsNo+3g2/tI2bHvnH5/KbcXnqaHMfwBePcKWrj8R9k2iOnwC23pepiZ8AqW+adSETwQ0dC1EIvMplcGjGB25CJssXr0CgKzVRVVoFj3pj2jreWHPAhBAaRYKaH7/DHKZ4LBwYWfiDTp6X6O+fD6NnfeQym3BstN0J98j4G2gMjQbw1NNyBhHeeBgelKrUOjEkktpjj6EZaVp63kRgLXbfkRfZgNKGeSs3iEwoIb6snXg9qaokeqpZsa8y6nf/yV6o2NQ6gtJt9OBvxQ6sV/tz2mNP0ci00ipfwrtva8CUFt6Kq3x53ZemnlqyZqdgL5rLbB/20xsYOO7Z6M8FqU1TQQjLQQjLYSqNuIraRfX/uJSZ03rtUT7lpDObR0ADxgW8AAmVt9GyJiAZad2TRR2G74/3EZ86yRWvvTfBCObyGVC/RyUChOpX07tpL/TF2sQEItHakOvoiFyHqDREnsUG5u6snnoWphY39tE+xazT9X1GJ5K1rXfgc8zkpFlp6MpPxuivyaTa2ff6lux7BzrOu6gKjQLv7cey0oR7XuLsH8yrfHnAagqOZpMro2sFUcpvV+i7Q75rmkWyuhj3eJLsU0viAubmRAef5yDzvr2AIg7kjo2Jlmzm9GRC+hKvouuApT5Z9Cd+oCa8FyCxhiCxt70plcztvJyNOXHp1eRzLYwJnIJlp3GsjNoymBc5dUEjNGAosSYiGVnGVHyDbx6hLb4C1SFZpPKbiJjdgx4iLY7ALQlEwmUbSVY0UwwsolgZBOlI1eje1Mse+oBOjccQqBsyw6zlqwZY2v8GdK5NpTy9kfi+NO0dD1BxuwgZEyktWchjZ334PfUoSmDTzt+wrqOn+Lz1FLim0RT5700xx4j5JtAJtdJZ2IRvZk16JqfrBnDo0KMH3Ejpb4ppHJb6UwsIpXdjELfPRZYLO3zlXTQ2z6OxiUXUDFmGUozi8qdEt9E6svPIZNrJ+zbj+qS48iaURoiF2FZaZpjDzG24rvUl5/L5q4nCXjrmVJ7HzkrwZb40+SsHqbWPYxlZ9nU9QQjQnOIBA4FNHJWAqU8BH37kDN7+vto+D31pLKbyZrRPQvAf+XOLWxb+zXa1x1J9bjFJLtHDlrBSWY20pF4g1S2BUMfQXlgJs2xR/BoYWrCJxFPrWBt+wJ8npF0Jt5g/1G/JWN20hx7lGjf4v6c0OrDtJJ0pz6gJ7WKssAMelIrSGTW0ZP6iJBvAh29r2PbWXQtiFevJGvGUMrYDTJmKGFGWfRFRzNq6kvMOOP7pHtGFLLCs4E/fDa5D/WzotXXT/BaCZadw7JTA2Pqyr5FPLWCnvTKYZinZ/dw4JCylrJWWtd8neiGmYSqWrCt7Zxlq/uAaSUGwAPIWb2fAQ9gc/cfhwU8AE359jwXHpicnkEpi5Uv3cLMsksprVlPNlkiGY1JLhN8PZsqPV9p5qR+1JUFpERgT9kObGU/hq0UoA8Wz7eTnjaD6VEdSJtW4q97LIDYGr5wO91bJ/P+n3/D5ONv6+cr08DM+gmWb6a0ds3jZs6PUiaWaZDpK0cpu2oAQFuBZrUawdjhQKOmZ8ilw2RTYRQ2NgojGMPj68XK+bEt7V9cayuUstC8KXLpEJm+CEqxnS7dcwGUgBKqaiIRHcO7f3gUzZsCWyOXCVBSuZHyuhXkMgGyyQhjDnqK2kmvkoyPKlHK7AdAz0aDkc1zGt8+r7Hlg9OpHv8mYw76E6GKjVg5P5o3SXTjTJLdo4g1zyDRORbd6BNNGiAY2UTF6PcJlG2lcq93yKbDZHorhQDtLx1AHTCHA0SvP45tefr1ozLxBVMku0fS3ToRpSwyiUpSPVWU16/AYyRMMxNA6blEqGLznKZ3zl790Qs/xjINYi3TaV19DGO/8nsqx65ky0ez2bjsTNK9I0BZwrPWQJ1FaTk2LJ2PEYxSN+0FRs94hvK6lWRTJaQTVf1ev+Capb8DKoDsDnMwMIAFwHuFapvAfOB4YDzgBZLAEuB54OUhYvZzYC5wFfC3IuMuBU6U0r433Vt5w4HzrlxZve/rizK9VUeFqloObHxr/gfLF96JEYjd6fXHjwZ1cqYv0mKZXoxQlFR3Ld5gFx5fog5bmwf2V4FJsvlNwBKU/ayZCa5JxWsIlG+mfvpCGg54lvL6VZgZL2rBNUs/b1l4HvB0gcrIPUBdketeB84HWoqMqQM2ye8VwLQiY58BTstbqZnzzZ155vderhzz7hMef/KVprfmP/Xh83dhBGM/8/rjV4sMmgystm0Ny/Sie9IA3wHuAoq97bpFaeaCXLqEVLyGYEUzNRNfp3rcP/EAnUDl5wAw6eqfCzw+hOvmiOXOLALiNY7fU4EjgX8MVgr8zAuSnC9rZn2Ea5IXr/rbOenlz92FEYrd5/XHLxfwbCCX15kC3h3A9UOY+y22pZfp3uQPSqoayWVCNC65kE0fnrIdB/YBH7qO7QWMHOTGYwcBb5kAPUY++VYNPCXAuFs5cLHr2I3AN3ZcqDWxTY/auuoYcqmy9Ir/uQ0jFHvQG+i6xLb0wS6bWwC8ZvGULHCIywOuAhbatvYP3ZskPGIdZs7YTkh/Chzm+vyxyNx/7OpHgROAg4GjRE7c5BpzxCCgfBsIuY4dzRDf/fpKt+nb1s7iw+fuwBeMPej1FwVPA37hOvZnme8Fwq/TC4y51yn2NT23HYCFMhPvYO9zgJNcsvNk4K+OY70C8q9d184vELGvGuQ51w+JWGxalWZihGIPa57UJbatFxt9CDDB0W8CzgR6XOOuFq59X7xqmwTSQQFTn+O9yRSgxNF/B1hcJLI6Jc1+rvPniXsDZIB2x7n/BBp2AJ8FylbKukEp66Ii1ex8wJzhOl7My74pvH0wcEyeR4fjpVKVq/9RkbEtQMzRD7uefb1rI+a6NvWHOwaQp4HbXcdzg4yvcfXXfo51W8MFYNrVLyYD/IDPtbC8NZwI7OM49xdxlyWOY5cAZUXu7xHt6WzXAa8MMj5VIIB9sZx9JwBsdPW/VuRN0CwXwFscAN7kkkh5d/qZM0YA3/scc7sO+Klr09zB0tlOLXKv7wBPAo8BD7g3cmcAbALWOPojCwQLgAhwt+vYIvk+GDjIcfxJieQAC0VWOGWEPoR5/UjAK2RZ+Q1+0+XeR0n0dbdpwP3AWZIEXOrWwTtbD7zH1b+M/v/snS5a73LRlRNd4x6W75sHkwkF+pWyiGLtJuBWZ4lwkHHbHHPIt0eBXwKzgcNlw950jblZgtywAfgQ8Jbr2H8Ij70J3AeMdp3/LtAmvDfXleqtKgB0YoiS5r8K6NIduflm17ErZR6LRQOWuQq4vyjEgU6eMAYh6GKgH1MAxMHareISuCwlL3XcrQf4naM/DjijwDx+ANxZ4HrD5b7Kde9DBymMFKKrI1ybOTAJbQcWqXbQ75WM5eYCgSXf/gkcJ/wEUC/CNd9WF6m8uN36KkdgQWjil18wSLYID9/k4vN8Wyf8PQNYXzCNXHDN0plAQB4WkyqIO98dK7LFkPOxwTIq4b5xYv6bBJz/LSAbpgmReyUqbi6y0KlyjSZWsEwyiQhQ7K3YFNGrWXnW8gISxtmmO/L+Dpl3rmgevie+lft3atr/Q7BzTZ/11YtPdfj/LHHTCUC3hOw5wnN9wN4SZZc77nGC1AQnAR8Dpwi/zZBznwCjxAWjUrJqlvtdIblut7h7SKL08RK8GiXDmOioIe4vhddW0WWnSdVmk7idU5deJRovIVF0MvB94EChlpRkSedKOpcFvi61xrRImlZ55mihmcPkd0veAi+TCeerD1mJamGHthvvUOyPO5L7i4FrhWyrpfbXCtQCF8qiUjLBevk+VoC8XZL0tVLYPEju+03huGtFj03IV54ddbxTpRp0HvC2fOIu4zhO+Hi5BKJy2dADZI33S0CcBjwiwSQf0SOOLKRcjGKWKyAOSJS7xcpGyckmYIQspl2S55gj/71VpMsjstgrgQ9ck28Tq81Llm6xliYBo0EsND+RuKj9e2RnW8UKmmVe0wSsNyXgjJNUsEuMIFMAwD6x4B757pIg9wDwouTatlj3HWJlbwgmdWJtlqw5DmwQEJvF0wY48BW58e0OzfWYiNY75WZNUpk+WaLSsQ5gPilADbViITgAOsZRsqp1udtGoQ5bdny8AFAv52NiNdeJFW2RxXllTH2BvDcjc99Lxk6QxV8tG3Wno5TWKcYA8CD9rxbukOv7BMyzZTNvc6xjIIgkxW3apP+E8NOvZHEB4T5NFtkgPLBeFoVkFqMcArbUsZjRkmu2igUvk9RslqPaslpoo0UKCluEj0Ky40+JdRwtgARkvr+RjKWzQMlquRhDWCzeK3x/s1jhWY4quCGU9Kxr7UFx6ZNEq2YdLo5H+GSquJhbZOYrFyPlotNlom3i9j8RV35UJne7wzLXu1w6Qf87jhfFKm4VAXuuWOhvhTpGA78XCrhZrPVw8Y7FwGvyrLhs0uOOEv2HLnH+LZnbaqksXyqFjLy7TxKuXSR1yFNca98gHmeIUUXl2fk3h/zfAOeJXC8Q/ehrAAAAAElFTkSuQmCC);"
                    + "background-repeat: no-repeat;"
                    + "padding: 0 0 0 95px;"
                    + "margin: 0 4px;"
                    + "font-size: 18px;"
                    + "align:bottom;"
                    + "font-family: pt_sans_narrow_boldregular,Tahoma,Verdana,Arial,sans-serif;"
                    + "min-height: 45px;"
                    + "line-height: 18px;"
                    + "background-position: 0 0; }"
                    + className + " a, " + className + " dt {"
                    + "color: #6d66ba;"
                    + "}"
                    + className + " dt {"
                    + "background-color: #dfdfdf;"
                    + "padding: 10px 0;"
                    + "border-top: 1px solid #cbcfd4;"
                    + "border-bottom: 1px solid #cbcfd4;"
                    + "display: none;"
                    + "cursor: pointer;"
                    + "min-height: 25px;"
                    + "margin-top: -1px;"
                    + "position: relative;"
                    + "display: block; }"
                    + className + " dt .title {"
                    + "font-weight: bold; }"
                    + className + " dt span {"
                    + "margin-left: 42px;"
                    + "display: block; }"
                    + className + " dt .index {"
                    + "margin-left: 11px;"
                    + "float: left;"
                    + "width: 19px;"
                    + "height: 17px;"
                    + "border: 1px solid #6d66ba;"
                    + "color: #6d66ba;"
                    + "position: relative;"
                    + "border-radius: 3px;"
                    + "display: inline-block; }"
                    + className + " .index span {"
                    + "margin: 0;"
                    + "position: absolute;"
                    + "right: 7px;"
                    + "line-height: 11px;"
                    + "bottom: 2px;"
                    + "}"
                    + className + " dt:hover {"
                    + "border-color: #4f8fdd;"
                    + "z-index: 1000; }"
                    + className + " dt:hover .index {"
                    + "border: 1px solid #4f8fdd;"
                    + "color: #6d66ba; }"
                    + className + " a:hover, " + className + " dt:hover {"
                    + "color: #6d66ba;"
                    + "background-color: #B8DA1B; "
                    + "}"
                    + className + " dt.active:hover {"
                    + "border-color: #cbcfd4;"
                    + "z-index: auto; }"
                    + className + " dt.active .index {"
                    + "border-color: #606469;"
                    + "background-color: #6d66ba; }"
                    + className + " dt.active:hover {"
                    + "color: #6d66ba; }"
                    + className + " dt.active .index span {"
                    + "color: #dfdfdf; }"
                    + className + " dt.active .title {"
                    + "font-size: 15px; }"
                    + className + " dt .err, " + className + " dt .loader, " + className + " dt .title {"
                    + "display: none; }"
                    + className + " dt .data {"
                    + "display: block; }"
                    + className + " dt.active .title {"
                    + "display: block; }"
                    + className + " dt.active .data {"
                    + "display: none; }"
                    + className + " dt.loading .title {"
                    + "display: none; }"
                    + className + " dt.loading .data {"
                    + "display: none; } "
                    + className + " dt.loading .loader {"
                    + "display: block; }"
                    + className + " dt.error {"
                    + "border-bottom: 0; }"
                    + className + " dt.error .index {"
                    + "border-color: #9b6868;"
                    + "background-color: #B8DA1B; }"
                    + className + " dt.error .index span {"
                    + "display: none; }"
                    + className + " dt.error .index span.err {"
                    + "bottom: -1px;"
                    + "line-height: 11px;"
                    + "margin: 0;"
                    + "position: absolute;"
                    + "right: 3px;"
                    + "display: block; }"
                    + className + " dt.error .err {"
                    + "line-height: 18px;"
                    + "font-weight: bold;"
                    + "color: #6d66ba;"
                    + "display: block; }"
                    + className + " dt.error .title, " + className + " dt.active.error .title {"
                    + "display: none; }"
                    + className + " dt.error .data {"
                    + "display: none; }"
                    + className + " dt.error .loader {"
                    + "display: none; }"
                    + className + " dt.personal.loading .title {"
                    + "display: block; }"
                    + className + " dt.personal.loading .data {"
                    + "display: none; }"
                    + className + " dt.personal.loading .loader {"
                    + "display: none; }"
                    + className + " dt.personal.error .err {"
                    + "display: none }"
                    + className + " dt.personal.error .title {"
                    + "display: block; }"
                    + className + " dt.personal.error .data {"
                    + "display: none; }"
                    + className + " dt.personal.error .loader {"
                    + "display: none; }"
                    + className + " dt.new .index {"
                    + "background-color: #B8DA1B; }"
                    + className + " dt.new .index span {"
                    + "margin-right: -1px; "
                    + "width: 16px;"
                    + "height: 16px;"
                    + "display: inline-block;"
                    + "right: 0;"
                    + "background-image: url(data:image/gif;base64,R0lGODlhEQAQAMQAANHR0n+ChcLDxKusromLjtjY2LO0tru8vZKUl5qcn9vb29HS0qWnqc3Nzmtvc3V4fMrKy6Okp2Bkad/f3wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS41LWMwMTQgNzkuMTUxNDgxLCAyMDEzLzAzLzEzLTEyOjA5OjE1ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NEU5NTlFRjlGNUNGMTFFMjgxRjhGMzE5MUI5RDM3ODQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NEU5NTlFRkFGNUNGMTFFMjgxRjhGMzE5MUI5RDM3ODQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0RTk1OUVGN0Y1Q0YxMUUyODFGOEYzMTkxQjlEMzc4NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0RTk1OUVGOEY1Q0YxMUUyODFGOEYzMTkxQjlEMzc4NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAAAAAAALAAAAAARABAAAAVxoCSKTgRNwlREwehGUzw9Z3w4bipDEiwDD9FAFotIAsQJj5CciWoyiW438skcB5d2i5gAWluXQ5CQGcMSAmBSlglwWiuTCHatY44CdTuMSQwyA1VBaVEPMi00KgQiehMiMAASXUSCgA1VA4BNEAwLCiEAOw==); }"
                    + className + " dt.new:hover .index {"
                    + "background-color: #4f8fdd; }"
                    + className + " dt.new:hover .index span {"
                    + "background-image: url(data:image/gif;base64,R0lGODlhEQAQAMQAAG6c3b7K3nmj3qO63tfa36y/3oSp3o+u3rXF3trc38nS35u23lSQ3WGW3c/V38fQ35m03kWJ3d/f3wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS41LWMwMTQgNzkuMTUxNDgxLCAyMDEzLzAzLzEzLTEyOjA5OjE1ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NEU5NTlFRkRGNUNGMTFFMjgxRjhGMzE5MUI5RDM3ODQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NEU5NTlFRkVGNUNGMTFFMjgxRjhGMzE5MUI5RDM3ODQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0RTk1OUVGQkY1Q0YxMUUyODFGOEYzMTkxQjlEMzc4NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0RTk1OUVGQ0Y1Q0YxMUUyODFGOEYzMTkxQjlEMzc4NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAAAAAAALAAAAAARABAAAAVxYCSKDPRIgURAwOhCUiw1Z4wwbio/ESw7DdFAFoNEAEQJT5CciWqyiG438skYCJd2a5A4WlsXI3CQGcMRgUNSlgVwWiuTCHatYwwCdTuMRQoyA1VBaVENMi00KgIiehIiMA4RXUSCgApVA4BNDwsOCSEAOw==); }"
                    + className + " dt.done {"
                    + "color: #7c9b68; }"
                    + className + " dt.done .title {"
                    + "display: block; }"
                    + className + " dt.done .index {"
                    + "background-color: #B8DA1B;"
                    + "border-color: #7c9b68;"
                    + "color: #dfdfdf;"
                    + "display: block; }"
                    + className + " dt.done:hover {"
                    + "border-top: 1px solid #cbcdd4;"
                    + "border-bottom: 1px solid #dfdfdf;"
                    + "color: #7c9b68; }"
                    + className + " dd {"
                    + "overflow: hidden;"
                    + "margin-top: -1px;"
                    + "padding: 0 42px 15px 42px;"
                    + "position: relative;"
                    + "display: none;"
                    + "margin-left: 0;"
                    + "background: #dfdfdf;"
                    + "display: none; }"
                    + className + " dd.active {"
                    + "display: block; }"
                    + className + " dd.active .error {"
                    + "color: #9b6868; }"
                    + className + " dd.active.error .serverError {"
                    + "color: #9b6868;"
                    + "text-align: justify;}"
                    + className + " dd.active.error {"
                    + "display: none;"
                    + "}"
                    + className + " dd.personal.active.error {"
                    + "display: block;"
                    + "}"
                    + className + " dd input[type=\"text\"] {"
                    + "border: 0;"
                    + "padding: 5px 10px;"
                    + "font-size: 13px;"
                    + "margin: 5px 0 7px 0;"
                    + "background-color: #d2d2d2; }"
                    + className + " dd input[type=\"text\"]:focus {"
                    + "border: 1px solid #dfdfdf;"
                    + "padding: 4px 8px;"
                    + "background-color: #fff; }"
                    + className + " [type=\"button\"] {"
                    + "color: #606469;"
                    + "border: 1px solid #606469;"
                    + "border-radius: 3px;"
                    + "font-size: 16px;"
                    + "cursor: pointer;"
                    + "padding: 3px 20px;"
                    + "margin: 10px 0;"
                    + "background: transparent; }"
                    + className + " input[type=\"button\"]:hover {"
                    + "color: #4589dd;"
                    + "border: 1px solid #4589dd; }"
                    + className + " dd p {"
                    + "font-size: 11px; }"
                    + className + " dd h3 {"
                    + "font-size: 13px;"
                    + "font-weight: bold; }"
                    + className + " dd ul {"
                    + "margin: -10px 0 0 0;"
                    + "padding: 10px 0 0 0;"
                    + "list-style-type: none; }"
                    + className + " dd ul li {"
                    + "margin: 10px 0;"
                    + "padding: 0;"
                    + "list-style-type: none; }"
                    + className + " label {"
                    + "display: block;"
                    + "font-weight: bold;"
                    + "}"
                    + className + " .error {"
                    + "display: none; }"
            };

            this.TEMPLATE = "<h1>Электронная запись<br>на прием к врачу</h1>"
                + "<dl>"
                + "<dt>"
                + "<span class=\"index\"><span>1</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">Выберите район</span>"
                + "<span class=\"data\">data2</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>2</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">В каком учреждении?</span>"
                + "<span class=\"data\">data3</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt class=\"personal\">"
                + "<span class=\"index\"><span>3</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">Представьтесь, пожалуйста</span>"
                + "<span class=\"data\">data4</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd class=\"personal\"></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>4</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">К врачу какой специальности?</span>"
                + "<span class=\"data\">data5</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>5</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">К какому врачу?</span>"
                + "<span class=\"data\">data6</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>6</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">В какой день?</span>"
                + "<span class=\"data\">data7</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>7</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">В какое время?</span>"
                + "<span class=\"data\">data8</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt>"
                + "<span class=\"index\"><span>8</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">Проверьте, все ли верно</span>"
                + "<span class=\"data\">data9</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка. Данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd></dd>"
                + "<dt class=\"done\">"
                + "<span class=\"index\"><span>9</span><span class=\"err\">!</span></span>"
                + "<span class=\"title\">Запись успешно завершена!</span>"
                + "<span class=\"data\">data10</span>"
                + "<span class=\"loader\">Загрузка...</span>"
                + "<span class=\"err\">Произошла ошибка данные не загрузились.<br /><input type=\"button\" class=\"retry\" value=\"Повторить\" /></span>"
                + "</dt>"
                + "<dd>10 step content</dd>"
                + "</dl>"
                + "";

            if (window.zdravFrame) { // frame

                var _data = Bridge.parseMessage(document.location.hash);

                var CURRENT_HOST = window.location.hostname;

                var url = decodeURIComponent(document.location.hash.replace(/^#/, ''));
                this.el = document.getElementById("widget");
                Bridge.postMessage(
                    Bridge.prepareMessage({"loaded": true, "key": "value"}),
                    url,
                    parent
                );
                Bridge.receiveMessage(this.bind(function (e) {
                    var _data = Bridge.parseMessage(e.data);
                    if (_data && _data.config) {
                        delete _data.config;
                        delete _data.el;
                        OPTIONS = _data;
                        //alert("recive config");
                        this.render();
                        domReady(this.bind(this.domReady, this));
                    }
                }, this), "http://" + CURRENT_HOST);

            } else {
                var CURRENT_HOST = window.location.hostname;

                Bridge.receiveMessage(this.bind(function (e) {
                    var _data = Bridge.parseMessage(e.data);
                    if (_data && _data.loaded) {
                        Bridge.postMessage(
                            Bridge.prepareMessage(extend({"config": true}, OPTIONS)),
                            document.location.href,
                            document.getElementById("widget_zdrav_iframe").contentWindow
                        );
                    } else if (_data && _data.height) {
                        this._updateHeightHandler(_data.height);
                    }
                    //_this.receiveData(e.data);
                }, this), "http://" + CURRENT_HOST);

                var l = window.location,
                    widget_url = FRAME_PATH;
                widget_url += "#" + encodeURIComponent(document.location.href);
                this.el.style.display = "block";
                this.el.innerHTML = "<iframe id=\"widget_zdrav_iframe\" src=\"" + widget_url + "\" scrolling=\"no\" width=\"100%\" height=\"400\" frameborder=\"0\"></iframe>";

                window.bridge = Bridge;
            }
        };

        Zdrav.prototype.domReady = function () {
            var parent_url = decodeURIComponent(
                document.location.hash.replace(/^#/, '')
            );

            this.el.style.display = "block";

            this.steps = new Steps({
                el: this.el
            });

            this.steps.on("setStep", function (e, step) {
                var dd = step.el,
                    elems = this.el.getElementsByTagName("dd"),
                    pos;
                for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                    //elems[_i].style.display = "none";

                    elems[_i].previousSibling.style.display = "none";

                    Dom.removeClass(elems[_i], "active");
                    Dom.removeClass(elems[_i].previousSibling, "active");
                }
                ;
                for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                    if (elems[_i] === dd) {
                        pos = _i;
                        break;
                    } else {
                        // to see dd
                        //elems[_i].style.display = "block";
                        elems[_i].previousSibling.style.display = "block";
                    }
                }

                dd.previousSibling.style.display = "block";
                Dom.addClass(dd, "active");
                Dom.addClass(dd.previousSibling, "active");

                this._updateHeight();
            }, this);

            this.steps.on("updateDom", function (e, step) {
                this._updateHeight();
            }, this);

            var data = {};

            if (OPTIONS) {
                if (OPTIONS.dist_id) {
                    data.dist_id = OPTIONS.dist_id;
                }
                if (OPTIONS.lpu_id) {
                    data.lpu_id = OPTIONS.lpu_id;
                }
                if (OPTIONS.dist_title) {
                    data.dist_title = OPTIONS.dist_title;
                }
                if (OPTIONS.lpu_title) {
                    data.lpu_title = OPTIONS.lpu_title;
                }
            }

            for (var _i = 0, _len = stepsItems.length; _i < _len; _i++) {
                this.steps.add(stepsItems[_i], {
                    data: data
                });
            }

            this.steps.render();
        };


        Zdrav.prototype.render = function (params) {
            if (!this.once) {
                var head = document.getElementsByTagName("head")[0],
                    style = document.createElement("style");

                style.type = "text/css";
                if (style.styleSheet) {
                    style.styleSheet.cssText = this.CONFIG.CSS;
                } else {
                    style.appendChild(document.createTextNode(this.CONFIG.CSS));
                }

                head.appendChild(style);

                Event.add(this.el, "click", this.move, this);

                this.outer = document.createElement('div');
                this.outer.style.width = "320px";
                Dom.addClass(this.outer, "zdrav_" + this.TIME_HASH);
                this.once = true;
            }


            this.outer.innerHTML = this.TEMPLATE;
            this.el.appendChild(this.outer);
        };

        Zdrav.prototype.move = function (e) {
            var dt = this.nodeContains(e.target, this.el.getElementsByTagName("dt"));
            if (dt && dt.getAttribute("class") === "new") {
                this.render();
                this.domReady();
            }
            if (dt) {
                var dd = dt.nextSibling,
                    elems = this.el.getElementsByTagName("dd"),
                    pos;
                for (var _i = 0, _len = elems.length; _i < _len; _i++) {
                    if (elems[_i] === dd) {
                        pos = _i;
                        break;
                    }
                }
                this.steps.setCurrentStep(pos);
            }
        };

        Zdrav.prototype._updateHeightHandler = function (height) {
            document.getElementById("widget_zdrav_iframe").style.height = height + "px";
        };

        Zdrav.prototype._updateHeight = function (query) {
            Bridge.postMessage(
                Bridge.prepareMessage({"height": document.body.offsetHeight}),
                decodeURIComponent(document.location.hash.replace(/^#/, '')),
                parent
            );
        };

        return Zdrav;
    })(Base);


    try {
        initedWidget = new Zdrav(rootObj);
    } catch (e) {
        if (console && typeof console.log === "function") {
            console.log(e);
        }
        domReady(function () {
            initedWidget = new Zdrav(rootObj);
        });
    }

}.call(this));
