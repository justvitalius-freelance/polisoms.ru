namespace :sync do
  task :start => :environment do
    Sync::Syncer.new.start
  end

  task :clear => :environment do
    Doctor.destroy_all
    DoctorsApiOffice.destroy_all
    ActsAsVotable::Vote.destroy_all
    puts 'All doctors data successfully removed'
  end
end