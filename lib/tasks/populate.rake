# coding: utf-8

require 'csv'

namespace :services do
  task :assign_category => :environment do
    category = ServiceCategory.find_by_title('Услуги')
    Service.all.each do |service|
      service.service_category_id = category.id
      service.save!
    end
  end
end

namespace :csv do
  desc "Import car brans"
  task :import_service_categories => :environment do
    ServiceCategory.destroy_all
    csv_file_path = 'db/service_categories.csv'
    CSV.foreach(csv_file_path) do |row|
      row = ServiceCategory.create!({
        :id => row[0],
        :title => row[1]  
      })
      puts "Service categories created!"
    end
  end
end


namespace :test_data do
  task :doctors => :environment do
    Doctor.destroy_all

    10.times do
      doctor = Doctor.new
      doctor.name = Faker::Name.name
      doctor.job = Faker::Name.title
      doctor.active = true
      doctor.save!
      print "."
    end
    puts "\n"
  end

  task :destroy_doctors => :environment do
    Doctor.destroy_all
    ActsAsVotable::Vote.destroy_all
  end
end





