namespace :sync do
  task :start do
    on roles :web do
      debug "will start sync..."
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'sync:start'
        end
      end
    end
  end

  task :clear do
    on roles :web do
      debug "will clear all synced data..."
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'sync:clear'
        end
      end
    end
  end

end