# namespace :load do
#   task :defaults do
#     set :staging_db_name, nil
#     set :db_dump_path, -> { "#{shared_path}/db_dump.sql" }
#   end
# end

namespace :content do
  task :clone_system do
    on roles :web do
      debug "system folder is clonning..."
      target = "#{shared_path}/public/"
      # destination = "/home/justvitalius/www/polis-beta/shared/system"
      destination = "/home/justvitalius/www/polisoms-production/shared/public/system"
      execute :cp, '-R', destination, target
    end
  end

  # task :apply_production_dump do
  #   on roles :web do
  #     unless 'production' == fetch(:rails_env) || fetch(:staging_db_name).nil?
  #       debug "production db importing to staging db..."
  #       if test("[ -e #{fetch(:db_dump_path)} ]")
  #         execute :mysql, '-u polis', fetch(:staging_db_name), '<', fetch(:db_dump_path)
  #       else
  #         error "dump of production database is not exists"
  #       end
  #     end
  #   end
  # end

end
